var express = require("express"),
    app = express();
var xFrameOptions = require('x-frame-options');

var port = process.env.VCAP_APP_PORT || 8000;
var uri = process.env.ENV_VAR || "http://localhost:8080";
// app.enable('trust proxy');

// app.use(function (req, res, next) {
//     if (req.secure) {
//         // request was via https, so do no special handling
//         next();
//     } else {
//         // request was via http, so redirect to https
//         res.redirect('https://' + req.headers.host + req.url);
//     }
// });

app.use(express.static(__dirname + './../app'));

// app.use(xFrameOptions());

app.get("/env", function (request, response) {
    response.type('text/plain');
    response.send(uri);
    res.get('X-Frame-Options')
});

app.all('/*', function (req, res) {
    res
        .status(200)
        .set({ 'content-type': 'text/html; charset=utf-8' })
        .sendfile('public/index.html');
});


app.listen(port);

require("cf-deployment-tracker-client").track();
