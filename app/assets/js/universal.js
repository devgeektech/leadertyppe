/****************************************************************************************
 Universal - Smart multi-purpose html5 template
 To use this template you must have a license purchased at Themeforest (themeforest.com)
 Copyright 2016 ForBetterWeb.com
 ***************************************************************************************/

(function ($) {
    "use strict";
    $(document).ready(function () {

        /***********************************************
         * Preloader
         ***********************************************/

        jQuery(window).load(function () {
            if (window.location.pathname == "/") {
                jQuery("#status").fadeOut();
                jQuery("#preloader").delay(1000).fadeOut("slow");
            }
        })

    });
})(jQuery);
