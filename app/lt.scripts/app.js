'use strict';
var leaderTYPE = angular.module('leaderTYPE', [
    'leaderTYPE.services',
    'ui.bootstrap',
    'ui.router',
    'ngCookies',
    'auth0',
    'angular-storage',
    'auth0.lock',
    'ngMessages',
    'angular-jwt',
    'dialogs.main',
    'ngSanitize',
    'dndLists',
    'googlechart',
    '720kb.tooltips',
    'ngInputModified',
    'angularUtils.directives.dirPagination',
    'angularUtils.filters.ordinalDate'
]);
leaderTYPE.controller("myAppCtrl", function($scope,$rootScope) {
  window.onbeforeunload = function(e) {
     var message = "Your confirmation message goes here.",
         e = e || window.event;
     // For IE and Firefox
     if (e) {
         e.returnValue = message;
     }
     // For Safari
     return message;
  };
});
leaderTYPE.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    $httpProvider.interceptors.push('authInterceptor');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        //1
        .state('landingpage', {
            url: '/',
            templateUrl: 'lt.modules/landingPage/landingPage.html',
            params: { 'package': null },
            controller: 'landingPageCtrl'
        })

        .state('feedback', {
            url: '/feedback?inviteid&emailId&category&userid',
            templateUrl: 'lt.modules/dashboard.LTITeam/feedback.html',
            controller: 'feedbackCtrl'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'lt.modules/dashboard/dashboard.html',
            controller: 'dashboardCtrl'
        })
        .state('verifyemail', {
            url: '/verifyemail',
            templateUrl: 'lt.modules/authentication/emailverification.html',
            controller: 'emailVerificationCtrl'
        })

        .state('noemail', {
            url: '/noemail',
            templateUrl: 'lt.modules/authentication/noemail.html',
            controller: 'noemailCtrl'
        })

        .state('dashboard.mylti', {
            url: '/lti',
            views: {

                // the main template will be placed here (relatively named)
                '': {
                    templateUrl: 'lt.modules/dashboard.LTI/mylti.html',
                    controller: 'myLtiCtrl'
                },

                'landing@dashboard.mylti': {
                    templateUrl: 'lt.modules/dashboard.LTI/modules/landinglti.html',
                    controller: 'landingltiCtrl'
                },

                'ltireport@dashboard.mylti': {
                    templateUrl: 'lt.modules/dashboard.LTI/modules/report.html',
                    controller: 'reportCtrl'
                },

                'ltiresult@dashboard.mylti': {
                    templateUrl: 'lt.modules/dashboard.LTI/modules/result.html',
                    controller: 'resultCtrl'
                },

                'slider@dashboard.mylti': {
                    templateUrl: 'lt.modules/dashboard.LTI/modules/slider.html',
                    controller: 'sliderCtrl'
                }
            }

        })
        .state('dashboard.myltbrand', {
            url: '/myltbrand',
            templateUrl: 'lt.modules/dashboard.LTIBrand/branding.html',
            controller: 'brandingCtrl'
        })

        .state('dashboard.myltteam', {
            url: '/myltteam',
            views: {
                '': {
                    templateUrl: 'lt.modules/dashboard.LTITeam/myltiteam.html',
                    controller: 'myltiteamCtrl'
                },
                'responsesTeam@dashboard.myltteam': {
                    templateUrl: 'lt.modules/dashboard.LTITeam/modules/responses.html',
                    controller: 'responsesCtrl'
                },
                'inviteTeam@dashboard.myltteam': {
                    templateUrl: 'lt.modules/dashboard.LTITeam/modules/invite.html',
                    controller: 'inviteCtrl'
                },
                'reportTeam@dashboard.myltteam': {
                    templateUrl: 'lt.modules/dashboard.LTITeam/modules/lteamreport.html',
                    controller: 'lteamReportCtrl'
                },
                'defaultViewTeam@dashboard.myltteam': {
                    templateUrl: 'lt.modules/dashboard.LTITeam/modules/defaultView.html',
                    controller: 'defaultViewCtrl'
                }
            }
        })


        .state('dashboard.myltpeak', {
            url: '/ltpeak',
            //templateUrl: 'lt.modules/comingSoon/soon.html'
            views: {
                '': {
                    templateUrl: 'lt.modules/dashboard.LTIPeak/leaderPeak.html',
                    controller: 'leaderPeakCtrl'
                },

                'createSWOT@dashboard.myltpeak': {
                    templateUrl: 'lt.modules/dashboard.LTIPeak/modules/createSWOT.html',
                    controller: 'createSWOTCtrl'
                },

                'listSWOT@dashboard.myltpeak': {
                    templateUrl: 'lt.modules/dashboard.LTIPeak/modules/listSWOT.html',
                    controller: 'listSWOTCtrl'
                },

                'viewSWOT@dashboard.myltpeak': {
                    templateUrl: 'lt.modules/dashboard.LTIPeak/modules/viewSWOT.html',
                    controller: 'viewSWOTCtrl'
                },

                'defaultViewSWOT@dashboard.myltpeak': {
                    templateUrl: 'lt.modules/dashboard.LTIPeak/modules/defaultViewSWOT.html',
                    controller: 'defaultViewSWOTCtrl'
                }
            }
        })
        //Leadership brand new user 4
        .state('profile', {
            url: '/profile',
            templateUrl: 'lt.modules/profile/profile.html',
            controller: 'profileCtrl'
        })
        .state('accountpage', {
            url: '/account',
            templateUrl: 'lt.modules/account/accountPage.html',
            controller: 'accountPageCtrl'

        })

        .state('howitworks', {
            url: '/howitworks',
            templateUrl: 'lt.modules/outerSpace/howItWorks.html',
            controller: 'howItWorksCtrl'
        })

        .state('methodology', {
            url: '/methodology',
            templateUrl: 'lt.modules/outerSpace/methodology.html'
        })

        .state('privacy', {
            url: '/privacy',
            templateUrl: 'lt.modules/outerSpace/privacy.html',
            controller: 'privacyCtrl'
        })

        .state('comingsoon', {
            url: '/comingsoon',
            templateUrl: 'lt.modules/comingSoon/soon.html'
        })

        .state('checkout', {
            url: '/checkout',
            templateUrl: 'lt.modules/checkout/checkOut.html',
            controller: 'checkOutCtrl'
        })

        .state('becomeALeaderTypeCoach', {
            url: '/becomeALeaderTypeCoach',
            templateUrl: 'lt.modules/resources/becomeALeaderTypeCoach.html',
            controller: 'resourcesCtrl'
        })
        .state('elementOfLeadership', {
            url: '/elementOfLeadership',
            templateUrl: 'lt.modules/resources/elementsOfLeadership.html',
            controller: 'resourcesCtrl'
        })
        .state('leaderCoachMobileApp', {
            url: '/leaderCoachMobileApp',
            templateUrl: 'lt.modules/resources/leaderCoachMobileApp.html#begin',
            controller: 'resourcesCtrl'
        })
        .state('leaderCoachMobileAppLeadYourself', {
            url: '/leaderCoachMobileApp#lead-yourself',
            templateUrl: 'lt.modules/resources/leaderCoachMobileApp.html#lead-yourself',
            controller: 'resourcesCtrl'
        })
        .state('leaderCoachMobileAppLeadConversations', {
            url: '/leaderCoachMobileApp#lead-conversations',
            templateUrl: 'lt.modules/resources/leaderCoachMobileApp.html#lead-conversations',
            controller: 'resourcesCtrl'
        })
        .state('leaderCoachMobileAppLeadMeetings', {
            url: '/leaderCoachMobileApp#lead-meetings',
            templateUrl: 'lt.modules/resources/leaderCoachMobileApp.html#lead-meetings',
            controller: 'resourcesCtrl'
        })
        .state('searchLeadershipCoachDatabase', {
            url: '/searchLeadershipCoachDatabase',
            templateUrl: 'lt.modules/resources/searchLeadershipCoachDatabase.html',
            controller: 'resourcesCtrl'
        })
        .state('reports', {
            url: '/reports',
            templateUrl: 'lt.modules/reports/reports.html',
            controller: 'reportsCtrl'
        })

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(false);

}).constant('constants', {
    apiUrl: '#{apiUrl}'
}).filter('join', [function () {
    return function (array, separator) {
        if (!array) {
            return '';
        }
        return array.join(separator);
    };
}]).directive('emailEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.emailEnter);
                });

                event.preventDefault();
            }
        });
    };
}).directive('multipleEmails', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {

                var emails = viewValue.split(',');
                // define single email validator here
                var re = /\S+@\S+\.\S+/;

                // angular.foreach(emails, function() {
                var validityArr = emails.map(function (str) {
                    return re.test(str.trim());
                }); // sample return is [true, true, true, false, false, false]
                var atLeastOneInvalid = false;
                angular.forEach(validityArr, function (value) {
                    if (value === false)
                        atLeastOneInvalid = true;
                });
                if (!atLeastOneInvalid) {
                    // ^ all I need is to call the angular email checker here, I think.
                    ctrl.$setValidity('multipleEmails', true);
                    return viewValue;
                } else {
                    ctrl.$setValidity('multipleEmails', false);
                    return undefined;
                }
                // })
            });
        }
    };
}).directive('limitChar', function () {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            limit: '=limit',
            ngModel: '=ngModel'
        },
        link: function (scope) {
            scope.$watch('ngModel', function (newValue, oldValue) {
                if (newValue) {
                    var length = newValue.toString().length;
                    if (length > scope.limit) {
                        scope.ngModel = oldValue;
                    }
                }
            });
        }
    };
}).run(function ($rootScope, $location, authService, envService, authManager, loaderService, constants, $timeout) {
    envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
        if (response && response != "") {
            constants.apiUrl = response;
        }
        else {
            constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
        }
    });

    // Put the authService on $rootScope so its methods
    // can be accessed from the nav bar
    //authService.init
    $rootScope.authService = authService;
    $rootScope.takingAssessment = false;

    $rootScope.loaderService = loaderService;
    $rootScope.$broadcast('authService', authService);

    authManager.checkAuthOnRefresh();

    authManager.redirectWhenUnauthenticated();
});
