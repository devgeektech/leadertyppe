angular.module('leaderTYPE.services')
    .factory('loaderService', function () {
        return {
            show: function (message) {
                jQuery(".ui").hide();
                jQuery("#preloader").show();
                jQuery("#status").delay(500).show();
                jQuery("#loadingmsg").text(message);
                jQuery("#loadingmsg").show();
            },
            hide: function () {
                jQuery("#loadingmsg").fadeOut();
                jQuery("#status").fadeOut();
                jQuery("#preloader").delay(1000).fadeOut("slow");
                jQuery(".ui").delay(1000).show();
            }
        }
    });