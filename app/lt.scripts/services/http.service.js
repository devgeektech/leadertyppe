

angular.module('leaderTYPE.services')
    .factory("envService", function ($http, $q) {
        return {
            get: function (url) {
                return $http.get(url + "env").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                );
            }
        }
    })
    .factory("ltiService", function ($http, constants) {
        return {
            get: function () {
                return $http.get(constants.apiUrl + "leadertype/questions/").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            post: function (assessment) {
                return $http.post(constants.apiUrl + "leadertype/LTI/assessment", assessment).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            getLTIndicator: function (indicatorId) {
                return $http.get(constants.apiUrl + "leadertype/LTI/indicators/" + indicatorId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            },
            setLeaderType: function (indicatorId, userId) {
                return $http.post(constants.apiUrl + "leadertype/LTI/assessment/" + userId + "/" + indicatorId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            },
            hasAssessment: function (userId) {
                return $http.get(constants.apiUrl + "leadertype/LTI/hasassessment/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            },
            getAssessment: function (userId, type) { //brand,result
                return $http.get(constants.apiUrl + "leadertype/LTI/assessment/" + userId + "/" + type).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            },
            updateGuestAssessment: function (oldId, newId) {
                return $http.post(constants.apiUrl + "leadertype/LTI/updateguestassessment/" + oldId + "/" + newId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            }
        }
    })
    .factory("profileService", function ($http, constants) {
        return {
            isUserAvailable: function (userId) {
                return $http.get(constants.apiUrl + "web/profile/availability/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            get: function (userId) {
                return $http.get(constants.apiUrl + "web/profile/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            post: function (profile) {
                return $http.post(constants.apiUrl + "web/profile", profile).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            }
        }
    })
    .factory("checkOut", function ($http, constants) {
        return {
            getOrderId: function (subscription) {
                return $http.post(constants.apiUrl + "web/subscription", subscription).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            payment: function (paymentDetails) {
                return $http.post(constants.apiUrl + "web/payment", paymentDetails).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            cancelSubscription: function (userId, isAutoRenewal) {
                return $http.post(constants.apiUrl + "web/toggleAutoRenewal/" + userId + "/" + isAutoRenewal).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            checkPackage:function(userId,packageId){
              return $http.get(constants.apiUrl + "web/validatePackage/" + userId + "/" + packageId).then(
                  function successCallback(response) {
                      return response.data;
                  },
                  function errorCallback(response) {
                      return response.data;
                  });
            }

        }
    })
    .factory("webService", function ($http, constants) {
        return {
            getPackages: function () {
                return $http.get(constants.apiUrl + "web/packages").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            getSubPackages: function (packageId) {
                return $http.get(constants.apiUrl + "web/packages/" + packageId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            postContact: function (contact) {
                return $http.post(constants.apiUrl + "web/contactus", contact).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            postInterestMail: function (emailData) {
                return $http.post(constants.apiUrl + "web/emailus", emailData).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            getPackageService: function (userId) {
                return $http.get(constants.apiUrl + "web/account/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            getNewToken: function (token) {
                return $http.get(constants.apiUrl + "web/regeneratetoken/" + token).then(
                    function successCallback(response) {
                        return response.data;

                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                );
            }
        }
    }).factory("brandingServices", function ($http, constants) {
        return {
            getBrandKind: function (brand) {
                return $http.get(constants.apiUrl + "leadertype/brand/" + brand).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            getBrandAssessment: function (userId) { //brand,result
                return $http.get(constants.apiUrl + "leadertype/LTI/assessment/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    }
                )
            },
            updateAssessment: function (content) {
                return $http.post(constants.apiUrl + "leadertype/LTI/updateAssessment", content).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            }
        }
    }).factory("teamServices", function ($http, constants) {
        return {
            postInvite: function (invite) {
                return $http.post(constants.apiUrl + "team/invite", invite).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getInvite: function (userId) {
                return $http.get(constants.apiUrl + "team/invite/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getQuestions: function (inviteId, emailId, userId, category) {
                if (userId != null) {
                    return $http.get(constants.apiUrl + "team/self/" + inviteId + "?emailId=" + emailId + "&userId=" + userId).then(
                        function successCallback(response) {
                            return response.data;
                        },
                        function errorCallback(error) {
                            return error.data;
                        }
                    )
                }
                else {
                    return $http.get(constants.apiUrl + "team/" + inviteId + "?emailId=" + emailId + "&category=" + category).then(
                        function successCallback(response) {
                            return response.data;
                        },
                        function errorCallback(error) {
                            return error.data;
                        }
                    )
                }

            },
            postFeedback: function (feedback) {
                return $http.post(constants.apiUrl + "team/assessment", feedback).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getFeedbacks: function (userId, inviteId) {
                if (inviteId != null) {
                    return $http.get(constants.apiUrl + "team/assessment/" + userId + "/" + inviteId).then(
                        function successCallback(response) {
                            return response.data;
                        },
                        function errorCallback(error) {
                            return error.data;
                        }
                    )
                }
                else {
                    return $http.get(constants.apiUrl + "team/assessment/" + userId).then(
                        function successCallback(response) {
                            return response.data;
                        },
                        function errorCallback(error) {
                            return error.data;
                        }
                    )
                }
            },
            updateEmail: function (inviteId, oldEmail, newEmail) {
                return $http.put(constants.apiUrl + "team/invite/" + inviteId + "?oldEmail=" + oldEmail + "&newEmail=" + newEmail).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            updateInvite: function (inviteId, emailIds) {
                return $http.put(constants.apiUrl + "team/invite/addRecipient/" + inviteId, emailIds).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            deleteInvite: function (inviteId) {
                return $http.delete(constants.apiUrl + "team/invite/" + inviteId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            }
        }
    }).factory("peakService", function ($http, constants) {
        return {
            getStrengths: function () {
                return $http.get(constants.apiUrl + "swot/swotlist/strengths").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getOpportunities: function () {
                return $http.get(constants.apiUrl + "swot/swotlist/opportunities").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getThreats: function () {
                return $http.get(constants.apiUrl + "swot/swotlist/threats").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getWeaknesses: function () {
                return $http.get(constants.apiUrl + "swot/swotlist/weaknesses").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getSuccessCriteria: function () {
                return $http.get(constants.apiUrl + "swot/swotlist/successcriteria").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getBiases: function () {
                return $http.get(constants.apiUrl + "swot/biases").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getSwot: function (userId, swotId) {
                return $http.get(constants.apiUrl + "swot/getswot/" + userId + "/" + swotId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getAllSwot: function (userId) {
                return $http.get(constants.apiUrl + "swot/getallswot/" + userId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            deleteSwot: function (userId, swotId) {
                return $http.delete(constants.apiUrl + "swot/deleteswot/" + userId + "/" + swotId).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            addSwot: function (userId, swot) {
                return $http.post(constants.apiUrl + "swot/addswot/" + userId, swot).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            updateSwot: function (userId, swotId, swot) {
                return $http.put(constants.apiUrl + "swot/updateswot/" + userId + "/" + swotId, swot).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            }
        }
    }).factory("reportService", function ($http, constants) {
        return {
            getReport: function (pageNo) {
                return $http.get(constants.apiUrl + "report/users/"+pageNo).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            getCampaign: function () {
                return $http.get(constants.apiUrl + "report/campaign").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            },
            addCampaign: function (campaign) {
                return $http.post(constants.apiUrl + "report/campaign/add", campaign).then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(response) {
                        return response.data;
                    });
            },
            deleteCampaign: function () {
                return $http.post(constants.apiUrl + "report/campaign/delete").then(
                    function successCallback(response) {
                        return response.data;
                    },
                    function errorCallback(error) {
                        return error.data;
                    }
                )
            }
        }
    });
