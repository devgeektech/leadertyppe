
angular
  .module('leaderTYPE.services', [])
  .service('authService', authService)
  .factory('authInterceptor', function ($rootScope, $q) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($rootScope.authService.token()) {
          config.headers.Authorization = 'Bearer ' + $rootScope.authService.token();
        }
        return config;
      },
      response: function (response) {
        return response || $q.when(response);
      },
      responseError: function (rejection) {
        if (rejection.status === 401) {
          $rootScope.authService.logout();
          $rootScope.authService.loginWithSignUp();
        }
        return $q.reject(rejection)
      }
    };
  });;

authService.$inject = ['$rootScope', 'authManager', '$location', '$state', 'profileService', 'constants', '$cookieStore'];

function authService($rootScope, authManager, $location, $state, profileService, constants, $cookieStore) {

  var userProfile = $cookieStore.get('profile') != undefined ? JSON.parse($cookieStore.get('profile')) : null;
  var lock = null;
  function init() {
    lock = new Auth0Lock('IdD9y0TBYey5jXAX5RC4IQVL2KHkixKT', 'leadertype.auth0.com', {
      auth: {
        redirect: false
      },
      theme: {
        logo: 'assets/images/logo.png',
        primaryColor: 'green'
      },
      languageDictionary: {
        title: "LeaderTYPE"
      },
      additionalSignUpFields: [
        {
          name: "first_name",
          placeholder: "your first name",
          validator: function (first_name) {
            return {
              valid: first_name.match(/^[a-zA-Z\s]*$/) && first_name.length != 0,
              hint: "Enter a valid First name" // optional
            };
          }
        },
        {
          name: "last_name",
          placeholder: "your last name",
          validator: function (last_name) {
            return {
              valid: last_name.match(/^[a-zA-Z\s]*$/) && last_name.length != 0,
              hint: "Enter a valid Last name" // optional
            };
          },
        },
        {
          name: "companyName",
          placeholder: "org code",
          validator: function (companyName) {
            return {
              valid: companyName.match(/^[a-zA-Z\s]*$/),
              hint: "Enter a valid organisation name" // optional
            };
          },
        }


      ],
      allowForgotPassword: true,
      autoclose: true,
      rememberLastLogin: false,
      redirect: true
    });
    registerAuthenticationListener();
  }

  function getUserProfile() {
    if (userProfile == null) {
      userProfile = $cookieStore.get('profile') != undefined ? JSON.parse($cookieStore.get('profile')) : null
    }
    return userProfile;
  }

  function login() {
    init();
    lock.show({
      auth: {
        params: { state: 'login@' + constants.apiUrl },
        redirect: true
      },
      allowSignUp: false,
      gravatar: false,

    });
  }

  function updateProfile() {
    userProfile = $cookieStore.get('profile') != undefined ? JSON.parse($cookieStore.get('profile')) : null;
  }

  function hide() {
    lock.hide();
  }

  function signUp() {
    init();
    lock.show({
      auth: {
        params: {
          state: 'signup@' + constants.apiUrl
        },
        redirect: true
      },
      allowLogin: false,
      loginAfterSignup: false,
      gravatar: false
    });
  }

  function loginWithSignUp() {
    init();
    lock.show({
      auth: {
        params: { state: 'login|signup@' + constants.apiUrl },
        redirect: true
      },
      allowSignUp: true,
      gravatar: false,

    });
  }
  function gotoHome() {
    $state.go("landingpage");
  }

  function gotoDashboard() {
    $state.go('dashboard.mylti');
  }
  function gotoMyLTBrand() {
    $rootScope.loaderService.show("Loading your LeaderBrand");
    $state.go('dashboard.myltbrand');
  }

  function gotoSWOT() {
    $state.go('dashboard.myltpeak');
  }
  function gotoAccount() {
    //$rootScope.loaderService.show("Loading your account");
    $state.go("accountpage");
  }

  function getToken() {
    return $cookieStore.get("id_token");
  }

  function getUserId(prof) {
    if (userProfile == null) {
      updateProfile();
    }
    if (prof != null) {
      if (prof.hasOwnProperty('user_id')) {
        return prof.user_id.split("|")[1];
      }
    }
    else if (userProfile != null) {
      return userProfile.userId;
    }
    return null;
  }

  function getIsSocial() {

    if (isRealValue(userProfile)) {
      if (userProfile.hasOwnProperty('user_id')) {
        return !userProfile.user_id.split("|")[0].startsWith("auth0");
      }
    }
    return true;
  }

  function gotoProfile() {
    $rootScope.loaderService.show("Loading your profile");
    $state.go('profile');
  }

  function gotoSlider() {
    $state.go('dashboard.mylti.slider');
  }

  function gotoReports() {
    $state.go('reports');
  }

  // Logging out just requires removing the user's
  // id_token and profile
  function logout() {
    clearCookieStore();
    $state.go('landingpage');
  }

  function clearCookieStore() {
    $cookieStore.remove('id_token');
    $cookieStore.remove('profile');
    $cookieStore.remove('name');
    authManager.unauthenticate();
    userProfile = null;
  }

  // Set up the logic for when a user authenticates
  // This method is called from app.run.js
  function registerAuthenticationListener() {
    lock.on('authenticated', function (authResult) {
      var state = authResult.state.split("@")[0];
      var prof = {};
      if (state == "login") {
        $rootScope.loaderService.show("Logging you in, please wait");
      }
      else {
        $rootScope.loaderService.show("Signing you up, please wait");
      }
      var isemailVerified = !(authResult.idTokenPayload.sub.startsWith("auth0") && state == "signup");
      $cookieStore.put('id_token', authResult.idToken);
      lock.getProfile(authResult.idToken, function (error, profile) {
        var modifiedProf = getModifiedProfile(profile);
        $cookieStore.put('profile', JSON.stringify(modifiedProf));
        updateProfile();
        authManager.authenticate();
        if (state == "signup") {
          doSignUp(isemailVerified, null,modifiedProf);
        }
        else {
          profileService.isUserAvailable(modifiedProf.userId).then(function (response) {
            if (response.data) {
              profileService.get(modifiedProf.userId).then(function (response) {
                if (profile.hasOwnProperty('user_metadata')) {
                  modifiedProf.firstName = response.data.firstName;
                  modifiedProf.lastName = response.data.lastName;
                  modifiedProf.isAdmin = response.data.isAdmin;
                  $cookieStore.put('profile', JSON.stringify(modifiedProf));
                  updateProfile();
                }
                if (response.data) {
                  $rootScope.$broadcast("onLoginSuccess");
                }
                else {
                  var prof = getModifiedProfile(profile);
                  profileService.post(prof).then(function (response) {
                    if (response.isSuccess) {
                      $rootScope.$broadcast("onLoginSuccess");
                    }
                    else if (response.responseMessage === "email cannot be empty") {
                      $rootScope.$broadcast("onEmailFailed");
                    }
                    else {
                      reset();
                      $rootScope.$broadcast("onLoginFailed");
                    }
                  });
                }
              });
            }
            else {
              doSignUp(!authResult.idTokenPayload.sub.startsWith("auth0"), state == "login|signup",modifiedProf);
            }
          });
        }
      });

    });
  }

  function reset() {
    userProfile = null;
    $cookieStore.put('profile', null);
  }

  function doSignUp(isemailVerified, doReset,prof) {
    profileService.post(prof).then(function (response) {
      if (response.isSuccess && isemailVerified) {
        $rootScope.$broadcast("onLoginSuccess");
      }
      else if (response.responseMessage === "email cannot be empty") {
        $rootScope.$broadcast("onEmailFailed");
      }
      else if (!isemailVerified) {
        if (doReset == null) {
          reset();
        }
        $rootScope.$broadcast("onEmailVerification");
      }
      else {
        authManager.unauthenticate();
      }
    });
  }

  function getProfile() {
    var profile = {};
    profile.userId = getUserId();
    if (userProfile.hasOwnProperty("email")) {
      profile.email = userProfile.email;
    }

    if (userProfile.hasOwnProperty("user_metadata")) {
      profile.firstName = userProfile.user_metadata.first_name;
      profile.lastName = userProfile.user_metadata.last_name;
      profile.companyName = userProfile.user_metadata.companyName;
    } else {
      if (userProfile.hasOwnProperty("given_name")) {
        profile.firstName = userProfile.given_name;
      }

      if (userProfile.hasOwnProperty("family_name")) {
        profile.lastName = userProfile.family_name;
      } else if (userProfile.hasOwnProperty("given_name")) {
        profile.lastName = userProfile.given_name;
      }
      //TODO: this needs to be updated when cash provides company code
      profile.companyName = "others";
      profile.isAdmin = userProfile.isAdmin;
    }

    profile.isSocialProfile = getIsSocial();
    return profile;
  }

  function getModifiedProfile(prof) {
    var profile = {};
    profile.userId = getUserId(prof);
    if (prof.hasOwnProperty("email")) {
      profile.email = prof.email;
    }

    if (prof.hasOwnProperty("user_metadata")) {
      profile.firstName = prof.user_metadata.first_name;
      profile.lastName = prof.user_metadata.last_name;
      profile.companyName = prof.user_metadata.companyName;
    } else {
      if (prof.hasOwnProperty("given_name")) {
        profile.firstName = prof.given_name;
      }

      if (prof.hasOwnProperty("family_name")) {
        profile.lastName = prof.family_name;
      } else if (prof.hasOwnProperty("given_name")) {
        profile.lastName = prof.given_name;
      }
      //TODO: this needs to be updated when cash provides company code
      profile.companyName = "others";
    }

    profile.isSocialProfile = getIsSocial();
    return profile;
  }

  function isRealValue(obj) {
    return obj && obj !== null && obj !== 'undefined';
  }

  function isAuthenticated() {
    return isRealValue(userProfile);
  }

  function getName() {
    userProfile = $cookieStore.get('profile') != undefined ? JSON.parse($cookieStore.get('profile')) : {};
    if (userProfile != null) {
      $cookieStore.put('name', userProfile.firstName);
    }
    return $cookieStore.get('name');
  }

  function isShowReports() {
    if(userProfile !== null) {
      var profile = getProfile();
      return profile.isAdmin;
    }
    return false;
  }

  return {
    updateProfile: updateProfile,
    userProfile: getUserProfile,
    login: login,
    logout: logout,
    token: getToken,
    name: getName,
    userId: getUserId,
    hide: hide,
    reset: reset,
    signUp: signUp,
    loginWithSignUp: loginWithSignUp,
    getProfile: getProfile,
    gotoAccount: gotoAccount,
    gotoDashboard: gotoDashboard,
    gotoMyLTBrand: gotoMyLTBrand,
    gotoSWOT: gotoSWOT,
    gotoProfile: gotoProfile,
    gotoReports: gotoReports,
    isShowReports: isShowReports,
    gotoHome: gotoHome,
    isAuthenticated: isAuthenticated,
    registerAuthenticationListener: registerAuthenticationListener,
  }
}
