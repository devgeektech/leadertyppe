var app = angular.module('leaderTYPE');
app.controller('noemailCtrl', function ($scope, $state, $rootScope, profileService,$cookieStore) {
    $rootScope.loaderService.hide();
    $scope.profile = {};
    $scope.showError = false;
    $scope.error = "Fill in email";
    $scope.updateProfile = function () {
        if ($scope.SentMessage.$invalid) {
            $scope.showError = true;
        } else {
            $scope.showError = false;
            $rootScope.loaderService.show("Updating your profile, please wait");
            var profile = $rootScope.authService.getProfile();
            profile.email = $scope.profile.email;
            profileService.post(profile).then(function (response) {
                $rootScope.loaderService.hide();
                if (response.isSuccess) {
                    $state.go("landingpage");
                }
                else if (response.responseMessage === "email cannot be empty") {
                    $scope.showError= true;
                    $scope.error = "Something went wrong, please try again";
                }
                else {
                    userProfile = null;
                    $cookieStore.put('profile', null);
                    $state.go("landingpage");
                }
            });

        }
    };

    $scope.goHome= function(){
        $rootScope.authService.reset();
        $rootScope.authService.gotoHome();
    }


});