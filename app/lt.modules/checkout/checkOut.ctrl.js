var app = angular.module('leaderTYPE');
app.controller('checkOutCtrl', function ($scope, $state, $rootScope, checkOut, $cookieStore, profileService) {
    if ($rootScope.authService.isAuthenticated()) {
        var jsonString = $cookieStore.get('cartList');
        var isPaymentSuccessful = false;
        var isPaymentFailed = false;
        var subcriptionList = [];
        var renewalType = parseInt($cookieStore.get('selectedPackageDuration')) == 0 ? "Month" : "Year";
        var subcription = {};
        var handler = StripeCheckout.configure({
            key: 'pk_live_dToJGXm9mUPcEsfEIzLHRHAN',
            image: '/assets/images/logo.png',
            locale: 'auto',
            allowRememberMe: false,
            token: function (token) {
                if (!isPaymentFailed) {
                    isPaymentSuccessful = true;
                    $rootScope.loaderService.show("Payment in progress, please dont close the site");
                    // You can access the token ID with `token.id`.
                    // Get the token ID to your server-side code for use.
                    var paymentDetails = {};
                    if (token) {
                        paymentDetails.userId = $rootScope.authService.userId();
                        paymentDetails.stripeToken = token.id;
                        paymentDetails.packageId = $scope.cartList[0].packageId;
                        if ($scope.Profile.isTrailUsed) {
                            paymentDetails.renewalType = renewalType;
                            //paymentDetails.amount = $scope.total;
                        } else {
                            paymentDetails.renewalType = "Month";
                            //paymentDetails.amount = 0;
                        }

                        $rootScope.loaderService.show("Payment in Progress..");
                        checkOut.payment(paymentDetails).then(function (response) {
                            $rootScope.loaderService.hide();
                            if (response.isSuccess) {
                                $cookieStore.put("cartList", "");
                                $scope.stripeToken = response.data;
                                $rootScope.authService.gotoAccount();
                            }
                            else {
                                $("#packagePaymentStatus").modal('show');
                            }
                        });
                    }
                }

            },
            closed: function () {
                if (!isPaymentSuccessful) {
                    isPaymentFailed = true;
                    $("#packagePaymentFailureStatus").modal("show");
                }
                else {
                    isPaymentFailed = false;
                }
            }
        });

        $scope.cartList = jsonString ? JSON.parse(jsonString) : {};
        $scope.total = 0;
        $scope.removeItemFromCart = function (itemToRemove) {
            for (var i = 0; i < $scope.cartList.length; i++)
                if ($scope.cartList[i].productName == itemToRemove) {
                    $scope.cartList.splice(i, 1);
                    calculateTotal();
                }
            $cookieStore.put("cartList", JSON.stringify($scope.cartList));
            //to get updated with latest changes
            //getOrders();
        }

        $scope.isCartEmpty = function () {
            return $scope.cartList.length == 0;
        }

        $scope.cartLength = function () {
            return cartList.length;
        }

        $scope.hideFailureStatus = function () {
            $("#packagePaymentFailureStatus").modal("hide");
        }

        $scope.hideRenewStatus = function () {
            $("#packagePaymentStatus").modal("hide");
        }

        $scope.hideConfirmPackage = function () {
            $("#packagePaymentConfirmation").modal("hide");
        }

        $scope.confirmPackage = function () {
            $("#packagePaymentConfirmation").modal("hide");
            if ($scope.Profile.isTrailUsed) {
                handler.open({
                    name: 'LeaderType',
                    currency: 'USD',
                    zipCode: false,
                    amount: (($scope.total + 0.009) * 100) // hack to get the rounded value 19.99 * 100 = 19.98 ,so we add 0.009 + 19.99 *100 = 19.99
                });
            } if (!$scope.Profile.isTrailUsed) {
                handler.open({
                    name: 'LeaderType',
                    currency: 'USD',
                    zipCode: false,
                    //,amount: (($scope.total + 0.009) * 100) // hack to get the rounded value 19.99 * 100 = 19.98 ,so we add 0.009 + 19.99 *100 = 19.99
                });
            }
        }

        $scope.payButtonClick = function ($event) {
            if ($scope.Profile.isTrailUsed) {
                checkOut.checkPackage($rootScope.authService.userId(), $scope.cartList[0].packageId).then(function (response) {
                    if (response.data == "Downgrade") {
                        $("#packageDowngradeWarning").modal("show");
                    } else if (response.data == "Upgrade") {
                        //$("#packageUpgradeWarning").modal("show");
                        $("#packagePaymentConfirmation").modal("show");
                    } else {
                        $("#packagePaymentConfirmation").modal("show");
                    }
                });
            } else {
                $("#packagePaymentConfirmation").modal("show");
            }
        }

        $scope.gotoAccount = function (option) {
            if(option == "Downgrade") {
                $("#packageDowngradeWarning").modal("hide");
                $cookieStore.put("cartList", "");
                $rootScope.authService.gotoAccount();
            }else {
                $scope.confirmPackage();
                $("#packageUpgradeWarning").modal("hide");
            }
        }

        window.addEventListener('popstate', function () {
            handler.close();
        });

        $scope.gotoHome = function () {
            $state.go('landingpage');
        }

        // Redirect to Privacy policy
        $scope.gotoPrivacy = function () {
            $state.go('privacy');
        }

        init();

        function calculateTotal() {
            $scope.total = 0;
            if($scope.cartList.length > 0) {
                $scope.total += parseFloat($scope.cartList[0].price);
                $scope.total = +(window.Math.round($scope.total + "e+2") + "e-2");
            }
        }

        function getOrders() {
            if ($scope.cartList && $scope.cartList.length > 0) {
                var packageId = $scope.cartList[0].packageId;
                var total = $scope.cartList[0].price;
                if (packageId) {
                    subcription.userId = $rootScope.authService.userId();
                    subcription.amount = $scope.total;
                    subcription.status = "INITIATED";
                    subcription.packageId = packageId;
                }
                //request for subscription id whenever there is a change in cart.
                checkOut.getOrderId(subcription).then(function (response) {
                    if (response.isSuccess) {
                        $scope.orderId = response.data;
                    }
                });
            }

        }

        function init() {
            // Get Profile details to check isTrailUsed or not
            profileService.get($rootScope.authService.userId()).then(function (response) {
                if (response.isSuccess) {
                    $scope.Profile = response.data;
                    if($scope.Profile.isTrailUsed) {
                        $scope.isTrail = "Pay";
                    }else {
                        $scope.isTrail = "Free";
                    }
                }
                else {
                    dialogs.error();
                    $state.go("landingpage");
                }
            });
            calculateTotal();
            //getOrders();
            $rootScope.loaderService.hide();
        }

    }
    else {
        $state.go("landingpage");
    }

});
