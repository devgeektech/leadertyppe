var app = angular.module('leaderTYPE');
// 
app.controller('resourcesCtrl', ['webService', '$scope', '$state', '$location', '$anchorScroll', '$timeout', function (webService, $scope, $state, $location, $anchorScroll, $timeout) {
    // .controller('landingPageCtrl', ['webService', 'ltiService', '$scope', '$location', '$state', 'auth', 'store', '$rootScope', '$timeout', '$cookieStore', '$anchorScroll', '$stateParams', function (webService, ltiService, $scope, $location, $state, auth, store, $rootScope, $timeout, $cookieStore, $anchorScroll, $stateParams) {

    // $location.hash('start');
    // $anchorScroll();

    $scope.emailUs = {};

    $scope.showError = false;
    $scope.sendMail = function () {
        if ($scope.sendEmail.$invalid) {
            $scope.showError = true;
            $timeout(function () {
                $scope.showError = false;
            }, 5000);
        } else {
            $scope.showError = false;
            webService.postInterestMail($scope.emailUs).then(function (response) {
                if (response.isSuccess) {
                    $scope.successMessage = "Hurrah! Your message has been sent successfully";
                    $timeout(function () {
                        $scope.successMessage = "";
                    }, 5000);
                }
                else {
                    $scope.successMessage = "Sorry,Something went wrong please try again later";
                    $timeout(function () {
                        $scope.successMessage = "";
                    }, 5000);
                }
            });
        }

    }


}]);
