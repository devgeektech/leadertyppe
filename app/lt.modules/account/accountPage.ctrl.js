'use strict';
var app = angular.module('leaderTYPE');
app.controller('accountPageCtrl', ['webService', 'checkOut', 'ltiService', '$scope', '$location', '$state', 'auth', 'store', '$rootScope', '$timeout', '$cookieStore', 'envService', 'constants', 'profileService', function (webService, checkOut, ltiService, $scope, $location, $state, auth, store, $rootScope, $timeout, $cookieStore, envService, constants, profileService) {
    if ($rootScope.authService.isAuthenticated()) {
        var dataIsEmpty = false; // to store the status of data
        var cost; // total amount to pay
        var checkedItems; // items wanted to renew
        var monthOrYear; // selected subsctiption period, Month-0 and Year-1
        var requestBean;
        var checkedPackageIds = [];
        var result;
        var currentSubscription = "Month"; //default selected subsctiption, it can be Month or Year
        var selectedObject, selectedPackageObj; //
        var orderId;
        var currentTokenId;
        var isPaymentSuccessful = false;
        var isPaymentFailed = false;

        $scope.orderId;
        $scope.selectedObject;
        $scope.selectedPackageObj;
        $scope.requestBean = { userId: "", status: "", packageIds: [], amount: "" };
        $scope.cost = 0;
        $scope.currentTokenId;
        $scope.monthOrYear = 0;
        $rootScope.loaderService.show("Loading your Packages");
        if (constants.apiUrl == "#{apiUrl}") {
            envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
                if (response && response != "") {
                    constants.apiUrl = response;
                }
                else {
                    constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
                }
                init();
            });
        }
        else {
            init();
        }


        /*
            convert the timestamp to mm-dd-yy hh:min format
        */
        $scope.formatDateAndTime = function (inputDate) {
            var date = new Date(inputDate);
            var yy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return mm + "-" + dd + "-" + yy + " " + strTime;
        }

        $scope.formatDate = function (inputDate) {
            var date = new Date(inputDate);
            var yy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            return mm + "-" + dd + "-" + yy;
        }

        /*
            to find the differents between two dates
        */
        $scope.diffDates = function (renewalDate) {
            var rdate = new Date(renewalDate);
            var sdate = new Date();
            return Math.round((rdate - sdate) / (1000 * 60 * 60 * 24));
        }

        $scope.getToken = function (token) {
            webService.getNewToken(token).then(function (response) {
                init();

            });
            return "";
        }
        $scope.renewConfirm = function () {
            $("#packageRenewalConfirmation").modal("show");
        }

        var handler = StripeCheckout.configure({
            key: 'pk_live_dToJGXm9mUPcEsfEIzLHRHAN',
            image: '/assets/images/logo.png',
            locale: 'auto',
            allowRememberMe: false,
            token: function (token) {
                $rootScope.loaderService.show("Payment in progress, please dont close the site");
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
                var paymentDetails = {};
                if (token) {
                    paymentDetails.stripeToken = token.id;
                    paymentDetails.userId = JSON.parse($cookieStore.get("profile")).identities[0].user_id;
                    paymentDetails.renewalType = currentSubscription; //Month or year based on check box
                    paymentDetails.packageId = $scope.packageData.data[0].packageId;
                    //paymentDetails.amount = $scope.total;

                    checkOut.payment(paymentDetails).then(function (response) {

                        $rootScope.loaderService.hide();
                        if (response.isSuccess) {
                            $cookieStore.put("cartList", "");
                            $scope.stripeToken = response.data;
                            if (response.data != "Token Already Available!") {
                                $("#LeaderCOACHToken-popUp").modal('show');
                            }
                            else {
                                init();
                            }

                        }
                        else {
                            $("#packageRenewalStatus").modal('show');
                        }
                    });
                }
            },
            closed: function () {
                if (!isPaymentSuccessful) {
                    isPaymentFailed = true;
                    $("#packagePaymentFailureStatus").modal("show");
                }
                else {
                    isPaymentFailed = false;
                }
            }
        });

        $scope.hideRenewStatus = function () {
            $("#packageRenewalStatus").modal('hide');
        }
        
        $scope.hideFailureStatus = function () {
            $("#packagePaymentFailureStatus").modal("hide");
        }
        /*
             it will create bean to send for payment,and perform the checkout functionality
        */
        $scope.confirmRenewPackage = function ($event) {
            $("#packageRenewalConfirmation").modal("hide");
            for(var price of $scope.packageData.data[0].priceInfo) {
                if(price.duration == currentSubscription) {
                    calculateTotal(price.price);
                }
            }
            handler.open({
                name: 'LeaderType',
                currency: 'USD',
                zipCode: false,
                amount: (($scope.total + 0.009) * 100) // hack to get the rounded value 19.99 * 100 = 19.98 ,so we add 0.009 + 19.99 *100 = 19.99
            });
        }

        function calculateTotal(price) {
            $scope.total = 0;
            $scope.total += parseFloat(price);
            $scope.total = +(window.Math.round($scope.total + "e+2") + "e-2");
        }

        /* hide the package renewal confirmation modal */
        $scope.hideRenewPackage = function () {
            $("#packageRenewalConfirmation").modal("hide");
        }
        $scope.takeMetoPackages = function () {
            $state.go("landingpage#package")
        }

        $scope.ChangeMonthOrYearly = function (value) {

            $scope.monthOrYear = value;
            if (value == 0) {
                currentSubscription = "Month";
            }
            else {
                currentSubscription = "Year";
            }
            $scope.selectRenewalItems(0, $scope.selectedObject);
        }


        var c = 0;
        $scope.checkedItems = { objects: [] };
        /*
            while checking items to renew, this function will pass the currently selected packages along with 
            current subsctiption to 'gettingCostForPackages()' function to get the appropriate cost.
            it will keep 'checkedItems' array as updated, while each check action taking place.
        */
        $scope.selectRenewalItems = function (index, obj) {

            $scope.selectedObject = obj;
            //  $scope.selectedPackageObj = packageObj;
            var count = 0, flag = true, items;

            for (items of $scope.checkedItems.objects) {

                if (items.packageId == obj.packageId || obj.token == false) {//
                    $scope.checkedItems.objects.splice(count, 1);
                    // checkedPackageIds.splice(count, 1);

                    result = gettingCostForPackages(obj, currentSubscription);
                    c = result.price;
                    flag = false;
                    break;
                }

                count++;
            }

            if (obj.token == true) {//flag==true||
                $scope.checkedItems.objects.push(obj);
                //  checkedPackageIds.push(obj.packageId);
                result = gettingCostForPackages(obj, currentSubscription);
                c = result.price;

            }
            /*
                Clearing all the items from the checkedPackageIds and adding newly selected items only
            */
            checkedPackageIds.splice(0, checkedPackageIds.length);
            for (var pid of $scope.checkedItems.objects) {
                checkedPackageIds.push(pid.packageId);
            }

            $scope.cost = c.toFixed(2);
        }


        $scope.gotoPackage = function () {
            $state.go("landingpage", { 'package': 'gotoPackage' });
        }

        $scope.gotoMyLTI = function () {
            $state.go('dashboard.mylti');
        }

        // Redirect to Privacy policy
        $scope.gotoPrivacy = function () {
            $state.go('privacy');
        }
    }
    else {
        $state.go("landingpage");
    }

    /*
        fetching packageDetails from back end
    */
    function init() {
        $rootScope.loaderService.show("Loading your Packages");
        $scope.selectedObject = '';
        $scope.selectedPackageObj = '';
        $scope.requestBean = { userId: "", status: "", packageIds: [], amount: "" };
        $scope.monthOrYear = 0;
        checkedPackageIds = [];
        $scope.checkedItems = { objects: [] };
        var item = JSON.parse($cookieStore.get("profile"));
        webService.getPackageService(item.userId).then(function (response) {
            $scope.packageData = response;
            console.log($scope.packageData);
            $scope.currentTokenId = response.token;
            if (response.data.length < 1) {
                $rootScope.loaderService.hide();
                $scope.dataIsEmpty = true;
            }
            else {
                profileService.get($rootScope.authService.userId()).then(function (response) {
                    if (response.isSuccess) {
                        $scope.isAutorenewal = response.data.isAutoRenewal;
                    }
                    $rootScope.loaderService.hide();
                });
                var cost = 0;
                angular.forEach($scope.packageData.data, function (pack) {
                    if ($scope.diffDates(pack.nextRenewalDate) <= 7) {
                        pack.token = true;
                        $scope.checkedItems.objects.push(pack);
                        checkedPackageIds.push(pack.packageId);
                        cost += gettingCostForPackages(pack, currentSubscription).price;
                    }
                });
                $scope.cost = cost.toFixed(2);
            }
        });
    }

    /*
       this function will return the cost for selected packages based on subscription criteria and 
       combo offer criteria. packageObj is the selected packages for renew and 'currentSubscription' contain currently selected subscription(Month/Year)
   */
    function gettingCostForPackages(packageObj, currentSubscription) {

        var itemCount = 0;
        /*
            finding the price of the package based on the subscription
        */
        for (var i of packageObj.priceInfo) {
            if (currentSubscription == i.duration) {
                result = i;
            }
        }
        return result;
    }

    $scope.showConfirmCancel = function() {
        $scope.expiryDate = $scope.packageData.data[0].nextRenewalDate;
        $("#cancelSubscriptionConfirm").modal("show");
    }

    $scope.confirmCancel = function() {
        $("#cancelSubscriptionConfirm").modal("hide");
        $rootScope.loaderService.show("Updating Packages...");
        var item = JSON.parse($cookieStore.get("profile"));
        checkOut.cancelSubscription(item.userId, (false)).then(function (response) {
            if (response.isSuccess) {
                init();
            }else {
                $rootScope.loaderService.hide();
            }
        });
    }

    $scope.hideConfirmCancel = function() {
        $("#cancelSubscriptionConfirm").modal("hide");
    }

}]);
