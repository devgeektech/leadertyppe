var app = angular.module('leaderTYPE');
app.controller('howItWorksCtrl', function ($scope, $state, $location, $anchorScroll) {
    
    $location.hash('start');
    $anchorScroll();

    $scope.gotoComingSoon = function () {
        $state.go('comingsoon');
    }
});