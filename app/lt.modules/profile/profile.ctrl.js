var app = angular.module('leaderTYPE');


app.controller('profileCtrl', function ($scope, $state, $rootScope, profileService, dialogs, $cookieStore, $location, constants,envService) {
    if ($rootScope.authService.isAuthenticated()) {
        $scope.isInEditMode = false;
        $scope.canEditName = function () {
            if (!$scope.isInEditMode) {
                return true;
            }
            else if ($scope.Profile.isSocialProfile) {
                return true;
            }
            else {
                return false;
            }
        };
        if (constants.apiUrl == "#{apiUrl}") {
            envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
                if (response && response != "") {
                    constants.apiUrl = response;
                }
                else {
                    constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
                }
                init();
            });
        }
        else {
            init();
        }

        function init() {
            profileService.get($rootScope.authService.userId()).then(function (response) {
                $rootScope.loaderService.hide();
                if (response.isSuccess) {
                    $scope.Profile = response.data;
                    if ($scope.Profile.phone == 0) {
                        $scope.Profile.phone = "";
                    }
                    if ($scope.Profile.yearOfBirth == 0) {
                        $scope.Profile.yearOfBirth = '';
                    }
                    else {
                        $scope.Profile.yearOfBirth = $scope.Profile.yearOfBirth.toString();
                    }

                }
                else {
                    dialogs.error();
                    $state.go("landingpage");
                }
            });
        }

        $scope.gotoMyLTI = function() {
            $state.go('dashboard.mylti');
        }

        $scope.onEdit = function () {
            $scope.isInEditMode = !$scope.isInEditMode;
        };

        $scope.onFinish = function () {
            $scope.isInEditMode = !$scope.isInEditMode;
            $rootScope.loaderService.show("Updating your profile");
            profileService.post($scope.Profile).then(function (response) {
                $rootScope.loaderService.hide();
                if (response.isSuccess) {
                    var cookieProf = JSON.parse($cookieStore.get('profile'));
                    cookieProf.user_metadata.first_name = $scope.Profile.firstName;
                    cookieProf.user_metadata.last_name = $scope.Profile.lastName;
                    cookieProf.email = $scope.Profile.email;
                    $cookieStore.put('profile', JSON.stringify(cookieProf));
                    $rootScope.authService.updateProfile();
                    $rootScope.$broadcast('dialogs.wait.complete');
                }
                else {
                    $rootScope.$broadcast('dialogs.wait.complete');
                    dialogs.error();
                }
            });
        };
        // Redirect to Privacy policy
        $scope.gotoPrivacy = function() {
            $state.go('privacy');
        }
    }
    else {
        $state.go("landingpage");
    }
});