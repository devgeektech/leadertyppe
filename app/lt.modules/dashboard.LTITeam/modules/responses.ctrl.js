var app = angular.module('leaderTYPE');

app.controller('responsesCtrl', function ($scope, $state, ltiService, authService, $rootScope, teamServices, $uibModal, profileService) {

    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.keep = true;
    $scope.start = false;
    $scope.stop = false;
    $scope.invite = {};
    $scope.responseReceived = 0;
    $scope.waitingForResponse = 0;
    $scope.totalSent = 0;
    $scope.totalFailed = 0;
    $scope.totalQueued = 0;
    $scope.totalBounced = 0;
    var totalInvite = 0;
    $scope.displayOption = "analysis";
    var rangeBarConstant = "linear-gradient(to right,{{minus2}} 25%,{{minus1}} 25%,{{minus1}} 49%,{{zero}} 49%,{{zero}} 51%,{{plus1}} 51%,{{plus1}} 75%,{{plus2}} 75% )";

    $scope.companyName = "others";
    profileService.get($rootScope.authService.userId()).then(function (response) {
        if (response.data.companyName != null && response.data.companyName != "" && response.data.companyName != undefined)
            $scope.companyName = response.data.companyName;
    });

    $scope.$on('lteamResponses', function (event, result) {
        $scope.invite = result.data;
        $scope.invite.emails = []
        if (result.data != null) {
            angular.forEach($scope.invite.categories, function (category) {
                angular.forEach(category.emails, function (email) {
                    email.category = category.category;
                    $scope.invite.emails.push(email);
                })
            });
            teamServices.getFeedbacks($rootScope.authService.userId(), $scope.invite.inviteId).then(
                function (response) {
                    if (result.data.categories) {
                        angular.forEach(result.data.categories, function (category) {
                            if (category.emails != null) {
                                $scope.totalInvite += category.emails.length;
                                angular.forEach(category.emails, function (email) {
                                    if (angular.equals(email.status, "Response received")) {
                                        $scope.responseReceived++;
                                    } else if (angular.equals(email.status, "Failed")) {
                                        $scope.totalFailed++;
                                    }
                                    else if (angular.equals(email.status, "Waiting for response")) {
                                        $scope.waitingForResponse++;
                                    }
                                    if (angular.equals(email.sent, "SENT")) {
                                        $scope.totalSent++;
                                    } else if (angular.equals(email.sent, "QUEUED")) {
                                        $scope.totalQueued++;
                                    }
                                    else if (angular.equals(email.sent, "BOUNCED")) {
                                        $scope.totalBounced++;
                                    }
                                })
                            }

                        });

                        chart(totalInvite, $scope.totalSent, $scope.responseReceived, $scope.totalFailed, $scope.totalQueued, $scope.totalBounced, $scope.waitingForResponse);
                    }

                    if (!response.isSuccess || response.data == null) {
                        $scope.showNoAnalyis = true;
                    }
                    else {
                        $scope.showNoAnalyis = false;
                        $scope.assessment = response.data;
                        if ($scope.assessment.result.keeps != null) {
                            $scope.showKeep();
                        }
                        else if ($scope.assessment.result.starts != null) {
                            $scope.showStart();
                        }
                        else if ($scope.assessment.result.stops != null) {
                            $scope.showStop();
                        }
                        angular.forEach($scope.assessment.result.assessmentSummary, function (value) {
                            createRangeBar(value.categoryDetail, value.colorCode);
                        });
                        if ($scope.assessment.result.companyAssessmentSummary != null) {
                            angular.forEach($scope.assessment.result.companyAssessmentSummary, function (value) {
                                createRangeBar(value.categoryDetail, value.colorCode);
                            });
                        }
                    }
                    $rootScope.loaderService.hide();
                },
                function error(error) {
                    $scope.showNoAnalyis = true;
                    $rootScope.loaderService.hide();
                })
        }

    });

    $scope.$on('lteamAdd', function (event, result) {
        angular.forEach(result.newEmails, function (newEmail) {
            angular.forEach(newEmail.emails, function (email) {
                var objEmail = {};
                objEmail.sent = "QUEUED";
                objEmail.category = newEmail.category;
                objEmail.status = "Waiting for response";
                objEmail.mailId = email.mailId;
                addToCategories(objEmail);
                $scope.invite.emails.push(objEmail);
            })
        })
    })

    function addToCategories(objEmail) {
        angular.forEach($scope.invite.categories, function (category) {
            if (category.category == objEmail.category) {
                category.emails.push(objEmail);
            }
        });
    }


    $scope.showKeep = function () {
        $scope.keep = true;
        $scope.start = false;
        $scope.stop = false;
    }

    $scope.showStart = function () {
        $scope.keep = false;
        $scope.start = true;
        $scope.stop = false;
    }

    $scope.showStop = function () {
        $scope.keep = false;
        $scope.start = false;
        $scope.stop = true;
    }

    $scope.onPrevKeep = function () {
        $('#CarouselKeep').carousel('prev');
    }

    $scope.onNextKeep = function () {
        $('#CarouselKeep').carousel('next');
    }

    $scope.onPrevStart = function () {
        $('#CarouselStart').carousel('prev');
    }

    $scope.onNextStart = function () {
        $('#CarouselStart').carousel('next');
    }

    $scope.onPrevStop = function () {
        $('#CarouselStop').carousel('prev');
    }

    $scope.onNextStop = function () {
        $('#CarouselStop').carousel('next');
    }

    $scope.addRecipient = function () {
        if ($scope.responseReceived < ($scope.totalSent + $scope.totalQueued)) {
            var modalInstance = $uibModal.open({
                templateUrl: 'addRecipient.html',
                controller: 'addRecepientModalCtrl',
                windowClass: 'full',
                size: 'lg',
                resolve: {
                    param: function () {
                        return { 'inviteData': $scope.invite };
                    }
                }
            });
        }
        else {
            $("#addRecepientDenyConfirmationModal").modal('show');
        }

    }

    $scope.takeSelfFeedback = function () {
        $state.go("feedback", { inviteid: $scope.invite.inviteId, emailId: $rootScope.authService.userProfile().email, category: 'Self', userid: $rootScope.authService.userId() })
    }

    $scope.backToReports = function () {
        $rootScope.$broadcast('myltteam', { type: 'init', tab: 'responsesTab' });
    }

    $scope.yesDenyRecepient = function () {
        $("#addRecepientDenyConfirmationModal").modal('hide');
    }

    $scope.deleteInvite = function () {
        $("#deleteConfirmationModal").modal('show');
    }

    $scope.noDelete = function () {
        $("#deleteConfirmationModal").modal('hide');
    }

    $scope.yesDelete = function () {
        $("#deleteConfirmationModal").modal('hide');
        teamServices.deleteInvite($scope.invite.inviteId).then(function (result) {
            if (result.isSuccess) {
                $rootScope.$broadcast('myltteam', { type: 'init', tab: 'responsesTab' });
            }
            else {
                toastr.options.timeOut = 1000;
                toastr.error("Something went wrong, please try again later");
            }
        })

        //here we delete and send back to reports
    }

    function chart(totalInvite, inviteSent, responseReceived, totalFailed, totalQueued, totalBounced, totalWaitingForResponse) {
        var type = "PieChart";
        var options = {
            chartArea: { top: 15, bottom: 10, height: "90%", width: "90%" },
            pieHole: 0.4,
            colors: ['#2ecc71', '#e74c3c', '#e74c3c'],
            fontSize: 13
        };

        $scope.inviteChart = {};
        $scope.inviteChart.type = type;
        $scope.inviteChart.options = options;
        $scope.inviteChart.data = [
            ['Category', 'value'],
            ['Sent', parseInt(inviteSent)],
            ['Bounced', parseInt(totalBounced)],
            ['Queued', parseInt(totalQueued)]
        ];

        $scope.responseChart = {};
        $scope.responseChart.type = type;
        $scope.responseChart.options = options;
        $scope.responseChart.data = [
            ['Category', 'value'],
            ['Recieved', parseInt(responseReceived)],
            ['Waiting for response', parseInt(totalWaitingForResponse)],
            ['Failed', parseInt(totalFailed)]
        ];

    }

    function createRangeBar(categories, colorCode) {
        angular.forEach(categories, function (category) {
            if (category != null) {
                category.colorStrip = rangeBarConstant.replace("{{zero}}", colorCode).replace("{{zero}}", colorCode)
                if (category.min == -1) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", colorCode).replace("{{minus1}}", colorCode).replace("{{minus2}}", "#FFFFFF");
                }
                else if (category.min == -2) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", colorCode).replace("{{minus1}}", colorCode).replace("{{minus2}}", colorCode);
                }
                else if (category.min == 0 || category.min == 1 || category.min == 2) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", "#FFFFFF").replace("{{minus1}}", "#FFFFFF").replace("{{minus2}}", "#FFFFFF");

                }

                if (category.max == 1) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", colorCode).replace("{{plus1}}", colorCode).replace("{{plus2}}", "#FFFFFF");
                }
                else if (category.max == 2) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", colorCode).replace("{{plus1}}", colorCode).replace("{{plus2}}", colorCode);
                }
                else if (category.max == 0 || category.max == -1 || category.max == -2) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", "#FFFFFF").replace("{{plus1}}", "#FFFFFF").replace("{{plus2}}", "#FFFFFF");
                }
            }

        });
    }

    // Accordion view
    $scope.better = false;
    $scope.muchBetter = false;
    $scope.asExpected = false;
    $scope.less = false;
    $scope.muchLess = false;
    $scope.trust = false;
    $scope.communication = false;
    $scope.toggleAccordion = function (key) {
        if (angular.equals("better", key)) {
            if ($scope.better)
                $scope.better = false;
            else
                $scope.better = true;
        } else if (angular.equals("muchBetter", key)) {
            if ($scope.muchBetter)
                $scope.muchBetter = false;
            else
                $scope.muchBetter = true;
        } else if (angular.equals("asExpected", key)) {
            if ($scope.asExpected)
                $scope.asExpected = false;
            else
                $scope.asExpected = true;
        } else if (angular.equals("less", key)) {
            if ($scope.less)
                $scope.less = false;
            else
                $scope.less = true;
        } else if (angular.equals("muchLess", key)) {
            if ($scope.muchLess)
                $scope.muchLess = false;
            else
                $scope.muchLess = true;
        } else if (angular.equals("trust", key)) {
            if ($scope.trust)
                $scope.trust = false;
            else
                $scope.trust = true;
        } else if (angular.equals("communication", key)) {
            if ($scope.communication)
                $scope.communication = false;
            else
                $scope.communication = true;
        }
    }

});

app.controller('addRecepientModalCtrl', function ($uibModalInstance, $scope, $rootScope, $http, param, teamServices) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.addModels = [];
    $scope.selectedCategory = "";
    $scope.categoryList = [];
    $scope.followerEmail = "";
    $scope.leaderEmail = "";

    $scope.inviteData = param.inviteData;
    angular.forEach($scope.inviteData.categories, function (category) {
        var addModel = {};
        $scope.categoryList.push(category.category);
        addModel.category = category.category;
        addModel.emails = [];
        addModel.availableEmails = category.emails;
        $scope.addModels.push(addModel);
    })

    $scope.selectedCategory = $scope.categoryList[0].category;
    $scope.emails = [];
    $scope.enableSubmit = true;

    $scope.leaderEmailError = true;
    $scope.followerEmailError = true;

    $scope.getEmails = function (category) {
        $scope.emails = [];
        angular.forEach($scope.addModels, function (model) {
            if (model.category.toLowerCase() == category.toLowerCase()) {
                angular.forEach(model.emails, function (email) {
                    $scope.emails.push(email.mailId);
                });
            }
        });
    }

    $scope.removeEmail = function (email, category) {
        var emailIndex = getIndexOf($scope.emails, email, "");
        $scope.emails.splice(emailIndex, 1);
        angular.forEach($scope.addModels, function (model) {
            if (model.category.toLowerCase() == category.toLowerCase()) {
                angular.forEach(model.emails, function (modelEmail) {
                    if (modelEmail.mailId == email) {
                        var index = getIndexOf(model.emails, modelEmail.mailId, "mailId");
                        model.emails.splice(index, 1);
                    }
                });
            }
        });
    }

    /**
     * Email validation while email-enter
     */
    $scope.emailValidation = function (email, category) {
        // define single email validator here
        var re = /\S+@\S+\.\S+/;
        var validityArr = re.test(email.trim());
        if (validityArr) {
            $scope.leaderEmailError = true;
            $scope.followerEmailError = true;
            var isError = false;
            $scope.errorMessage = "";
            var emailObj = {};
            emailObj.mailId = email;
            angular.forEach($scope.addModels, function (model) {
                if (model.category.toLowerCase() == category.toLowerCase()) {
                    angular.forEach(model.availableEmails, function (availableEmail) {
                        if (availableEmail.mailId == email) {
                            isError = true;
                            $scope.errorMessage = "Email is already available while creating invite.";
                        }
                    })
                    if (!getIsAvailable(model.emails, email, "mailId") && !isError) {
                        if (!isError) {
                            model.emails.push(emailObj);
                        }
                    }
                    else if ($scope.errorMessage == "") {
                        isError = true;
                        $scope.errorMessage = "Email already added.";
                    }
                }
            });

            if (!isError) {
                $scope.emails.push(email);
            }
            else {
                if (category == "leader") {
                    $scope.leaderEmailError = false;
                }
                else {
                    $scope.followerEmailError = false;
                }
            }

            if (category == "leader") {
                $scope.leaderEmail = "";
            }
            else {
                $scope.followerEmail = "";
            }
        } else {
            $scope.errorMessage = "Please enter valid EmailId";
            if (category == "leader") {
                $scope.leaderEmailError = false;
            }
            else {
                $scope.followerEmailError = false;
            }
        }
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
            k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (prop != "") {
                if (arr[k][prop] === val) {
                    return k;
                }
            }
            else {
                if (arr[k] === val) {
                    return k;
                }
            }

        }
        return -1;
    }

    function getIsAvailable(arr, val, prop) {
        return getIndexOf(arr, val, prop) >= 0;
    }

    /**
     * Email validation while ng-blur
     */
    $scope.blurValidation = function (email, category) {
        if (email.length > 0) {
            $scope.emailValidation(email, category);
        } else {
            $scope.followerEmailError = true;
            $scope.leaderEmailError = true;
        }
    }

    $scope.submit = function () {
        var hasEmails = false;
        var sendModels = [];
        angular.forEach($scope.addModels, function (addModel) {
            var sendModel = {};
            sendModel.category = addModel.category;
            sendModel.emails = addModel.emails;
            sendModels.push(sendModel);
            if (addModel.emails.length > 0) {
                hasEmails = true;
            }
        });

        if (hasEmails && (!$scope.followerEmail && !$scope.leaderEmail)) {
            $uibModalInstance.dismiss('cancel');
            $rootScope.loaderService.show("Adding Recepients to invite")

            teamServices.updateInvite($scope.inviteData.inviteId, sendModels).then(function (result) {
                $rootScope.loaderService.hide();
                if (!result.isSuccess) {
                    toastr.options.timeOut = 1000;
                    toastr.error("Something went wrong, please try again later");
                }
                else {
                    $rootScope.$broadcast("lteamAdd", { newEmails: sendModels });
                }
            })
        } else {
            $scope.followerEmailError = false;
            $scope.leaderEmailError = false;
            $scope.errorMessage = "Please add recepients";
        }
    }
});