var app = angular.module('leaderTYPE');

app.controller('inviteCtrl', function ($scope, $state, ltiService, authService, $rootScope, teamServices) {
    var tab = "reportTab";
    $scope.backToReports = function () {
        $("#discardConfirmationModal").modal('show');
    }

    $scope.noDiscard = function () {
        $("#discardConfirmationModal").modal('hide');
    }

    $scope.yesDiscard = function () {
        $("#discardConfirmationModal").modal('hide');
        $rootScope.$broadcast('myltteam', { type: 'init', tab: tab });
    }

    $rootScope.$on('lteamInvite', function (event, result) {
        tab = result.tab;
    });

    $scope.error = {};
    $scope.error.title = true;
    $scope.error.categories = [];
    $scope.error.categories[0] = {};
    $scope.error.categories[1] = {};
    $scope.error.categories[0].emailError = true;
    $scope.error.categories[0].subjectError = true;
    $scope.error.categories[0].messageError = true;
    $scope.error.categories[1].emailError = true;
    $scope.error.categories[1].subjectError = true;
    $scope.error.categories[1].messageError = true;

    $scope.$watch('invite.title', function (title) {
        if (!$scope.error.title) {
            $scope.error.title = title != undefined && title != "";
        }
    });

    $scope.$watch('invite.categories[1].emails', function (newEmail, oldEmail) {
        if (!$scope.error.categories[1].emailError) {
            $scope.error.categories[1].emailError = $scope.invite.categories[1].emails.length > 0;
        }
    }, true);

    $scope.$watch('invite.categories[1].message', function (message) {
        if (!$scope.error.categories[1].messageError) {
            $scope.error.categories[1].messageError = message != undefined && message.length > 0;
        }
    }, true);

    $scope.$watch('invite.categories[1].subject', function (subject) {
        if (!$scope.error.categories[1].subjectError) {
            $scope.error.categories[1].subjectError = subject != undefined && subject != "";
        }
    }, true);

    $scope.$watch('invite.categories[0].emails', function (newEmail, oldEmail) {
        if (!$scope.error.categories[0].emailError) {
            $scope.error.categories[0].emailError = $scope.invite.categories[0].emails.length > 0;
        }
    }, true);

    $scope.$watch('invite.categories[0].message', function (message) {
        if (!$scope.error.categories[0].messageError) {
            $scope.error.categories[0].messageError = message != undefined && message.length > 0;
        }
    }, true);

    $scope.$watch('invite.categories[0].subject', function (subject) {
        if (!$scope.error.categories[0].subjectError) {
            $scope.error.categories[0].subjectError = subject != undefined && subject != "";
        }
    }, true);

    $scope.invite = {};
    $scope.invite.title = "";
    $scope.invite.categories = [];
    $scope.invite.categories[0] = {};
    $scope.invite.categories[0].category = "Leader";
    $scope.invite.categories[0].emails = [];
    $scope.invite.categories[0].message = "Hello,\r\n\r\nAs part of my ongoing development as a leader, I need your feedback. Only by being made aware how I’m doing can I improve.\r\n\r\nPlease respond to the questions in this survey with the following assurance: LeaderType.com will not reveal—nor will I seek to find out—how you individually answered these questions. Your ratings will be aggregated with others, and displayed in summary form. Your specific recommendations as to what I should keep doing, start doing and stop doing will be displayed in random order to protect anonymity.\r\n\r\nThank you in advance for taking the time to provide me candid feedback.";
    $scope.invite.categories[0].subject = "";
    $scope.invite.categories[0].leaderLevel = "1";
    $scope.invite.categories[1] = {};
    $scope.invite.categories[1].category = "Follower(s)";
    $scope.invite.categories[1].emails = [];
    $scope.invite.categories[1].message = "Hello,\r\n\r\nAs part of my ongoing development as a leader, I need your feedback. Only by being made aware how I’m doing can I improve.\r\n\r\nPlease respond to the questions in this survey with the following assurance: LeaderType.com will not reveal—nor will I seek to find out—how you individually answered these questions. Your ratings will be aggregated with others, and displayed in summary form. Your specific recommendations as to what I should keep doing, start doing and stop doing will be displayed in random order to protect anonymity.\r\n\r\nThank you in advance for taking the time to provide me candid feedback.";
    $scope.invite.categories[1].subject = "";
    $scope.invite.categories[1].leaderLevel = "1";
    $scope.leaderEmailError = false;
    $scope.followerEmailError = false;
    $scope.emails = [];
    $scope.leaderEmail = '';
    $scope.followerEmail = '';
    $scope.leaderExist = false;
    $scope.followerExist = false;

    $scope.defaultSubject = function () {
        if ($scope.invite.title.length > 0) {
            if ($scope.invite.categories[0].subject.length == 0)
                $scope.invite.categories[0].subject = angular.copy($scope.invite.title);
            if ($scope.invite.categories[1].subject.length == 0)
                $scope.invite.categories[1].subject = angular.copy($scope.invite.title);
        }
    }

    /**
     * Email validation while email-enter
     */
    $scope.emailValidation = function (email, category) {
        $scope.leaderExist = false;
        $scope.followerExist = false;
        // define single email validator here
        var re = /\S+@\S+\.\S+/;
        var validityArr = re.test(email.trim());
        if (validityArr) {
            $scope.leaderEmailError = false;
            $scope.followerEmailError = false;
            var emailObj = {};
            emailObj.mailId = email;
            if (category == "leader") {
                angular.forEach($scope.invite.categories[0].emails, function (emails) {
                    if (angular.equals(emails.mailId, emailObj.mailId)) {
                        $scope.leaderExist = true;
                    }
                });
                if (!$scope.leaderExist) {
                    $scope.invite.categories[0].emails.push(emailObj);
                    $scope.leaderEmail = '';
                }
            }
            else {
                angular.forEach($scope.invite.categories[1].emails, function (emails) {
                    if (angular.equals(emails.mailId, emailObj.mailId)) {
                        $scope.followerExist = true;
                    }
                });
                if (!$scope.followerExist) {
                    $scope.invite.categories[1].emails.push(emailObj);
                    $scope.followerEmail = '';
                }
            }
        } else {
            if (category == "leader") {
                $scope.leaderEmailError = true;
            }
            else {
                $scope.followerEmailError = true;
            }
        }
    }

    $scope.removeEmail = function (email, category) {
        if (category == "leader") {
            var leaderIndex = getIndexOf($scope.invite.categories[0].emails, email.mailId, "mailId");
            if (leaderIndex >= 0) {
                $scope.invite.categories[0].emails.splice(leaderIndex, 1);
            }
        }
        else {
            var followerIndex = getIndexOf($scope.invite.categories[1].emails, email.mailId, "mailId");
            if (followerIndex >= 0) {
                $scope.invite.categories[1].emails.splice(followerIndex, 1);
            }
        }
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
            k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] === val) {
                return k;
            }
        }
        return -1;
    }

    /**
     * Email validation while ng-blur
     */
    $scope.blurValidation = function (email, category) {
        if (email.length > 0) {
            $scope.emailValidation(email, category);
        }
    }

    $scope.resetLeaderForm = function () {
        $scope.invite.categories[0] = {};
        $scope.invite.categories[0].category = "Leader";
        $scope.invite.categories[0].emails = [];
        $scope.invite.categories[0].message = "";
        $scope.invite.categories[0].subject = "";
        $scope.invite.categories[0].leaderLevel = "1";
    }

    $scope.resetFollowerForm = function () {
        $scope.invite.categories[1] = {};
        $scope.invite.categories[1].category = "Follower(s)";
        $scope.invite.categories[1].emails = [];
        $scope.invite.categories[1].message = "";
        $scope.invite.categories[1].subject = "";
        $scope.invite.categories[1].leaderLevel = "1";
    }

    function validateLeader() {
        $scope.error.categories[0].emailError = $scope.invite.categories[0].emails.length > 0;
        $scope.error.categories[0].subjectError = $scope.invite.categories[0].subject != "";
        $scope.error.categories[0].messageError = $scope.invite.categories[0].message.length > 0;
        return ($scope.error.categories[0].emailError && $scope.error.categories[0].subjectError && $scope.error.categories[0].messageError);
    }

    function validateFollower() {
        $scope.error.categories[1].emailError = $scope.invite.categories[1].emails.length > 0;
        $scope.error.categories[1].subjectError = $scope.invite.categories[1].subject != "";
        $scope.error.categories[1].messageError = $scope.invite.categories[1].message.length > 0;
        return ($scope.error.categories[1].emailError && $scope.error.categories[1].subjectError && $scope.error.categories[1].messageError);
    }

    function dummyCategory(index) {
        var categories = {};
        categories.category = "";
        if (index == 1)
            categories.category = "Follower(s)";
        else
            categories.category = "Leader";
        categories.emails = [];
        categories.message = "";
        categories.subject = "";
        categories.leaderLevel = "1";
        return categories;
    }

    $scope.noSubmit = function () {
        $("#submitConfirmationModal").modal('hide');
    }

    $scope.yesSubmit = function () {
        $("#submitConfirmationModal").modal('hide');
        var isValid = false;
        $scope.error.title = $scope.invite.title != undefined && $scope.invite.title != "";
        var leader = validateLeader();
        var follower = validateFollower();
        isValid = (leader && $scope.error.title) || (follower && $scope.error.title);
        if (($scope.invite.categories[0].emails.length > 0 && $scope.invite.categories[0].subject == "")
            || ($scope.invite.categories[0].emails.length > 0 && $scope.invite.categories[0].message.length == 0)) {
            isValid = false;
        }
        if (($scope.invite.categories[1].emails.length > 0 && $scope.invite.categories[1].subject == "")
            || ($scope.invite.categories[1].emails.length > 0 && $scope.invite.categories[1].message.length == 0)) {
            isValid = false;
        }
        if (isValid) {
            $rootScope.loaderService.show("Sending Invite");
            var categoryOne = false;
            var categoryTwo = false;
            // Delete empty category
            if (($scope.invite.categories[0].emails.length == 0) || (($scope.invite.categories[0].emails.length > 0) && (!$scope.error.categories[0].subjectError))
                || ((!$scope.error.categories[0].subjectError) && (!$scope.error.categories[0].messageError))) {
                $scope.invite.categories.splice(0, 1);
                categoryOne = true;
            } else if (($scope.invite.categories[1].emails.length == 0) || (($scope.invite.categories[1].emails.length > 0) && (!$scope.error.categories[1].subjectError))
                || ((!$scope.error.categories[1].subjectError) && (!$scope.error.categories[1].messageError))) {
                $scope.invite.categories.splice(1, 1);
                categoryTwo = true;
            }

            $scope.inviteFormData = angular.copy($scope.invite);

            // Add deleted category
            if (categoryOne)
                $scope.invite.categories.splice(0, 0, dummyCategory(0));
            else if (categoryTwo)
                $scope.invite.categories.splice(1, 0, dummyCategory(1));

            $scope.inviteFormData.userId = $rootScope.authService.userId();
            teamServices.postInvite($scope.inviteFormData).then(
                function success(response) {
                    if (response.isSuccess) {
                        $rootScope.loaderService.hide();
                        $rootScope.$broadcast('myltteam', { type: 'init', tab: tab });
                    }
                    else {
                        $rootScope.loaderService.hide();
                        toastr.options.timeOut = 1000;
                        toastr.error("Something went wrong, please try again later");
                    }
                }, function error(error) {
                    $rootScope.loaderService.hide();
                    toastr.options.timeOut = 1000;
                    toastr.error("Something went wrong, please try again later");
                }
            )
        }
        //here we delete and send back to reports
    }

    /**
     * Function to sent invite request to service
     */
    $scope.sendInvite = function () {
        $("#submitConfirmationModal").modal('show');
    }

});