var app = angular.module('leaderTYPE');

app.controller('lteamReportCtrl', function ($scope, $state, ltiService, authService, $rootScope, profileService, teamServices, $filter, $timeout, $location) {
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.showNoAnalyis = false;
    $scope.keep = true;
    $scope.start = true;
    $scope.stop = true;
    $scope.selectedInvite = "1";
    $scope.displayOption = "analysis";
    $scope.selectedReport = null;
    var rangeBarConstant = "linear-gradient(to right,{{minus2}} 25%,{{minus1}} 25%,{{minus1}} 49%,{{zero}} 49%,{{zero}} 51%,{{plus1}} 51%,{{plus1}} 75%,{{plus2}} 75% )";

    $scope.companyName = "others";
    profileService.get($rootScope.authService.userId()).then(function (response) {
        if (response.data.companyName != null && response.data.companyName != "" && response.data.companyName != undefined)
            $scope.companyName = response.data.companyName;
    });

    $scope.$on('lteamReport', function (event, result) {
        $scope.invites = result.data;
        $scope.inviteList = angular.copy($scope.invites);

        $timeout(function () {
            $("#" + result.tab).click();
        }, 1000);
        angular.forEach($scope.invites, function (invite) {
            var count = 0;
            var bounced = 0;
            var emailCount = 0;
            var categoryTitle = [];
            angular.forEach(invite.categories, function (categoryDetail) {
                if (categoryDetail.emails != null) {
                    emailCount += categoryDetail.emails.length;
                    categoryTitle.push(categoryDetail.category);
                    angular.forEach(categoryDetail.emails, function (email) {
                        if ((email.status == "Response received" || email.status == "Waiting for Response") && (email.sent == "SENT" || email.sent == "QUEUED")) {
                            count++;
                        }
                        else if (email.sent == "BOUNCED") {
                            bounced++;
                        }
                    });
                }
            });

            if (count == (emailCount - bounced)) {
                invite.status = "CLOSED";
            }
            else {
                invite.status = "OPEN";
            }

            invite.emailCount = emailCount;
            invite.categoryList = categoryTitle.join(",");
            invite.lastModifiedDate = $filter('ordinalDate')(invite.modifiedAt, 'd MMMM yyyy');
        });
        if ($scope.invites != null) {
            getFeedbacks();
        }
    });

    $scope.showKeep = function () {
        $scope.keep = true;
        $scope.start = false;
        $scope.stop = false;
    }

    $scope.showStart = function () {
        $scope.keep = false;
        $scope.start = true;
        $scope.stop = false;
    }

    $scope.showStop = function () {
        $scope.keep = false;
        $scope.start = false;
        $scope.stop = true;
    }

    $scope.refresh = function () {
        $scope.selectedInvite = "1";
        $rootScope.$broadcast('myltteam', { type: 'init', tab: $location.hash() + 'Tab' });
    }

    $scope.createInvite = function () {
        $rootScope.$broadcast('myltteam', { type: 'inviteTeam', tab: $location.hash() + 'Tab' });
    }

    $scope.showResponseItemDetails = function (invite) {
        $rootScope.$broadcast('myltteam', { type: 'responsesTeam', data: invite });
    }

    $scope.onPrevKeep = function () {
        $('#CarouselKeep').carousel('prev');
    }

    $scope.onNextKeep = function () {
        $('#CarouselKeep').carousel('next');
    }

    $scope.onPrevStart = function () {
        $('#CarouselStart').carousel('prev');
    }

    $scope.onNextStart = function () {
        $('#CarouselStart').carousel('next');
    }

    $scope.onPrevStop = function () {
        $('#CarouselStop').carousel('prev');
    }

    $scope.onNextStop = function () {
        $('#CarouselStop').carousel('next');
    }

    $scope.getFeedbackByInvite = function (inviteId) {
        $rootScope.loaderService.show("Loading selected invite report...");
        if (angular.equals(inviteId, "1")) {
            getFeedbacks();
        } else {
            teamServices.getFeedbacks($rootScope.authService.userId(), inviteId).then(
                function success(response) {
                    setFeedbackReport(response);
                },
                function error(error) {
                    $scope.showNoAnalyis = true;
                    $rootScope.loaderService.hide();
                }
            )
        }
    }

    function getFeedbacks() {
        teamServices.getFeedbacks($rootScope.authService.userId()).then(
            function success(response) {
                setFeedbackReport(response);
            },
            function error(error) {
                $scope.showNoAnalyis = true;
                $rootScope.loaderService.hide();
            }
        )
    }

    function setFeedbackReport(response) {
        if (!response.isSuccess || response.data == null) {
            $scope.showNoAnalyis = true;
            $rootScope.loaderService.hide();

            if ($scope.inviteList.length > 0 && $scope.selectedInvite == "1") {
                var title = "Latest - " + $scope.inviteList[0].title;
                $scope.inviteList[0].title = title;
                $scope.selectedInvite = $scope.inviteList[0].inviteId;
            }
        } else {
            $scope.showNoAnalyis = false;
            $scope.assessment = response.data;
            if ($scope.selectedInvite == "1") {
                $scope.selectedInvite = $scope.assessment.inviteId;
                angular.forEach($scope.inviteList, function (invite, $index) {
                    if ($scope.selectedInvite == invite.inviteId) {
                        var title = "Latest - " + $scope.inviteList[$index].title;
                        $scope.inviteList[$index].title = title;
                        arraymove($scope.inviteList, $index, 0);
                    }
                })
            }
            else {
                $scope.selectedInvite = $scope.assessment.inviteId;
            }

            if ($scope.assessment.result.keeps != null) {
                $scope.showKeep();
            }
            else if ($scope.assessment.result.starts != null) {
                $scope.showStart();
            }
            else if ($scope.assessment.result.stops != null) {
                $scope.showStop();
            }
            angular.forEach($scope.assessment.result.assessmentSummary, function (value) {
                createRangeBar(value.categoryDetail, value.colorCode);
            });
            if ($scope.assessment.result.companyAssessmentSummary != null) {
                angular.forEach($scope.assessment.result.companyAssessmentSummary, function (value) {
                    createRangeBar(value.categoryDetail, value.colorCode);
                });
            }
            var count = 0;
            angular.forEach($scope.invites, function (invite) {
                if ($scope.selectedInvite == "1") {
                    if (angular.equals($scope.selectedInvite, invite.inviteId)) {
                        $scope.selectedReport = invite;
                        count++;
                    } else if (count == 0) {
                        $scope.selectedReport = null;
                    }
                }
                else {
                    if (angular.equals($scope.selectedInvite, invite.inviteId)) {
                        $scope.selectedReport = invite;
                        count++;
                    } else if (count == 0) {
                        $scope.selectedReport = null;
                    }
                }

            })
            $rootScope.loaderService.hide();
        }
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
            k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] === val) {
                return k;
            }
        }
        return -1;
    }

    function arraymove(arr, fromIndex, toIndex) {
        var element = arr[fromIndex];
        arr.splice(fromIndex, 1);
        arr.splice(toIndex, 0, element);
    }

    function createRangeBar(categories, colorCode) {
        angular.forEach(categories, function (category) {
            if (category != null) {
                category.colorStrip = rangeBarConstant.replace("{{zero}}", colorCode).replace("{{zero}}", colorCode)
                if (category.min == -1) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", colorCode).replace("{{minus1}}", colorCode).replace("{{minus2}}", "#FFFFFF");
                }
                else if (category.min == -2) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", colorCode).replace("{{minus1}}", colorCode).replace("{{minus2}}", colorCode);
                }
                else if (category.min == 0 || category.min == 1 || category.min == 2) {
                    category.colorStrip = category.colorStrip.replace("{{minus1}}", "#FFFFFF").replace("{{minus1}}", "#FFFFFF").replace("{{minus2}}", "#FFFFFF");

                }

                if (category.max == 1) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", colorCode).replace("{{plus1}}", colorCode).replace("{{plus2}}", "#FFFFFF");
                }
                else if (category.max == 2) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", colorCode).replace("{{plus1}}", colorCode).replace("{{plus2}}", colorCode);
                }
                else if (category.max == 0 || category.max == -1 || category.max == -2) {
                    category.colorStrip = category.colorStrip.replace("{{plus1}}", "#FFFFFF").replace("{{plus1}}", "#FFFFFF").replace("{{plus2}}", "#FFFFFF");
                }
            }
        });
    }

    // Accordion view
    $scope.better = false;
    $scope.muchBetter = false;
    $scope.asExpected = false;
    $scope.less = false;
    $scope.muchLess = false;
    $scope.trust = false;
    $scope.communication = false;
    $scope.toggleAccordion = function (key) {
        if (angular.equals("better", key)) {
            if ($scope.better)
                $scope.better = false;
            else
                $scope.better = true;
        } else if (angular.equals("muchBetter", key)) {
            if ($scope.muchBetter)
                $scope.muchBetter = false;
            else
                $scope.muchBetter = true;
        } else if (angular.equals("asExpected", key)) {
            if ($scope.asExpected)
                $scope.asExpected = false;
            else
                $scope.asExpected = true;
        } else if (angular.equals("less", key)) {
            if ($scope.less)
                $scope.less = false;
            else
                $scope.less = true;
        } else if (angular.equals("muchLess", key)) {
            if ($scope.muchLess)
                $scope.muchLess = false;
            else
                $scope.muchLess = true;
        } else if (angular.equals("trust", key)) {
            if ($scope.trust)
                $scope.trust = false;
            else
                $scope.trust = true;
        } else if (angular.equals("communication", key)) {
            if ($scope.communication)
                $scope.communication = false;
            else
                $scope.communication = true;
        }
    }
});