var app = angular.module('leaderTYPE');

app.controller('feedbackCtrl', function ($scope, $state, ltiService, authService, $rootScope, teamServices, $location) {

    $scope.inviteeName = "";
    $scope.roleSet = [];
    $scope.questionSet = [];
    $scope.questionPageCount = 0;
    $scope.percentage = 0;
    $scope.invalid = false;
    $scope.isOnReset = false;
    $scope.pageArray = [true];
    $scope.feedback = {};
    $scope.feedback.questionDetails = [];
    $scope.nextPageFunction = function () {
        if ($scope.pageArray.length - 2 > $scope.questionPageCount) {
            $scope.pageArray[$scope.questionPageCount] = false;
            $scope.questionPageCount = $scope.questionPageCount + 1;
            $scope.pageArray[$scope.questionPageCount] = true;
        }

        $scope.percentage = Math.round(100 * $scope.questionPageCount / ($scope.pageArray.length - 2))
    }
    $scope.submitFunction = function () {
        $("#submitConfirmationModal").modal('show');

    }

    $scope.yesSubmit = function () {
        $("#submitConfirmationModal").modal('hide');
        $rootScope.loaderService.show("Submitting your feedback");
        angular.forEach($scope.questionSet, function (questionSet) {
            angular.forEach(questionSet, function (subSet) {
                $scope.feedback.questionDetails.push(subSet);
            });
        });

        teamServices.postFeedback($scope.feedback).then(function (result) {
            $rootScope.loaderService.hide();
            if (result.isSuccess) {
                $scope.pageArray[$scope.questionPageCount] = false;
                $scope.questionPageCount = $scope.questionPageCount + 1;
                $scope.pageArray[$scope.questionPageCount] = true;
            }
            else {
                $("#errorConfirmationModal").modal('show');
            }
        });
    }

    $scope.cancelFunction = function () {
        $("#cancelConfirmationModal").modal('show');
    }
    $scope.yesCancel = function () {
        $("#cancelConfirmationModal").modal('hide');
        $state.go("dashboard.myltteam");
    }

    $scope.noCancel = function () {
        $("#cancelConfirmationModal").modal('hide');
    }


    $scope.yesError = function () {
        $("#errorConfirmationModal").modal('hide');
    }

    $scope.noSubmit = function () {
        $("#submitConfirmationModal").modal('hide');
    }

    $scope.gotoLeaderTeam = function () {
        $state.go("dashboard.myltteam");
    }
    $scope.prevPageFunction = function () {
        if ($scope.questionPageCount >= 0) {
            $scope.pageArray[$scope.questionPageCount] = false;
            $scope.questionPageCount = $scope.questionPageCount - 1;
            $scope.pageArray[$scope.questionPageCount] = true;
        }
        $scope.percentage = Math.round(100 * $scope.questionPageCount / ($scope.pageArray.length - 2))
    }

    $scope.resetFunction = function () {
        $scope.isOnReset = true;
        $scope.invalid = false;
        $scope.roleSet = [];
        $scope.questionSet = [];
        $scope.questionPageCount = 0;
        $scope.pageArray = [true];
        $scope.feedback = {};
        $scope.feedback.questionDetails = [];
        $scope.percentage = 0;
        init(true)
    }

    function init(isReset) {
        if (isReset) {
            $rootScope.loaderService.show("Resetting your Invite");
        }
        else {
            $rootScope.loaderService.show("Loading your Invite");
        }

        var inviteId = $location.search().inviteid;
        var emailId = $location.search().emailId;
        var queryUserId = $location.search().userid;
        var queryCategory = $location.search().category;
        if (inviteId == null || emailId == null || queryCategory == null) {
            $scope.invalid = true;
        }
        else {
            $scope.feedback.category = queryCategory;
            $scope.feedback.inviteId = inviteId;
            $scope.feedback.email = emailId;
            $scope.questionPageCount = 0;
            $scope.roleSet = [];
            $scope.questionSet = [];
            var isOwner = (queryUserId != null) ? true : false;
            var userId = isOwner ? $rootScope.authService.userId() : null;
            teamServices.getQuestions(inviteId, emailId, userId, queryCategory).then(function (result) {
                if (result == null || result.data == null || result.data.questions == null) {
                    $scope.invalid = true;
                    if (isOwner) {
                        $scope.inviteeName = "you";
                    }
                    $rootScope.loaderService.hide();
                }
                else {
                    if (isOwner) {
                        $scope.inviteeName = "you";
                    }
                    else {
                        $scope.inviteeName = result.data.firstName;
                    }

                    var questions = result.data.questions;

                    var roles = [];
                    var questionsGroup = [];
                    angular.forEach(questions, function (question) {
                        if (roles.indexOf(question.role) < 0) {
                            roles.push(question.role);
                        }
                        if (questionsGroup[question.role] == null || questionsGroup[question.role] == undefined) {
                            questionsGroup[question.role] = {};
                            questionsGroup[question.role].data = [];
                        }
                        questionsGroup[question.role].data.push(question);
                    });

                    angular.forEach(roles, function (role) {
                        var roleQuestions = questionsGroup[role].data;

                        var arrays = [], size = 3;
                        while (roleQuestions.length > 0)
                            arrays.push(roleQuestions.splice(0, size));

                        angular.forEach(arrays, function (quesSet) {
                            $scope.questionSet.push(quesSet);
                            $scope.roleSet.push(role);
                            $scope.pageArray.push(false);
                        });
                    });
                    $scope.pageArray.push(false);
                    $rootScope.loaderService.hide();
                    $scope.isOnReset = false;
                }
            });
        }
    }

    init(false);


});