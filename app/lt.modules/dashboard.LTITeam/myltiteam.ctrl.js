var app = angular.module('leaderTYPE');

app.controller('myltiteamCtrl', function ($scope, $state, ltiService, authService, $rootScope, teamServices, constants, $timeout, webService) {

  $rootScope.loaderService.show("Loading My LeaderTeam");
  $rootScope.heading = "MY LEADER TEAM"
  $rootScope.brandclassName = "customHeaderForOtherDasboardPage"
  $scope.showResponsesTeam = false;
  $scope.showInviteTeam = false;
  $scope.showReportTeam = false;
  $scope.showDefaultViewTeam = false;
  $scope.subscribedPackages = [false, false];

  $scope.$watch('showResponsesTeam', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showInviteTeam = false;
      $scope.showReportTeam = false;
      $scope.showDefaultViewTeam = false;
    }
  });
  $scope.$watch('showInviteTeam', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showResponsesTeam = false;
      $scope.showReportTeam = false;
      $scope.showDefaultViewTeam = false;
    }
  });
  $scope.$watch('showReportTeam', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showResponsesTeam = false;
      $scope.showInviteTeam = false;
      $scope.showDefaultViewTeam = false;
    }
  });

  $scope.$watch('showDefaultViewTeam', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showResponsesTeam = false;
      $scope.showInviteTeam = false;
      $scope.showReportTeam = false;
    }
  });

  $scope.$on('myltteam', function (event, args) {
    switch (args.type) {
      case "init":
        $rootScope.loaderService.show("Loading My LeaderTeam");
        init(args.tab);
        break;
      case "inviteTeam":
        $scope.showInviteTeam = true;
        $timeout(function () {
          $rootScope.$broadcast('lteamInvite', { tab: args.tab })
        });
        break;
      case "reportTeam":
        $scope.showReportTeam = true;
        break;
      case "responsesTeam":
        $rootScope.loaderService.show("Loading Responses");
        $scope.showResponsesTeam = true;
        $timeout(function () {
          $rootScope.$broadcast('lteamResponses', { data: args.data });
        });
        break;
    }
  });

  function init(tab) {
    $rootScope.loaderService.show("Loading My LeaderTEAM");
    webService.getPackageService($rootScope.authService.userId()).then(function (response) {
      var highestPackage = null;
      if (response != null && response.data.length > 0) {
        var isValid = (new Date(response.data[0].nextRenewalDate) - new Date()) > 0;
        if ((response.data[0].packageName == "PLATINUM"
          || response.data[0].packageName == "PREMIUM") && isValid) {
          highestPackage = response.data[0];
        }
      }
      if (highestPackage != null && (highestPackage.packageName == "PLATINUM" || highestPackage.packageName == "PREMIUM")) {
        $scope.subscribedPackages[0] = true;
        if ((new Date(highestPackage.nextRenewalDate) - new Date()) < 0) {
          $scope.subscribedPackages[1] = true;
        }
      }
      if ($scope.subscribedPackages[0] == true && !$scope.subscribedPackages[1]) {
        if ($rootScope.authService.isAuthenticated()) {
          teamServices.getInvite($rootScope.authService.userId()).then(function (result) {
            if (result == null || result.data == null || (result.data != null && result.data.length == 0)) {
              $scope.showDefaultViewTeam = true;
              $rootScope.loaderService.hide();
            } else {
              $rootScope.loaderService.show("Loading My LeaderTeam report");
              $scope.showReportTeam = true;
              $timeout(function () {
                $rootScope.$broadcast('lteamReport', { data: result.data, tab: tab });
              });
            }
          })
        } else {
          $rootScope.loaderService.hide();
          $rootScope.authService.gotoHome();
        }
      } else {
        $rootScope.loaderService.hide();
      }
    });
  }

  $scope.gotoPackage = function () {
    $state.go('landingpage', { 'package': true });
  }

  if (constants.apiUrl == "#{apiUrl}") {
    envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
      if (response && response != "") {
        constants.apiUrl = response;
      }
      else {
        constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
      }
      init('reportTab');
    });
  } else {
    init('reportTab');
  }

});
