var app = angular.module('leaderTYPE');
app.controller('reportsCtrl', function ($scope, $state, $rootScope, $location, $anchorScroll, reportService) {

    $scope.view = 'Users';
    $scope.editFlag = false;
    $scope.editCampaign = {};
    $scope.isLoading = true;
    $scope.totalPages = 0;
    $scope.pageNo = 0;
    $location.hash('start');
    $anchorScroll();

    onLoad();
    function onLoad() {
        if (!$rootScope.authService.isShowReports()) {
            $state.go('landingpage');
        }
    }

    $scope.changeTab = function (view) {
        $scope.isLoading = true;
        if (view === 'Users') {
            getReport();
        } else if (view === 'Campaign') {
            getCampaign();
        }
        $scope.view = view;
    }

    getReport();
    function getReport() {
        reportService.getReport($scope.pageNo).then(function (response) {
            $scope.userList = response;
            $scope.totalPages = Math.floor(response.total / response.limit);
            $scope.isLoading = false;
        });
    }

    $scope.prevPage= function() {
        $scope.isLoading = true;
        if($scope.pageNo > 0)
        {
            $scope.pageNo--;
            getReport();
        }
    }

    $scope.nextPage= function() {
        $scope.isLoading = true;
        if($scope.pageNo < $scope.totalPages)
        {
            $scope.pageNo++;
            getReport();
        }
    }

    function resetEditCampaign() {
        $scope.editCampaign.line1 = '';
        $scope.editCampaign.line2 = '';
        $scope.editCampaign.line3 = '';
        $scope.editCampaign.url = '';
    }

    function getCampaign() {
        reportService.getCampaign().then(function (response) {
            if (response === '') {
                $scope.campaign = null;
            } else {
                $scope.campaign = response;
            }
            $scope.isLoading = false;
        });
    }

    $scope.showEdit = function () {
        $scope.editCampaign = JSON.parse(JSON.stringify($scope.campaign));
        $scope.editFlag = true;
    }

    $scope.showAdd = function () {
        resetEditCampaign();
        $scope.editFlag = true;
    }

    $scope.hideEdit = function () {
        resetEditCampaign();
        $scope.editFlag = false;
    }

    $scope.saveCampaign = function () {
        if($scope.editCampaign.url && $scope.editCampaign.url.length > 0)
        {
            if($scope.editCampaign.url.indexOf("http") <0)
            {
                $scope.editCampaign.url = "http://"+$scope.editCampaign.url;
            }
        }
        reportService.addCampaign($scope.editCampaign).then(function (response) {
            console.log('save', response);
            getCampaign();
            $scope.hideEdit();
        })
    }

    $scope.showDelete = function () {
        $("#deleteConfirmation").modal("show");
    }

    $scope.hideDelete = function () {
        $("#deleteConfirmation").modal("hide");
    }

    $scope.deleteCampaign = function () {
        reportService.deleteCampaign().then(function (response) {
            console.log(response);
            getCampaign();
            $scope.hideDelete();
        });
    }

    /*
    *  convert the timestamp to mm-dd-yy hh:min format
    */
    $scope.formatDateAndTime = function (inputDate) {
        var date = new Date(inputDate);
        var yy = date.getFullYear();
        var mm = date.getMonth() + 1;
        var dd = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return mm + "-" + dd + "-" + yy + " " + strTime;
    }
});