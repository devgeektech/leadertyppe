var app = angular.module('leaderTYPE');
app.controller('myLtiCtrl', function ($scope, $state, ltiService, authService, $rootScope, envService, constants) {
  $rootScope.loaderService.show("Loading My LeaderType");
  $rootScope.$broadcast("dashboard", "mylti");
  
  $scope.$watch('showLanding', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showSlider = false;
      $scope.showReport = false;
      $scope.showResult = false;
      $rootScope.takingAssessment = false;
    }
  });

  $scope.$watch('showSlider', function (newValue, oldValue, scope) {

    if (newValue) {
      $scope.showLanding = false;
      $scope.showReport = false;
      $scope.showResult = false;

    }
  });

  $scope.$watch('showReport', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showLanding = false;
      $scope.showSlider = false;
      $scope.showResult = false;
      $rootScope.takingAssessment = false;
    }
  });

  $scope.$watch('showResult', function (newValue, oldValue, scope) {
    if (newValue) {
      $scope.showLanding = false;
      $scope.showSlider = false;
      $scope.showReport = false;
      $rootScope.takingAssessment = false;
    }
  });

  // Redirect to Privacy policy
  $scope.gotoPrivacy = function () {
    $state.go('privacy');
  }

  $scope.showLanding = false;
  $scope.showSlider = false;
  $scope.showReport = false;
  $scope.showResult = false;
  $rootScope.sliderFlag = false;
  $rootScope.takingAssessment = false;
  if (constants.apiUrl == "#{apiUrl}") {
    envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
      if (response && response != "") {
        constants.apiUrl = response;
      }
      else {
        constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
      }
      init();
    });
  }
  else {
    init();
  }

  function init() {
    if ($rootScope.authService.isAuthenticated()) {
      ltiService.hasAssessment($rootScope.authService.userId()).then(function (response) {

        if (response.isSuccess) {
          if (response.data) {
            $scope.showReport = true;
            setTimeout(function () {
              $scope.$apply(function () {
                $rootScope.$broadcast('ltiReport', null);
              });
            }, 1000);

          }
          else {
            $scope.showLanding = true;
          }
        }

      });
    }
    else {
      $scope.showSlider = true;
      $rootScope.takingAssessment = true;
    }
  }


  $scope.$on('mylti', function (event, args) {
    switch (args.type) {
      case "ltiLanding":
        $scope.showLanding = true;
        $rootScope.sliderFlag = false;
        break;
      case "ltiResult":
        $scope.showResult = true;
        $rootScope.sliderFlag = false;
        setTimeout(function () {
          $scope.$apply(function () {
            $rootScope.$broadcast('ltiResult', args.data);
          });
        }, 1000);
        break;

      case "ltiSlider":
        $rootScope.sliderFlag = true;
        $scope.showSlider = true;
        $rootScope.takingAssessment = true;
        break;

      case "ltiResult_SelectLeaderType":
        $rootScope.sliderFlag = false;
        $scope.showReport = true;
        setTimeout(function () {
          $scope.$apply(function () {
            $rootScope.$broadcast('ltiReport', null);
          });
        }, 1000);
        break
    }

  });

});
