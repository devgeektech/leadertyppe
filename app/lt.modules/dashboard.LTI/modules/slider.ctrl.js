var app = angular.module('leaderTYPE');


app.controller('sliderCtrl', function ($scope, $state, ltiService, $rootScope,$cookieStore) {

  $scope.isOnReset=false;
  $scope.currentN=0;
  $scope.prev=false;
  $rootScope.$on('sliderCheckFormModified', function (event, val) {
    if ($scope.changeForm != undefined) {
      if (!$scope.changeForm.modified) {
        $rootScope.$broadcast("confirmcancel", val);
      } else {
        $("#exitPageConfirmationModal").modal("show");
        $scope.confirmExit = function () {
          $rootScope.sliderFlag = false;
          $("#exitPageConfirmationModal").modal("hide");
        }
        $scope.hideExitModal = function () {
          $("#exitPageConfirmationModal").modal("hide");
          $rootScope.$broadcast("confirmcancel", val);
        }
      }
    }else {
      $rootScope.$broadcast("confirmcancel", val);
    }
  });
  $scope.$on('onLoginSuccess', function (args) {
    $rootScope.loaderService.hide();
  });

  $rootScope.$on("onEmailFailed", function () {
    $state.go('noemail')
  });

  $rootScope.$on("onLoginFailed", function () {
    $rootScope.loaderService.hide();
  });

  $rootScope.$on("onEmailVerification", function () {
    $state.go('verifyemail');

  });

  $scope.submit = function () {
    $cookieStore.remove('dummy_id');
    $rootScope.loaderService.show("Calculating your assessment");
    var index = 0;
    var payload = {};
    payload.userId = $rootScope.authService.userId();
    payload.steps = $scope.ltiSet;
    payload.results = null;
    payload.selectedLeaderType = null;
    payload.createdDate = null;
    ltiService.post(payload).then(function (response) {
      $rootScope.$broadcast('mylti', { data: response.data, type: 'ltiResult' });
    });

  }

  $scope.range = function () {
    var min = $scope.startNum;
    var max = $scope.startNum+$scope.questionPerPage-1;
    $scope.endNum=$scope.startNum+$scope.questionPerPage-1;
    var step = 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
      input.push(i);
    }
    return input;
  };

  $scope.getLtiQuestions = function () {
    $scope.ltiSet = [];
    ltiService.get().then(function (response) {
      if (response.isSuccess) {
        response.data[0].questions.length == 16 ? $scope.ltiSet[1] = response.data[0] : $scope.ltiSet[0] = response.data[0];
        response.data[1].questions.length == 24 ? $scope.ltiSet[0] = response.data[1] : $scope.ltiSet[1] = response.data[1];
        angular.forEach($scope.ltiSet, function (value) {
          $scope.totalquest += value.questions.length;
          angular.forEach(value.questions, function (ques) {
            ques.value = 0.0;
          });
        });
        $scope.percentage = Math.round(100 / ($scope.totalquest-2));
        $scope.incrementor = Math.round($scope.percentage * 100 * ($scope.questionPerPage)) / 100;
        $scope.getContent(0);
        $rootScope.loaderService.hide();
        $scope.isOnReset = false;
        if ($scope.changeForm != undefined)
            $scope.changeForm.$setPristine();
      }
    });
  }

  $scope.resetPage = function () {
    $scope.isOnReset = true;
    init();
  }

  $scope.getContent = function (i) {
    if (i < $scope.questionPerPage) {
      $scope.key = 0;
      $scope.prevPa = null;
      $scope.isCanPrev = false;
      $scope.isCanNext = true;
      $scope.nextPage = $scope.questionPerPage;
      $scope.dis = false;
    }

    else if (i >= ($scope.totalquest - ($scope.totalquest % $scope.questionPerPage)) - 7) {
      $scope.key = i;
      $scope.prevPa = i - $scope.questionPerPage;
      $scope.isCanPrev = true;
      $scope.isCanNext = false;
      $scope.nextPage = null;
      $scope.dis = true;
    }

    else {
      $scope.prevPa = i - $scope.questionPerPage;
      $scope.isCanPrev = true;
      $scope.isCanNext = true;
      $scope.nextPage = i + $scope.questionPerPage;
    }
    $scope.inputs = $scope.range();
  };


  $scope.nextPagefunc = function (e) {
    for(var i of $scope.ltiSet ){
      if($scope.endNum>=i.questions.length-3){
        $scope.questionPerPage=4;

      }
    else
       $scope.questionPerPage=3;

      break;
    }
    $scope.percentage += $scope.incrementor;
    if ($scope.percentage > 100) {
      $scope.percentage = 100;
    }
    $scope.startNum = $scope.endNum + 1;
    $scope.endNum = $scope.startNum + ($scope.questionPerPage - 1);
    $scope.getContent($scope.startNum);
  }



  $scope.prevPage = function () {

    for(var i of $scope.ltiSet ){
      if($scope.endNum-4>=i.questions.length){
        $scope.questionPerPage=4;
         $scope.prev=true;
      }
    else{
       $scope.questionPerPage=3;
    }
      break;
    }

    $scope.startNum = $scope.startNum - $scope.questionPerPage;
    $scope.endNum = $scope.endNum - $scope.questionPerPage;
    $scope.getContent($scope.startNum);
    if ($scope.percentage >= 100) {
      $scope.percentage -= $scope.incrementor / $scope.questionPerPage;
    } else if($scope.startNum == 0){
       $scope.percentage =(100 / $scope.totalquest);
    } else {
      $scope.percentage -= $scope.incrementor;
    }
  }



  $scope.onChange = function (valNum) {
    $scope.questionPerPage = valNum;
    $scope.startNum = $scope.startNum;
    $scope.endNum = $scope.startNum + (valNum - 1);
  }

  function init() {
    $scope.totalquest = 0;
    $scope.questionPerPage = 3;
    $scope.startNum = 0;
    $scope.endNum = $scope.questionPerPage - 1;
    $scope.inputs = $scope.range();
    $scope.getLtiQuestions();
  }
  init();
});
