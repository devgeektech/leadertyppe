var app = angular.module('leaderTYPE');


app.controller('resultCtrl', function ($scope, $state, $rootScope, ltiService, dialogs, $uibModal, $cookieStore) {
    var $ctrl = this;
    $scope.showModal = function () {
        $("#image-popup").modal('show');
    }

    $scope.selectedLeader = {};
    $scope.$on('onLoginSuccess', function (args) {
        $rootScope.loaderService.hide();
    });

    $rootScope.$on("onEmailFailed", function () {
        $state.go('noemail')
    });

    $rootScope.$on("onLoginFailed", function () {
        $rootScope.loaderService.hide();
    });


    $scope.$on('ltiResult', function (event, result) {
        if (!$rootScope.authService.isAuthenticated()) {
            $cookieStore.put("dummy_id", result.userId);
        }
        $scope.leadertypes = [];
        $scope.managerPercentages = [];

        if (result.indicators) {
            var i = 0;
            result.indicators.forEach(function (indicator) {
                $scope.leadertypes.push(indicator.indicatorId);
                $scope.managerPercentages.push(indicator.managerPercentage);
            });
        }
        else {
            $scope.leadertypes = result.result.leadertypes;
            $scope.managerPercentages = result.result.managerPercentages;
        }
        $rootScope.loaderService.hide();
    });

    $scope.showLeaderDetails = function (id) {
        ltiService.getLTIndicator(id).then(function (response) {
            if (response.isSuccess) {
                $scope.selectedLeader = response.data;
                $("#image-popup").modal('show');
            }
            else {
                dialogs.error();
            }
        });
    }

    $scope.retakeAssessment = function () {
        $rootScope.takingAssessment = true;
        $rootScope.$broadcast('mylti', { type: 'ltiSlider' });
    }

    $scope.selectLeader = function (id) {
        if ($rootScope.authService.isAuthenticated()) {
            selectLeaderType(id, $rootScope.authService.userId(), 0);
        }
        else {

            $rootScope.authService.loginWithSignUp();
            $scope.$on('onLoginSuccess', function (args) {
                selectLeaderType(id, $cookieStore.get("dummy_id"), 0);
            });

            $scope.$on('onEmailVerification', function (args) {
                selectLeaderType(id, $cookieStore.get("dummy_id"), 1);
            });
        }

    }

    function selectLeaderType(id, userId, type) {
        $rootScope.loaderService.show("Updating your leadertype");
        ltiService.setLeaderType(id, userId).then(function (response) {
            if (response.isSuccess) {
                var dummy_id = $cookieStore.get("dummy_id");
                if (dummy_id) {
                    updateUserId(dummy_id, $rootScope.authService.userId(), type);
                }
                else {
                    if (type == 0) {
                        $rootScope.$broadcast('mylti', { type: 'ltiResult_SelectLeaderType', data: null });
                    }
                    else if (type == 1) {
                        $rootScope.authService.reset();
                        $state.go("verifyemail");
                    }
                }
            }
            else {
                dialogs.error();
            }
        });
    }

    function updateUserId(dummyId, userId, type) {
        ltiService.updateGuestAssessment(dummyId, userId).then(function (response) {
            if (response.isSuccess) {
                if (response.data) {
                    $cookieStore.put("dummy_id", "");
                    if (type == 0) {
                        $rootScope.$broadcast('mylti', { type: 'ltiResult_SelectLeaderType', data: null });
                    }
                    else {
                        $rootScope.authService.reset();
                        $state.go("verifyemail");
                    }
                }
                else {
                    dialogs.error();
                }
            }
        })

    }

    $scope.gotoMethodology = function () {
        $state.go('methodology');
    }
});

