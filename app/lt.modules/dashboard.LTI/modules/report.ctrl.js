var app = angular.module('leaderTYPE');


app.controller('reportCtrl', function ($scope, $state, ltiService, $rootScope) {
    
    $scope.report = {};
    $rootScope.$on('onLoginSuccess', function (args) {
        $rootScope.loaderService.hide();
    });

    $rootScope.$on("onEmailFailed", function () {
        $state.go('noemail')
    });

    $rootScope.$on("onLoginFailed", function () {
        $rootScope.loaderService.hide();
    });

    $rootScope.$on("onEmailVerification", function () {
        $state.go('verifyemail');

    });
    
    $rootScope.$on('ltiReport', function (event, result) {
        // if (result == null) {
            ltiService.getAssessment($rootScope.authService.userId(), "lti").then(function (response) {
                if (response.isSuccess) {
                    if (response.data.selectedLeaderType == null) {
                        $rootScope.$broadcast('mylti', { type: 'ltiResult', data: response.data });
                    }
                    else {
                        $scope.report = response.data;
                        $rootScope.loaderService.hide();
                    }
                }
                else {
                    $rootScope.$broadcast('mylti', { type: 'ltiLanding' });
                }
            });
        // }
    });

    $scope.retakeAssessment = function () {
        $rootScope.takingAssessment=true;
        $rootScope.$broadcast('mylti', { type: 'ltiSlider' });
    }

    $scope.showLeaderDetails = function (id) {
        ltiService.getLTIndicator(id).then(function (response) {
            if (response.isSuccess) {
                $scope.selectedLeader = response.data;
                // var modalInstance = $uibModal.open({
                //     animation: true,
                //     templateUrl: 'leaderModal.html',
                //     component: 'modalComponent',
                //     scope:$scope
                // });
                $("#image-popup").modal('show');
                //$scope.showModal();
            }
            else {
                dialogs.error();
            }
        });
    }
    $scope.selectLeaderType = function (id) {
        $rootScope.loaderService.show("Updating your leadertype");
        ltiService.setLeaderType(id, $rootScope.authService.userId()).then(function (response) {
            if (response.isSuccess) {
                ltiService.getAssessment($rootScope.authService.userId(), "lti").then(function (response) {
                    $rootScope.loaderService.hide();
                    if (response.isSuccess) {
                        if (response.data.selectedLeaderType == null) {
                            $rootScope.$broadcast('mylti', { type: 'ltiResult', data: response.data });
                        }
                        else {
                            $scope.report = response.data;
                        }
                    }
                    else {
                        $rootScope.$broadcast('mylti', { type: 'ltiLanding' });
                    }
                });
            }
            else {
                dialogs.error();
            }
        });
    }
});