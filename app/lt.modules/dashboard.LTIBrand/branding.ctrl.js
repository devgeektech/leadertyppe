var app = angular.module('leaderTYPE');
app.controller('brandingCtrl', function ($scope, $rootScope, $uibModal, $http, brandingServices, $state, envService, constants, $cookieStore, webService, $sce) {
    $rootScope.loaderService.show("Loading My LeaderBrand");
    //$rootScope.loaderService.show("Fetching subscription details");
    $rootScope.$broadcast("dashboard", "myltbrand");
    $scope.mbtiIsDisabled = true;
    $scope.brandingPageData = [];
    $scope.bestFitType = [];
    $scope.showNeedsChartFlag = false;
    $scope.showTriggerChartFlag = false;
    $scope.showTkiChartFlag = false;
    $scope.detailFrame = $sce.trustAsResourceUrl("http://www.scarfsolutions.com/SelfAssessment.aspx");
    $scope.mbtiValid = true;
    $scope.rangeColor = ["grey-range", "grey-range", "grey-range", "grey-range"];
    $scope.isSorted = false;
    $scope.leaderTypeDictionary = {
        'lt8_name': 'lt5',
        'lt3_name': 'lt2_name',
        'lt7': 'lt6_name',
        'lt4_name': 'lt1',
    }
    $rootScope.$on("change", function () {
        $scope.changeForm.modified = true;
    });

    /*$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      if ($scope.changeForm != undefined) {
        if ($scope.changeForm.modified) {
          event.preventDefault();
          $(window).trigger('beforeunload');
        }
      }
    });*/
    /*window.onhashchange = function() {
     console.log("asd");
   };*/
    window.onbeforeunload = function (e) {
        var message = "Your confirmation message goes here.",
            e = e || window.event;
        // For IE and Firefox
        if (e) {
            e.returnValue = message;
        }
        // For Safari
        return message;
    };
    $scope.subscribedPackages = [false, false];

    function getElement(object, value) {
        if (object[value] !== undefined) {
            return object[value];
        } else {
            return Object.keys(object).find(key => object[key] === value);
        }
    }

    /**
     *false for edit mode
     *true for save
     **/
    function initforBrandData() {
        $scope.brandAssessmentFlag = true;
        brandingServices.getBrandAssessment($rootScope.authService.userId()).then(function (response) {
            if (!response.data) {
                $rootScope.loaderService.show("Loading My LeaderType");
                $rootScope.authService.gotoDashboard();
            } else {
                $scope.mainBrandingData = response.data;
                delete $scope.mainBrandingData.objectId;
                $scope.brandingPageData = angular.copy(response.data.results);

                var size = $scope.brandingPageData.leadertypes.length;
                if ($scope.brandingPageData.leadertypeValues.length < 8) {
                    var appendZerosLength = 8 - $scope.brandingPageData.leadertypeValues.length;
                    $scope.brandingPageData.leadertypeValues = $scope.brandingPageData.leadertypeValues.concat(new Array(appendZerosLength).fill(0));
                }
                for (var index = 0; index < size / 2; index++) {
                    var value = $scope.brandingPageData.leadertypes[index];
                    if (getElement($scope.leaderTypeDictionary, value) === $scope.brandingPageData.leadertypes[size - 1 - index]) {
                        continue;
                    } else {
                        var var_index = $scope.brandingPageData.leadertypes.indexOf(getElement($scope.brandingPageData.leadertypes, value));
                        if (var_index >= 4 && var_index <= 7) {

                            //Swap Values Array
                            $scope.brandingPageData.leadertypeValues[size - 1 - index] =
                                [$scope.brandingPageData.leadertypeValues[var_index], $scope.brandingPageData.leadertypeValues[var_index] = $scope.brandingPageData.leadertypeValues[size - 1 - index]][0];

                            //Swap Text Array
                            $scope.brandingPageData.leadertypes[size - 1 - index] =
                                [$scope.brandingPageData.leadertypes[var_index], $scope.brandingPageData.leadertypes[var_index] = $scope.brandingPageData.leadertypes[size - 1 - index]][0];
                        }
                    }
                }
                $scope.isSorted = true;


                if (!$scope.brandingPageData.values) {
                    $scope.brandingPageData.values = null;
                }
                if (!$scope.brandingPageData.leadershipQualities) {
                    $scope.brandingPageData.leadershipQualities = null;
                }
                if (!$scope.brandingPageData.gallup) {
                    $scope.brandingPageData.gallup = [];
                }
                if (!$scope.brandingPageData.needs) {
                    $scope.brandingPageData.needs = null;
                } else {
                    $rootScope.$emit("loadNeedChart", {});
                }
                if (!$scope.brandingPageData.triggers) {
                    $scope.brandingPageData.triggers = null;
                } else {
                    $rootScope.$emit("loadTriggerChar", {});
                }
                if (!$scope.brandingPageData.tki) {
                    $scope.brandingPageData.tki = null;
                } else {
                    $rootScope.$emit("loadTkiChart", {});
                }
                brandingServices.getBrandKind("bestfit").then(function (response) {
                    $scope.bestFitType = response.data;
                    $scope.bestFitValuesWithIndicatorId = [];
                    var i = 0;
                    angular.forEach($scope.bestFitType, function (bestFitValues) {
                        $scope.bestFitValuesWithIndicatorId[bestFitValues.indicatorId] = i;
                        i++;
                    });
                    if (!$scope.brandingPageData.bestFit) {
                        $scope.brandingPageData.bestFit = null;
                    } else {
                        $scope.bestFitMappingDropdown = $scope.bestFitValuesWithIndicatorId[$scope.brandingPageData.bestFit.indicatorId] + "";
                        $scope.bestFitImg = "../assets/images/latest_images/" + $scope.brandingPageData.bestFit.indicatorId + ".png";
                        $scope.bestFitValues = $scope.brandingPageData.bestFit.values;
                    }
                    if ($scope.changeForm != undefined)
                        $scope.changeForm.$setPristine();
                });
                if (!$scope.brandingPageData.mbti) {
                    $scope.brandingPageData.mbti = null;
                }
                brandingServices.getBrandKind("Gallup").then(function (response) {
                    $scope.gallUpStrengthOptions = response.data.values;
                });
                brandingServices.getBrandKind("MBTI").then(function (response) {
                    if (!$scope.brandingPageData.mbti) {
                        $scope.brandingPageData.mbti = response.data;
                        for (var i = 0; i < $scope.brandingPageData.mbti.length; i++) {
                            $scope.brandingPageData.mbti[0].value = 0;
                        }
                    }
                    if ($scope.changeForm != undefined)
                        $scope.changeForm.$setPristine();
                });
                brandingServices.getBrandKind("bestfit").then(function (response) {
                    $scope.bestFitType = response.data;
                    $scope.bestFitValuesWithIndicatorId = [];
                    angular.forEach($scope.bestFitType, function (bestFitValues) {
                        $scope.bestFitValuesWithIndicatorId[bestFitValues.indicatorId] = bestFitValues.values;
                    });
                    if ($scope.changeForm != undefined)
                        $rootScope.loaderService.hide();
                    $scope.copyBrandingPageData = $scope.brandingPageData;
                    if ($scope.changeForm != undefined)
                        $scope.changeForm.$setPristine();
                });
                $scope.changeForm.$setPristine();
            }
        });
    }
    if (constants.apiUrl == "#{apiUrl}") {
        envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
            if (response && response != "") {
                constants.apiUrl = response;
            }
            else {
                constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
            }
            init();
        });
    }
    else {
        init();
    }
    function init() {

        $rootScope.loaderService.show("Loading My LeaderBrand");
        var item = JSON.parse($cookieStore.get("profile"));

        webService.getPackageService(item.userId).then(function (response) {
            $rootScope.loaderService.hide();
            var highestPackage = null;
            angular.forEach(response.data, function (pack) {
                var isValid = (new Date(pack.nextRenewalDate) - new Date()) > 0;
                if (pack.packageName != "BASIC" && isValid) {
                    highestPackage = pack;
                }
            })

            if (highestPackage != null && highestPackage.packageName != "BASIC") {
                $scope.subscribedPackages[0] = true;
                if ((new Date(highestPackage.nextRenewalDate) - new Date()) < 0) {
                    $scope.subscribedPackages[1] = true;
                }
            }
            $scope.currentTokenId = response.token;
            if (response.data.length < 1) {
                $scope.dataIsEmpty = true;
            }

            if ($scope.subscribedPackages[0] == true && !$scope.subscribedPackages[1]) {
                // $rootScope.loaderService.show("Loading My LeaderBrand");
                initforBrandData();
                $scope.showNeedsChart = false;
                $scope.showTriggerChart = false;
                $scope.showTbChart = false;
                $scope.gallupStrengthIsDisabled = true;
                $scope.bestFitDisabled = true;
                $scope.bestFitValues = [];
                $scope.bestFitImg = null;
                $scope.pageHeading = "MY LEADERBRAND";
                //$rootScope.loaderService.hide();
            }
        });



    }
    $scope.gotoPackage = function () {

        $state.go('landingpage', { 'package': true });
    }
    // $rootScope.$on('saveEditClick', function (event, saveEditFlag) {
    //   //$scope.mainBrandingData.results = $scope.brandingPageData;
    //   if (saveEditFlag.saveEditFlag) {
    //     //on save is cliked
    //     $scope.brandAssessmentFlag = true;
    //     //update branding page
    //     $scope.copyBrandingPageData = $scope.mainBrandingData;
    //     $rootScope.loaderService.show("Saving your LeaderBrand");
    //     brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
    //       initforBrandData();
    //     });
    //   } else {
    //     //on edit is clicked
    //     $scope.brandAssessmentFlag = false;
    //   }
    // });

    $rootScope.$on('brandCheckFormModified', function (event, val) {
        console.log("cancelClick-brand");
        if ($scope.changeForm != undefined) {
            if (!$scope.changeForm.modified) {
                $rootScope.$broadcast("confirmcancel", val);
            } else {
                $("#exitPageConfirmationModal").modal("show");
                $scope.confirmExit = function () {
                    $("#exitPageConfirmationModal").modal("hide");
                }
                $scope.hideExitModal = function () {
                    $("#exitPageConfirmationModal").modal("hide");
                    $rootScope.$broadcast("confirmcancel", val);
                }
            }
        } else {
            $rootScope.$broadcast("confirmcancel", val);
        }
    });

    $scope.valueSortSaveEditFlag = false;
    $scope.leadershipQualitiesSaveEditFlag = false;
    $scope.gallupStrengthSaveEditFlag = false;
    $scope.mbtiSaveEditFlag = false;
    $scope.needsSaveEditFlag = false;
    $scope.triggersSaveEditFlag = false;
    $scope.tkiSaveEditFlag = false;
    $scope.bestFitSaveEditFlag = false;

    // Value Sort Operation
    $scope.valueSortEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.values = [];
            $scope.brandingPageData.values = angular.copy($scope.mainBrandingData.results.values);
            $scope.valueSortSaveEditFlag = false;
        } else {
            if ($scope.valueSortSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.values = angular.copy($scope.brandingPageData.values);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.valueSortSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.valueSortSaveEditFlag = true;
            }
        }
    }

    // Leadership Qualities operation
    $scope.leadershipEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.leadershipQualities = {};
            $scope.brandingPageData.leadershipQualities = angular.copy($scope.mainBrandingData.results.leadershipQualities);
            $scope.leadershipQualitiesSaveEditFlag = false;
        } else {
            if ($scope.leadershipQualitiesSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.leadershipQualities = angular.copy($scope.brandingPageData.leadershipQualities);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.leadershipQualitiesSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.leadershipQualitiesSaveEditFlag = true;
            }
        }
    }

    // Gallup operation
    $scope.gallupEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.gallup = [];
            $scope.brandingPageData.gallup = angular.copy($scope.mainBrandingData.results.gallup);
            $scope.gallupStrengthSaveEditFlag = false;
        } else {
            if ($scope.gallupStrengthSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.gallup = angular.copy($scope.brandingPageData.gallup);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.gallupStrengthSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.gallupStrengthSaveEditFlag = true;
            }
        }
    }

    // Mbti operation
    $scope.mbtiEditSaveClick = function (type) {
        $scope.mbtiValid = true;
        if (type == 'cancel') {
            if ($scope.mainBrandingData.results.mbti == null) {
                $scope.mainBrandingData.results.mbti = angular.copy($scope.brandingPageData.mbti);
            }
            $scope.brandingPageData.mbti = [];
            $scope.brandingPageData.mbti = angular.copy($scope.mainBrandingData.results.mbti);
            $scope.mbtiSaveEditFlag = false;
        } else {
            if ($scope.mbtiSaveEditFlag) {
                $scope.brandingPageData.mbti.forEach(function (mbti) {
                    if (mbti.value == null || mbti.value == 0) {
                        $scope.mbtiValid = false;
                    }
                });
                if ($scope.mbtiValid) {
                    //save clicked
                    $scope.mainBrandingData.results.mbti = angular.copy($scope.brandingPageData.mbti);
                    $rootScope.loaderService.show("Saving your LeaderBrand");
                    brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                        $scope.mbtiSaveEditFlag = false;
                        $rootScope.loaderService.hide();
                    });
                }
            } else {
                //edit clicked
                $scope.mbtiSaveEditFlag = true;
            }
        }
    }

    // Needs Operation
    $scope.needsEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.needs = [];
            $scope.brandingPageData.needs = angular.copy($scope.mainBrandingData.results.needs);
            $scope.needsSaveEditFlag = false;
        } else {
            if ($scope.needsSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.needs = angular.copy($scope.brandingPageData.needs);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.needsSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.needsSaveEditFlag = true;
            }
        }
    }

    // Trigger operation
    $scope.triggersEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.triggers = [];
            $scope.brandingPageData.triggers = angular.copy($scope.mainBrandingData.results.triggers);
            $scope.triggersSaveEditFlag = false;
        } else {
            if ($scope.triggersSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.triggers = angular.copy($scope.brandingPageData.triggers);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.triggersSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.triggersSaveEditFlag = true;
            }
        }
    }

    // TKI operation
    $scope.tkiEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.tki = [];
            $scope.brandingPageData.tki = angular.copy($scope.mainBrandingData.results.tki);
            $scope.tkiSaveEditFlag = false;
        } else {
            if ($scope.tkiSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.tki = angular.copy($scope.brandingPageData.tki);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.tkiSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.tkiSaveEditFlag = true;
            }
        }
    }

    // BestFit operation
    $scope.bestFitEditSaveClick = function (type) {
        if (type == 'cancel') {
            $scope.brandingPageData.bestFit = [];
            $scope.brandingPageData.bestFit = angular.copy($scope.mainBrandingData.results.bestFit);
            $scope.bestFitSaveEditFlag = false;
        } else {
            if ($scope.bestFitSaveEditFlag) {
                //save clicked
                $scope.mainBrandingData.results.bestFit = angular.copy($scope.brandingPageData.bestFit);
                $rootScope.loaderService.show("Saving your LeaderBrand");
                brandingServices.updateAssessment($scope.mainBrandingData).then(function (res) {
                    $scope.bestFitSaveEditFlag = false;
                    $rootScope.loaderService.hide();
                });
            } else {
                //edit clicked
                $scope.bestFitSaveEditFlag = true;
            }
        }
    }

    $scope.bestFitChange = function () {
        if ($scope.bestFitMappingDropdown != "") {
            $scope.brandingPageData.bestFit = {};
            $scope.brandingPageData.bestFit = $scope.bestFitType[$scope.bestFitMappingDropdown];
            $scope.bestFitImg = "../assets/images/latest_images/" + $scope.bestFitType[$scope.bestFitMappingDropdown].indicatorId + ".png";
            $scope.bestFitValues = $scope.bestFitType[$scope.bestFitMappingDropdown].values;
        }
    };
    $scope.filter1 = function (item) {
        return (item != $scope.brandingPageData.gallup[1] && item != $scope.brandingPageData.gallup[2] && item != $scope.brandingPageData.gallup[3] && item != $scope.brandingPageData.gallup[4]);
    };
    $scope.filter2 = function (item) {
        return (item != $scope.brandingPageData.gallup[0] && item != $scope.brandingPageData.gallup[2] && item != $scope.brandingPageData.gallup[3] && item != $scope.brandingPageData.gallup[4]);
    };
    $scope.filter3 = function (item) {
        return (item != $scope.brandingPageData.gallup[1] && item != $scope.brandingPageData.gallup[0] && item != $scope.brandingPageData.gallup[3] && item != $scope.brandingPageData.gallup[4]);
    };
    $scope.filter4 = function (item) {
        return (item != $scope.brandingPageData.gallup[1] && item != $scope.brandingPageData.gallup[2] && item != $scope.brandingPageData.gallup[0] && item != $scope.brandingPageData.gallup[4]);
    };
    $scope.filter5 = function (item) {
        return (item != $scope.brandingPageData.gallup[1] && item != $scope.brandingPageData.gallup[2] && item != $scope.brandingPageData.gallup[3] && item != $scope.brandingPageData.gallup[0]);
    };
    $scope.needsModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'needs.html',
            controller: 'needsModalCtrl',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData };
                }
            }
        });
    }
    $scope.triggesAssessmentModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'triggers.html',
            controller: 'triggerModalCtrl',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData, 'detailFrame': $scope.detailFrame };
                }
            }
        });
    }
    $scope.tkiAssessmentModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'tkiAssessment.html',
            controller: 'tkiAssessmentModalCtrl',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData };
                }
            }
        });
    }
    $scope.leaderShipQualitiesModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'leaderShipQualities.html',
            controller: 'leaderShipQualitiesModalCtrl',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData };
                }
            }
        });
    }
    $scope.valueSortAssessmentModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'value.html',
            controller: 'valueSortController',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData };
                }
            }
        });
    }
    $scope.loadNeedChart = function () {
        if ($scope.brandingPageData.needs) {
            var needChart = {};
            needChart.type = "PieChart";
            needChart.data = [
                ['Category', 'value']
            ];
            angular.forEach($scope.brandingPageData.needs, function (data) {
                needChart.data.push([data.type, parseInt(data.value)]);
            });
            needChart.options = {
                displayExactValues: true,
                is3D: false,
                legend: 'labeled',
                pieSliceText: "none",
                chartArea: { top: 10, bottom: 10, height: "90%", width: "90%" },
                backgroundColor: { fill: "transparent" },
                slices: {
                    0: { color: 'f7e651' },
                    1: { color: '291f75' },
                    2: { color: 'f41a1a' }
                }
            };
            $scope.needChart = needChart;
            $scope.showNeedsChartFlag = true;
        } else {
            $scope.showNeedsChartFlag = false;
        }
    }
    $scope.loadTriggerChar = function () {
        if ($scope.brandingPageData.triggers) {
            var triggerChart = {};
            triggerChart.type = "PieChart";
            triggerChart.data = [
                ['Category', 'value']
            ];
            angular.forEach($scope.brandingPageData.triggers, function (data) {
                triggerChart.data.push([data.type, parseInt(data.value)]);
            });
            triggerChart.options = {
                legend: 'labeled',
                pieSliceText: "none",
                chartArea: { top: 10, bottom: 10, height: "90%", width: "90%" },
                backgroundColor: { fill: "transparent" },
                slices: {
                    0: { color: '5190ef' },
                    1: { color: '5190ef' },
                    2: { color: '5190ef' },
                    3: { color: '5190ef' },
                    4: { color: '5190ef' }
                }
            };
            $scope.triggerChart = triggerChart;
            $scope.showTriggerChartFlag = true;
        } else {
            $scope.showTriggerChartFlag = true;
        }

    }
    $scope.loadTkiChart = function () {
        if ($scope.brandingPageData.tki) {
            var tkiChart = {};
            tkiChart.type = "PieChart";
            tkiChart.data = [
                ['Category', 'value']
            ];
            angular.forEach($scope.brandingPageData.tki, function (data) {
                tkiChart.data.push([data.type, parseInt(data.value)]);
            });
            tkiChart.options = {
                legend: 'labeled',
                pieSliceText: "none",
                chartArea: { top: 10, bottom: 10, height: "90%", width: "90%" },
                backgroundColor: { fill: "transparent" },
                slices: {
                    0: { color: '5190ef' },
                    1: { color: '5190ef' },
                    2: { color: '5190ef' },
                    3: { color: '5190ef' },
                    4: { color: '5190ef' }
                }
            };
            $scope.tkiChart = tkiChart;
            $scope.showTkiChartFlag = true;
        } else {
            $scope.showTkiChartFlag = false;
        }

    }
    $rootScope.$on("loadNeedChart", function () {
        $scope.loadNeedChart();
    });
    $rootScope.$on("loadTriggerChar", function () {
        $scope.loadTriggerChar();
    });
    $rootScope.$on("loadTkiChart", function () {
        $scope.loadTkiChart();
    });

    // Redirect to Privacy policy
    $scope.gotoPrivacy = function () {
        $state.go('privacy');
    }
});
app.controller('needsModalCtrl', function ($uibModalInstance, $scope, $rootScope, $http, param, brandingServices) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.error = "";
    $scope.brandingPageData = param.brandingPageData;
    $scope.getQuestions = function () {
        brandingServices.getBrandKind("needs").then(function (response) {
            $scope.needs = response.data;
            $scope.isCanPrev = true;
            $scope.isCanNext = false;
            $scope.totalquest = 18;
            $scope.percentage = (100 / $scope.totalquest);
            $scope.incrementor = Math.round($scope.percentage * 100 * 1) / 100;
        });
    };
    $scope.resetPage = function () {
        init();
    };
    $scope.nextPagefunc = function () {
        var value = 0;
        angular.forEach($scope.needs.questions[$scope.questionId].questionGroup, function (val) {
            value = value + val.value;
        });

        if (value == 5) {
            $scope.isCanPrev = false;
            $scope.percentage += $scope.incrementor;
            $scope.questionId++;
            if ($scope.questionId == 17) {
                $scope.isCanNext = true;
            } else {
                $scope.isCanNext = false;
            }
            if ($scope.percentage > 100) {
                $scope.percentage = 100;
            }
            $scope.error = "";
        } else if (value > 5) {
            $scope.error = "Question must total 5 in order to go to next step.";
        } else {
            if ($scope.needs.questions[$scope.questionId].questionGroup[0].value == undefined || $scope.needs.questions[$scope.questionId].questionGroup[1].value == undefined
                || $scope.needs.questions[$scope.questionId].questionGroup[2].value == undefined) {
                $scope.error = "Values cannot be empty.";
            } else {
                $scope.error = "Question must total 5 in order to go to next step.";
            }
        }

    };
    $scope.prevPage = function () {
        $scope.questionId = $scope.questionId - 1;
        if ($scope.questionId == 0) {
            $scope.isCanPrev = true;
        } else {
            $scope.isCanPrev = false;
        }
        $scope.isCanNext = false;
        if ($scope.percentage >= 100) {
            $scope.percentage -= $scope.incrementor / $scope.questionPerPage;
        } else {
            $scope.percentage -= $scope.incrementor;
        }
        $scope.error = "";
    };
    $scope.submit = function () {
        var value = 0;
        angular.forEach($scope.needs.questions[$scope.questionId].questionGroup, function (val) {
            value = value + val.value;
        });
        if (value == 5) {
            $scope.typeASum = 0;
            $scope.typeBSum = 0;
            $scope.typeCSum = 0;
            for (var i = 0; i < $scope.needs.questions.length; i++) {
                $scope.typeASum = $scope.typeASum + $scope.needs.questions[i].questionGroup[0].value;
                $scope.typeBSum = $scope.typeASum + $scope.needs.questions[i].questionGroup[1].value;
                $scope.typeCSum = $scope.typeASum + $scope.needs.questions[i].questionGroup[2].value;
            }
            $scope.brandingPageData.needs = [];
            $scope.brandingPageData.needs.push({ "type": $scope.needs.needs[0], "value": $scope.typeASum + "" });
            $scope.brandingPageData.needs.push({ "type": $scope.needs.needs[1], "value": $scope.typeBSum + "" });
            $scope.brandingPageData.needs.push({ "type": $scope.needs.needs[2], "value": $scope.typeCSum + "" });
            $rootScope.$emit("change");
            $rootScope.$emit("loadNeedChart", {});
            $uibModalInstance.dismiss('cancel');
        } else {
            if ($scope.needs.questions[$scope.questionId].questionGroup[0].value == undefined || $scope.needs.questions[$scope.questionId].questionGroup[1].value == undefined
                || $scope.needs.questions[$scope.questionId].questionGroup[2].value == undefined) {
                $scope.error = "Values cannot be empty.";
            } else {
                $scope.error = "Question must total 5 in order to go to next step.";
            }
        }
    };
    function init() {
        $scope.error = "";
        $scope.totalquest = 0;
        $scope.questionPerPage = 3;
        $scope.questionId = 0;
        $scope.startNum = 0;
        $scope.endNum = $scope.questionPerPage - 1;
        $scope.getQuestions();
    }
    init();

});
app.controller('triggerModalCtrl', function ($uibModalInstance, $scope, $rootScope, $http, param, brandingServices) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.error = "";
    $scope.brandingPageData = param.brandingPageData;
    $scope.detailFrame = param.detailFrame;
    $scope.getQuestions = function () {
        brandingServices.getBrandKind("Triggers").then(function (response) {
            $scope.triggersQuestion = response.data.values;
            $scope.questionsData = [];
            for (var i = 0; i < $scope.triggersQuestion.length; i++) {
                $scope.questionsData.push({ "question": $scope.triggersQuestion[i], "value": "" });
            }
            $scope.isCanPrev = false;
            $scope.isCanNext = false;
            $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
        });
    };
    $scope.range = function (min, max) {
        var input = [];
        var step = 1;
        for (var i = min; i <= max; i += step) {
            if (i < $scope.questionsData.length) {
                input.push($scope.questionsData[i]);
            }
        }
        return input;
    };
    $scope.resetPage = function () {
        init();
    };
    function init() {
        $scope.error = "";
        $scope.totalquest = 0;
        $scope.questionPerPage = 5;
        $scope.startNum = 0;
        $scope.endNum = $scope.questionPerPage - 1;
        $scope.getQuestions();
    }
    $scope.submit = function () {
        var total = 0;
        for (var i = 0; i < $scope.questionsData.length; i++) {
            total = total + $scope.questionsData[i].value;
        }
        if (total == 100) {
            $scope.brandingPageData.triggers = [];
            for (var i = 0; i < $scope.questionsData.length; i++) {
                $scope.brandingPageData.triggers.push({ "type": $scope.questionsData[i].question, "value": $scope.questionsData[i].value + "" });
            }
            $rootScope.$emit("loadTriggerChar", {});
            $rootScope.$emit("change");
            $uibModalInstance.dismiss('cancel');
        } else {
            $scope.error = "Percentages which should total 100%.";
        }
    }
    init();
});
app.controller('tkiAssessmentModalCtrl', function ($uibModalInstance, $scope, $rootScope, $http, param, brandingServices) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.error = "";
    $scope.brandingPageData = param.brandingPageData;
    $scope.getQuestions = function () {
        brandingServices.getBrandKind("tki").then(function (response) {
            $scope.tkiQuestion = response.data.values;
            $scope.questionsData = [];
            for (var i = 0; i < $scope.tkiQuestion.length; i++) {
                $scope.questionsData.push({ "question": $scope.tkiQuestion[i], "value": "" });
            }
            $scope.isCanPrev = false;
            $scope.isCanNext = false;
            $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
            $scope.error = "";
        });
    };
    $scope.range = function (min, max) {
        var input = [];
        var step = 1;
        for (var i = min; i <= max; i += step) {
            if (i < $scope.questionsData.length) {
                input.push($scope.questionsData[i]);
            }
        }
        return input;
    };
    $scope.resetPage = function () {
        init();
    };
    function init() {
        $scope.error = "";
        $scope.totalquest = 0;
        $scope.questionPerPage = 5;
        $scope.startNum = 0;
        $scope.endNum = $scope.questionPerPage - 1;
        $scope.getQuestions();
    }
    $scope.submit = function () {
        var isValid = 0;
        angular.forEach($scope.questionsData, function (val) {
            try {
                parseInt(val.value)
            }
            catch (er) {
                isValid = 3;
            }
            if (isValid == 0) {
                if (val.value == "") {
                    isValid = 1;
                } else if (val.value == undefined || (val.value < 0 && val.value > 12)) {
                    isValid = 2;
                }
            }

        });
        if (isValid == 0) {
            $scope.brandingPageData.tki = [];
            angular.forEach($scope.questionsData, function (val) {
                $scope.brandingPageData.tki.push({ "type": val.question, "value": val.value + "" });
            });
            $rootScope.$emit("loadTkiChart", {});
            $rootScope.$emit("change");
            $uibModalInstance.dismiss('cancel');
        } else if (isValid == 3) {
            $scope.error = "Please enter number in the fields.";
        }
        else if (isValid == 1) {
            $scope.error = "Please enter all the fields.";
        } else {
            $scope.error = "Please enter a number between 0 - 12 on each field.";
        }
    }
    init();
});
app.controller('leaderShipQualitiesModalCtrl', function ($uibModalInstance, $scope, $rootScope, $http, param, $filter, brandingServices) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.brandingPageData = param.brandingPageData;
    $scope.getQuestions = function () {
        brandingServices.getBrandKind("LeadershipQualities").then(function (response) {
            $scope.needsQuestion = response.data.values;
            $scope.questionsData = [];
            for (var i = 0; i < $scope.needsQuestion.length; i++) {
                $scope.questionsData.push({ "question": $scope.needsQuestion[i], "value": 0 });
            }
            $scope.totalSteps = Math.round($scope.needsQuestion.length / 3);
            $scope.steps = 1;
            $scope.totalquest = $scope.needsQuestion.length;
            $scope.isCanPrev = true;
            $scope.isCanNext = false;
            $scope.percentage = (100 / $scope.totalquest);
            $scope.incrementor = Math.round($scope.percentage * 100 * 3) / 100;
            $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
        });
    };
    $scope.range = function (min, max) {
        var input = [];
        var step = 1;
        for (var i = min; i <= max; i += step) {
            if (i < $scope.questionsData.length) {
                input.push($scope.questionsData[i]);
            }
        }
        return input;
    };
    $scope.resetPage = function () {
        init();
    };
    $scope.nextPagefunc = function () {
        $scope.isCanPrev = false;
        $scope.percentage += $scope.incrementor;
        $scope.startNum = $scope.endNum + 1;
        $scope.endNum = $scope.startNum + ($scope.questionPerPage - 1);
        if ($scope.endNum >= $scope.needsQuestion.length) {
            $scope.isCanNext = true;
        } else {
            $scope.isCanNext = false;
        }
        if ($scope.percentage > 100) {
            $scope.percentage = 100;
        }
        $scope.steps = $scope.steps + 1;
        $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
    };
    $scope.prevPage = function () {
        $scope.startNum = $scope.startNum - $scope.questionPerPage;
        $scope.endNum = $scope.endNum - $scope.questionPerPage;
        if ($scope.endNum < 3) {
            $scope.isCanPrev = true;
        } else {
            $scope.isCanPrev = false;
        }
        $scope.isCanNext = false;
        if ($scope.percentage >= 100) {
            $scope.percentage -= $scope.incrementor / $scope.questionPerPage;
        } else {
            $scope.percentage -= $scope.incrementor;
        }
        $scope.steps = $scope.steps - 1;
        $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
    };
    $scope.submit = function () {
        $scope.strongestQualities = [];
        $scope.questionsData = $filter('orderBy')($scope.questionsData, 'value');
        $scope.developmentQualities = [];
        var i = 0;
        angular.forEach($scope.questionsData, function (data) {
            if (i <= 9) {
                $scope.strongestQualities.push($scope.questionsData[i].question);
            }
            if (i >= $scope.questionsData.length - 10) {
                $scope.developmentQualities.push($scope.questionsData[i].question);
            }
            i++;
        });
        $scope.brandingPageData.leadershipQualities = {};
        $scope.brandingPageData.leadershipQualities.strongestQualities = $scope.strongestQualities;
        $scope.brandingPageData.leadershipQualities.developmentAreas = $scope.developmentQualities;
        $rootScope.$emit("change");
        $uibModalInstance.dismiss('cancel');
    };
    function init() {
        $scope.totalquest = 0;
        $scope.questionPerPage = 3;
        $scope.startNum = 0;
        $scope.endNum = $scope.questionPerPage - 1;
        $scope.getQuestions();
    }
    init();
});
app.controller("valueSortController", function ($uibModalInstance, $rootScope, $scope, $http, param, brandingServices, $q) {
    $scope.brandingPageData = param.brandingPageData;
    $scope.questionsData = [];
    $scope.error = "";
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.moveToCategory = function (fromIndex, toIndex, index) {
        $scope.fromIndexArray = [];
        $scope.toIndexArray = {};
        angular.forEach($scope.models1[fromIndex].items, function (item) {
            if ($scope.myFilter(item)) {
                $scope.fromIndexArray.push(item);
            }
        });
        $scope.toIndexArray.label = $scope.fromIndexArray[index].label;
        $scope.toIndexArray.selectedValue = $scope.fromIndexArray[index].selectedValue;
        $scope.models1[toIndex].items.push($scope.toIndexArray);
        $scope.fromIndexArray[index].selectedValue = true;
    }

    $scope.myFilter = function (item) {
        return !item.selectedValue;
    };
    $scope.getQuestions = function () {
        $scope.page = 1;
        $scope.valueSortData = [];
        $scope.models1 = [
            { listName: "Questions", items: [], dragging: false },
            { listName: "Very Important", items: [], dragging: false },
            { listName: "Important", items: [], dragging: false },
            { listName: "Less Important", items: [], dragging: false }
        ];
        $scope.models2 = [
            { listName: "Very Important", items: [], dragging: false }
        ];
        brandingServices.getBrandKind("Values").then(function (response) {
            $scope.needsQuestion = response.data.values;
            $scope.isCanPrev = true;
            $scope.isCanNext = false;
            $scope.questionsData = [];
            for (var i = 0; i < $scope.needsQuestion.length; i++) {
                $scope.models1[0].items.push({ label: $scope.needsQuestion[i], selectedValue: false });
            }
        });
    };
    $scope.getSelectedItemsIncluding = function (list, item) {
        item.selected = true;
        return list.items.filter(function (item) { return item.selected; });
    };
    $scope.onDragstart = function (list, event) {
        list.dragging = true;
    };
    $scope.onDrop = function (list, items, index) {
        angular.forEach(items, function (item) { item.selected = false; });
        list.items = list.items.slice(0, index)
            .concat(items)
            .concat(list.items.slice(index));
        return true;
    }
    $scope.onMoved = function (list) {
        list.items = list.items.filter(function (item) { return !item.selected; });
    };
    $scope.prevPage = function () {
        $scope.error = "";
        $scope.page = $scope.page - 1;
        $scope.isCanPrev = true;
        $scope.isCanNext = false;
    }
    $scope.nextPagefunc = function () {
        if ($scope.models1[1].items.length >= 6) {
            $scope.page = $scope.page + 1;
            $scope.isCanPrev = false;
            $scope.isCanNext = true;
            $scope.error = "";
            angular.forEach($scope.models1[1].items, function (data) {
                $scope.models2[0].items = $scope.models1[1].items;
            });
        } else {
            $scope.error = "Please make sure that you sort all values among the three given categories !";
        }
    }
    $scope.resetPage = function () {
        init();
    };
    $scope.submit = function () {
        var count = 0;
        angular.forEach($scope.models2[0].items, function (value) {
            if (value.selected) {
                count += 1;
            }
        });
        if (count >= 6) {
            if (count > 6) {
                $scope.error = "You cannot select more than 6 values.";
            } else {
                $scope.error = "";
                $scope.brandingPageData.values = [];
                angular.forEach($scope.models2[0].items, function (value) {
                    if (value.selected) {
                        $scope.brandingPageData.values.push(value.label);
                    }
                });
                $rootScope.$emit("change");
                $uibModalInstance.dismiss('cancel');
            }
        } else {
            $scope.error = "Please make sure you select 6 core values .";
        }
    }
    function init() {
        $scope.getQuestions();
    }
    $scope.resetPageForMobile = function () {
        init();
    }
    $scope.prevPageForMobile = function () {
        $scope.error = "";
        $scope.page = $scope.page - 1;
        $scope.isCanPrev = true;
        $scope.isCanNext = false;
    }
    $scope.nextPagefuncForMobile = function () {
        if ($scope.models1[1].items.length >= 6) {
            $scope.page = $scope.page + 1;
            $scope.isCanPrev = false;
            $scope.isCanNext = true;
            $scope.error = "";
            angular.forEach($scope.models1[1].items, function (data) {
                $scope.models2[0].items = $scope.models1[1].items;
            });
        } else {
            $scope.error = "Please make sure that you sort all values among the three given categories !";
        }
    }
    $scope.submitForMobile = function () {
        var count = 0;
        angular.forEach($scope.models2[0].items, function (value) {
            if (value.selected) {
                count += 1;
            }
        });
        if (count >= 6) {
            if (count > 6) {
                $scope.error = "You cannot select more than 6 values.";
            } else {
                $scope.error = "";
                $scope.brandingPageData.values = [];
                angular.forEach($scope.models2[0].items, function (value) {
                    if (value.selected) {
                        $scope.brandingPageData.values.push(value.label);
                    }
                });
                $rootScope.$emit("change");
                $uibModalInstance.dismiss('cancel');
            }
        } else {
            $scope.error = "Please make sure you select 6 core values .";
        }
    }
    init();


});
