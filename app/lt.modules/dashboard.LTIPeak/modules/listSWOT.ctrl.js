var app = angular.module('leaderTYPE');


app.controller('listSWOTCtrl', function ($scope, $state, peakService, $rootScope, $filter) {
    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $scope.$on('listSwot', function (event, result) {
        if (result.data != undefined) {
            $scope.swotList = [];
            $scope.swotList = result.data;
            if ($scope.swotList != undefined) {
                // sorting the list by modified date
                $scope.swotList.sort(function (a, b) {
                    return b.modifiedDate - a.modifiedDate;
                });
                var i = 0;
                result.data.forEach(function (swot) {
                    $scope.swotList[i].createdDate = $filter('ordinalDate')(swot.createdDate, 'd MMMM yyyy');
                    $scope.swotList[i].modifiedDate = $filter('ordinalDate')(swot.modifiedDate, 'd MMMM yyyy');
                    i++;
                }, this);
            }
        }
    });
    initializeSwotList();
    function initializeSwotList() {
        if ($scope.swotList === undefined) {
            peakService.getAllSwot($rootScope.authService.userId()).then(
                function success(response) {
                    $scope.swotList = response.data;
                    if ($scope.swotList != undefined) {
                        // sorting the list by modified date
                        $scope.swotList.sort(function (a, b) {
                            return b.modifiedDate - a.modifiedDate;
                        });
                        var i = 0;
                        response.data.forEach(function (swot) {
                            $scope.swotList[i].createdDate = $filter('ordinalDate')(swot.createdDate, 'd MMMM yyyy');
                            $scope.swotList[i].modifiedDate = $filter('ordinalDate')(swot.modifiedDate, 'd MMMM yyyy');
                            i++;
                        }, this);
                    }
                }
            );
        }
    }

    $scope.createSWOT = function () {
        $rootScope.$broadcast('myltpeak', { type: 'createSWOT', data: { createUpdate: "create" } });
    }

    $scope.viewSwot = function (swot) {
        $rootScope.$broadcast('myltpeak', { type: 'viewSWOT', data: swot });
    }

    $scope.deleteSWOT = function (swot) {
        $scope.deleteSwotId = swot.swotId;
        $("#deleteConfirmationModal").modal('show');
    }

    $scope.noDelete = function () {
        $("#deleteConfirmationModal").modal('hide');
    }

    $scope.yesDelete = function () {
        $rootScope.loaderService.show("Deleting Swot...");
        peakService.deleteSwot($rootScope.authService.userId(), $scope.deleteSwotId).then(
            function success(response) {
                $rootScope.loaderService.hide();
                $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
            }
        );
        $("#deleteConfirmationModal").modal('hide');
    }

    $scope.editSWOT = function (swot) {
        $scope.editSwot = swot;
        $("#editConfirmationModal").modal('show');
    }

    $scope.noEdit = function () {
        $("#editConfirmationModal").modal('hide');
    }

    $scope.yesEdit = function () {
        $("#editConfirmationModal").modal('hide');
        $rootScope.$broadcast('myltpeak', { type: 'createSWOT', data: { createUpdate: "update", swot: $scope.editSwot } });
    }
});