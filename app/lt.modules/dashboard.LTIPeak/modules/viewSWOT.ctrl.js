var app = angular.module('leaderTYPE');


app.controller('viewSWOTCtrl', function ($scope, $state, peakService, $rootScope) {
    $scope.$on('viewThisSWOT', function (event, result) {
        $scope.swot = {};
        $scope.displaySwot = {};
        if (result.data != undefined) {
            $scope.swot.title = result.data.title;
            $scope.swot.vision = result.data.vision;
            $scope.swot.mission = result.data.mission;
            $scope.swot.strategy = result.data.strategy;
            $scope.swot.currentState = result.data.currentState;
            $scope.swot.swotWeight = {};
            $scope.swot.swotWeight.strength = result.data.swotWeight.strength;
            $scope.swot.swotWeight.opportunity = result.data.swotWeight.opportunity;
            $scope.swot.swotWeight.weakness = result.data.swotWeight.weakness;
            $scope.swot.swotWeight.threat = result.data.swotWeight.threat;
            $scope.swot.successCriteria = result.data.successCriteria;
            $scope.swot.strengths = result.data.strengths;
            $scope.swot.weaknesses = result.data.weaknesses;
            $scope.swot.opportunities = result.data.opportunities;
            $scope.swot.threats = result.data.threats;
            $scope.swot.biases = result.data.biases;
            $scope.swot.swotId = result.data.swotId;
            // display swot
            $scope.displaySwot.title = result.data.title;
            $scope.displaySwot.vision = result.data.vision;
            $scope.displaySwot.mission = result.data.mission;
            $scope.displaySwot.strategy = result.data.strategy;
            $scope.displaySwot.currentState = result.data.currentState;
            $scope.displaySwot.swotWeight = {};
            $scope.displaySwot.swotWeight.strength = result.data.swotWeight.strength;
            $scope.displaySwot.swotWeight.opportunity = result.data.swotWeight.opportunity;
            $scope.displaySwot.swotWeight.weakness = result.data.swotWeight.weakness;
            $scope.displaySwot.swotWeight.threat = result.data.swotWeight.threat;
            $scope.displaySwot.successCriteria = result.data.successCriteria;
            $scope.displaySwot.strengths = [];
            $scope.displaySwot.weaknesses = [];
            $scope.displaySwot.opportunities = [];
            $scope.displaySwot.threats = [];

            result.data.strengths.forEach(function (element) {
                if (element != null && element != undefined) {
                    $scope.displaySwot.strengths.push(element);
                }

            }, this);

            result.data.weaknesses.forEach(function (element) {
                if (element != null && element != undefined) {
                    $scope.displaySwot.weaknesses.push(element);
                }
            }, this);


            result.data.opportunities.forEach(function (element) {
                if (element != null && element != undefined) {
                    $scope.displaySwot.opportunities.push(element);
                }
            }, this);

            result.data.threats.forEach(function (element) {
                if (element != null && element != undefined) {
                    $scope.displaySwot.threats.push(element);
                }
            }, this);

            $scope.displaySwot.biases = result.data.biases;
            $scope.displaySwot.swotId = result.data.swotId;
        }
    });



    $scope.editSWOT = function () {
        $("#editConfirmationModal").modal('show');
    }

    $scope.noEdit = function () {
        $("#editConfirmationModal").modal('hide');
    }

    $scope.yesEdit = function () {
        $("#editConfirmationModal").modal('hide');
        $rootScope.$broadcast('myltpeak', { type: 'createSWOT', data: { createUpdate: "update", swot: $scope.swot } });
    }

    $scope.listSWOT = function () {
        // $rootScope.$broadcast('myltpeak', { type: 'listSWOT' });
        $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
    }

    $scope.deleteSWOT = function () {
        $("#deleteConfirmationModal").modal('show');
    }

    $scope.noDelete = function () {
        $("#deleteConfirmationModal").modal('hide');
    }

    $scope.yesDelete = function () {
        // delete the swot and return to list page
        $rootScope.loaderService.show("Deleting Swot...");
        peakService.deleteSwot($rootScope.authService.userId(), $scope.swot.swotId).then(
            function success(response) {
                $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
                // $rootScope.loaderService.hide();
            }
        );

        $("#deleteConfirmationModal").modal('hide');
        // $rootScope.$broadcast('myltpeak', { type: 'listSWOT' });
    }
});