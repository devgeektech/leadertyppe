var app = angular.module('leaderTYPE');


app.controller('createSWOTCtrl', function ($scope, $state, peakService, $rootScope, $uibModal) {
    $scope.$on('createSwot', function (event, result) {
        $scope.job = result.data;
        if ($scope.job.swot != undefined) {
            $scope.peakModel.title = $scope.job.swot.title;
            $scope.peakModel.vision = $scope.job.swot.vision;
            $scope.peakModel.mission = $scope.job.swot.mission;
            $scope.peakModel.strategy = $scope.job.swot.strategy;
            $scope.peakModel.currentState = $scope.job.swot.currentState;
            $scope.peakModel.swotWeight.strength = String($scope.job.swot.swotWeight.strength);
            $scope.peakModel.swotWeight.opportunity = String($scope.job.swot.swotWeight.opportunity);
            $scope.peakModel.swotWeight.weakness = String($scope.job.swot.swotWeight.weakness);
            $scope.peakModel.swotWeight.threat = String($scope.job.swot.swotWeight.threat);

            if ($scope.job.swot.successCriteria != null) {
                // $scope.peakModel.successCriteria = $scope.job.swot.successCriteria;
                $scope.inputSuccessList = $scope.job.swot.successCriteria;
            } else {
                $scope.peakModel.successCriteria = [];
            }
            if ($scope.job.swot.biases != null) {
                $scope.peakModel.biases = $scope.job.swot.biases;
            } else {
                $scope.peakModel.biases = [];
            }
            if ($scope.job.swot.strengths != null) {
                $scope.peakModel.strengths = $scope.job.swot.strengths;
            } else {
                $scope.peakModel.strengths = [];
            }

            if ($scope.job.swot.weaknesses != null) {
                $scope.peakModel.weaknesses = $scope.job.swot.weaknesses;
            } else {
                $scope.peakModel.weaknesses = [];
            }

            if ($scope.job.swot.opportunities) {
                $scope.peakModel.opportunities = $scope.job.swot.opportunities;
            } else {
                $scope.peakModel.opportunities = [];
            }

            if ($scope.job.swot.threats != null) {
                $scope.peakModel.threats = $scope.job.swot.threats;
            } else {
                $scope.peakModel.threats = [];
            }
        }
        if ($scope.changeForm != undefined)
            $scope.changeForm.$setPristine();
    });
    $rootScope.$on('swotCheckFormModified', function (event, val) {
      if ($scope.changeForm != undefined) {
        if (!$scope.changeForm.modified) {
          $rootScope.$broadcast("confirmcancel", val);
        } else {
          $("#exitPageConfirmationModal").modal("show");
          $scope.confirmExit = function () {
            $("#exitPageConfirmationModal").modal("hide");
          }
          $scope.hideExitModal = function () {
            $("#exitPageConfirmationModal").modal("hide");
            $rootScope.$broadcast("confirmcancel", val);
          }
        }
      }else {
        $rootScope.$broadcast("confirmcancel", val);
      }
    });

    $rootScope.$on("change", function (event, val) {
        $scope.peakModel.biases = val.data;
    });
    $scope.validStringRegex = new RegExp(/^[a-zA-Z0-9\ \,\.\(\)]*$/g);
    $scope.showCreateSWOTStep1 = true;
    $scope.showCreateSWOTStep2 = false;
    $scope.swotDetails = getSwotDetails();
    $scope.peakModel = getEmptyModel();
    $scope.swotInputDetail = function () {
        swotInputDetail.strength = "";
        swotInputDetail.weakness = "";
        swotInputDetail.threat = "";
        swotInputDetail.opportunity = "";
        swotInputDetail.successCriteria = "";
    }
    $scope.successCriteriaListInputs;
    $scope.cancelSWOT = function () {
        $("#cancelConfirmationModal").modal("show");
    }

    $scope.yesCancel = function () {
        $("#cancelConfirmationModal").modal("hide");
        $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
    }

    $scope.noCancel = function () {
        $("#cancelConfirmationModal").modal("hide");
    }

    $scope.saveSWOT = function () {
        validateStep3();
        if ($scope.isStep3Valid) {
            $("#saveConfirmationModal").modal("show");
        }
    }

    $scope.noSave = function () {
        $("#saveConfirmationModal").modal("hide");
    }

    $scope.yesSave = function () {
        $("#saveConfirmationModal").modal("hide");
        mergeSuccessCriteriaArrays();

        var userId = $rootScope.authService.userId();
        // following code will send the request to save/update the swot of user.
        if ($scope.job.createUpdate != undefined && $scope.job.createUpdate === "update") {
            // update swot
            $rootScope.loaderService.show("Updating Swot...");
            peakService.updateSwot(userId, $scope.job.swot.swotId, $scope.peakModel).then(
                function success(response) {
                    if (response.isSuccess) {
                        $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
                    } else {
                        $rootScope.loaderService.hide();
                        $scope.showError("Update Error", "couldn't udpate. Some thing went wrong...");
                    }
                }
            );
        } else if ($scope.job.createUpdate != undefined && $scope.job.createUpdate === "create") {
            // create swot
            // suspend until response comes back
            $rootScope.loaderService.show("Saving Swot...");
            peakService.addSwot(userId, $scope.peakModel).then(
                function success(response) {
                    if (response.isSuccess) {
                        $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
                    } else {
                        $rootScope.loaderService.hide();
                        $scope.showError("Save Error",response.responseMessage);
                    }

                }
            );
        }
    }

    $scope.moveToStep2 = function () {
        validateStep1();
        if ($scope.isStep1Valid) {
            $scope.showCreateSWOTStep1 = false;
            $scope.showCreateSWOTStep2 = true;
            $scope.showCreateSWOTStep3 = false;
        }

    }
    $scope.goToStep3 = function () {
        var vallll = validateStep2();
        if (vallll) {
            $scope.showCreateSWOTStep1 = false;
            $scope.showCreateSWOTStep2 = false;
            $scope.showCreateSWOTStep3 = true;
        }
    }

    $scope.backToStep1 = function () {
        $scope.showCreateSWOTStep1 = true;
        $scope.showCreateSWOTStep2 = false;
        $scope.showCreateSWOTStep3 = false;
    }
    $scope.backToStep2 = function () {
        $scope.showCreateSWOTStep1 = false;
        $scope.showCreateSWOTStep2 = true;
        $scope.showCreateSWOTStep3 = false;
    }

    function getEmptyModel() {
        var peak = {};
        peak.title = "";
        peak.vision = "";
        peak.mission = "";
        peak.strategy = "";
        peak.currentState = "";
        peak.swotWeight = {};
        peak.swotWeight.strength = "1";
        peak.swotWeight.opportunity = "1";
        peak.swotWeight.weakness = "1";
        peak.swotWeight.threat = "1";
        peak.successCriteria = [];
        peak.biases = [];
        peak.strengths = [];
        peak.weaknesses = [];
        peak.opportunities = [];
        peak.threats = [];
        return peak;
    }

    function getSwotDetails() {
        var swotDetails = {};
        swotDetails.strengths = [];
        swotDetails.opportunities = [];
        swotDetails.threats = [];
        swotDetails.weaknesses = [];
        swotDetails.biases = [];
        swotDetails.successCriteriaList = [];

        peakService.getStrengths().then(
            function success(response) {
                swotDetails.strengths = response.data;
                $scope.peakModel.strengths.length = response.data.length;
            }
        );

        peakService.getOpportunities().then(
            function success(response) {
                swotDetails.opportunities = response.data;
                $scope.peakModel.opportunities.length = response.data.length;
            }
        );

        peakService.getThreats().then(
            function success(response) {
                swotDetails.threats = response.data;
                $scope.peakModel.threats.length = response.data.length;
            }
        );

        peakService.getWeaknesses().then(
            function success(response) {
                swotDetails.weaknesses = response.data;
                $scope.peakModel.weaknesses.length = response.data.length;
            }
        );

        peakService.getBiases().then(
            function success(response) {
                swotDetails.biases = response.data;
            }
        );
        peakService.getSuccessCriteria().then(
            function success(response) {
                swotDetails.successCriteriaList = response.data;
                $scope.isCriteriaListAvailable = true;
            }
        );
        return swotDetails;
    }

    $scope.removeFromSelectedList = function (index, type) {
        switch (type) {
            case "successCriteria":
                $scope.successCriteriaErrorMessage = "";
                $scope.successArray1.splice(index, 1);
                break;
            case "bias":
                $scope.biasErrorMessage = "Cannot remove from here... Please retake Assessment to modify selection.";
                break;
        }
    }
    $scope.removeFromInputList = function (index, type) {
        switch (type) {
            case "successCriteria":
                $scope.successCriteriaErrorMessage = "";
                $scope.inputSuccessList.splice(index, 1);
                break;
            case "bias":
                $scope.biasErrorMessage = "Cannot remove from here... Please retake Assessment to modify selection.";
                break;
        }
    }

    $scope.inputSuccessList = [];
    $scope.addToArray = function (event) {
        if (event.keyCode == 13) {
            if ($scope.swotInputDetail.successCriteria != undefined && $scope.swotInputDetail.successCriteria.trim() != "") {
                if ($scope.inputSuccessList.includes($scope.swotInputDetail.successCriteria) || $scope.successArray1.includes($scope.swotInputDetail.successCriteria)) {
                    $scope.successCriteriaErrorMessage = "This item already exists";
                } else if (($scope.inputSuccessList.length + $scope.successArray1.length) >= 5) {
                    $scope.successCriteriaErrorMessage = "Max 5 items allowed";
                }
                // else if (!$scope.swotInputDetail.successCriteria.match($scope.validStringRegex)) {
                //     $scope.successCriteriaErrorMessage = "Success Criteria should not contain any special characters except(. , )().";
                // }
                else if ($scope.swotInputDetail.successCriteria.length > 100) {
                    $scope.successCriteriaErrorMessage = "Success Criteria should not contain more than 100 chars.";
                } else {
                    $scope.successCriteriaErrorMessage = "";
                    $scope.inputSuccessList.push($scope.swotInputDetail.successCriteria);
                    // $scope.peakModel.successCriteria.push($scope.swotInputDetail.successCriteria);
                    $scope.swotInputDetail.successCriteria = "";
                }
            }
        }

    }
    $scope.successArray1 = [];
    $scope.addSelectedToArray = function (value) {

        if (value != undefined && value != null) {
            $scope.successArray1.splice(0, $scope.successArray1.length);

            value.forEach(function (element) {
                if ($scope.inputSuccessList.includes(element) || $scope.successArray1.includes(element)) {
                    $scope.successCriteriaErrorMessage = "This item already exists";
                } else if (($scope.inputSuccessList.length + $scope.successArray1.length) >= 5) {
                    $scope.successCriteriaErrorMessage = "Max 5 items allowed";
                } else {
                    $scope.successCriteriaErrorMessage = "";
                    // $scope.peakModel.successCriteria.push(element);
                    $scope.successArray1.push(element);
                }
            }, this);
        }
    }
    function mergeSuccessCriteriaArrays() {
        $scope.peakModel.successCriteria.splice(0, $scope.peakModel.successCriteria.length);
        $scope.successArray1.forEach(function (element) {
            $scope.peakModel.successCriteria.push(element);
        }, this);
        $scope.inputSuccessList.forEach(function (element) {
            $scope.peakModel.successCriteria.push(element);
        }, this);
    }

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.isCriteriaListAvailable = false;
    $scope.error = "";

    $scope.biasesModalPopup = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'bias.html',
            controller: 'biasAssessmentModalController',
            windowClass: 'full',
            size: 'lg',
            resolve: {
                param: function () {
                    return { 'brandingPageData': $scope.brandingPageData };
                }
            }
        });
    }


    function validateStep1() {
        $scope.isStep1Valid = false;
        // validate peakModels title, strategy, mission, vision
        if ($scope.peakModel.title === "" || $scope.peakModel.title === null || $scope.peakModel.title === undefined) {
            $scope.isStep1Valid = false;
            $scope.titleErrorMessage = "Title is a mendatory field.";
        } else if ($scope.peakModel.title.length > 30) {
            $scope.isStep1Valid = false;
            $scope.titleErrorMessage = "Title length should not exceed 30 characters.";
        }
        // else if (!$scope.peakModel.title.match($scope.validStringRegex)) {
        //     $scope.isStep1Valid = false;
        //     $scope.titleErrorMessage = "Title should not contain any special characters except(. , )().";
        // }
        else if ($scope.peakModel.title.trim() == "") {
            $scope.isStep1Valid = false;
            $scope.titleErrorMessage = "Title cannot not be blank.";
        } else if ($scope.peakModel.vision.length > 200) {
            $scope.isStep1Valid = false;
            $scope.visionErrorMessage = "Vision length should not exceed 300.";
        }
        // else if (!$scope.peakModel.vision.match($scope.validStringRegex)) {
        //     $scope.isStep1Valid = false;
        //     $scope.visionErrorMessage = "Vision should not contain any special characters except(. , )().";
        // }
        else if ($scope.peakModel.mission.length > 200) {
            $scope.isStep1Valid = false;
            $scope.missionErrorMessage = "Mission length should not exceed 300.";
        }
        // else if (!$scope.peakModel.mission.match($scope.validStringRegex)) {
        //     $scope.isStep1Valid = false;
        //     $scope.missionErrorMessage = "Mission should not contain any special characters except(. , )().";
        // }
        else if ($scope.peakModel.strategy.length > 300) {
            $scope.isStep1Valid = false;
            $scope.strategyErrorMessage = "Strategy length should not exceed 300.";
        }
        // else if (!$scope.peakModel.strategy.match($scope.validStringRegex)) {
        //     $scope.isStep1Valid = false;
        //     $scope.strategyErrorMessage = "Strategy should not contain any special characters except(. , )().";
        // }
        else {
            $scope.isStep1Valid = true;
            $scope.titleErrorMessage = "";
            $scope.missionErrorMessage = "";
            $scope.visionErrorMessage = "";
            $scope.strategyErrorMessage = "";
        }
    }
    function validateStep2() {
        $scope.isStep2Valid = true;
        if ($scope.peakModel.strengths != undefined && $scope.peakModel.strengths.length > 0) {
            $scope.peakModel.strengths.forEach(function (element) {
                if (element != null) {
                    if (element.trim() != null && element.trim() != "") {
                        // if (!element.match($scope.validStringRegex)) {
                        //     $scope.isStep2Valid = false;
                        //     $scope.strengthErrorMessage = "Strength cannont contain any special characters except(. , )().";
                        // }
                    }
                }

            }, this);

        }
        if ($scope.peakModel.opportunities != undefined && $scope.peakModel.opportunities.length > 0) {
            $scope.peakModel.opportunities.forEach(function (element) {
                if (element != null) {
                    if (element.trim() != null && element.trim() != "") {
                        // if (!element.match($scope.validStringRegex)) {
                        //     $scope.isStep2Valid = false;
                        //     $scope.opportunityErrorMessage = "Opportunity cannont contain any special characters except(. , )().";
                        // }
                    }
                }

            }, this);

        }
        if ($scope.peakModel.threats != undefined && $scope.peakModel.threats.length > 0) {
            $scope.peakModel.threats.forEach(function (element) {
                if (element != null) {
                    if (element.trim() != null && element.trim() != "") {
                        // if (!element.match($scope.validStringRegex)) {
                        //     $scope.isStep2Valid = false;
                        //     $scope.threatErrorMessage = "Threat cannont contain any special character except(. , )().";
                        // }
                    }
                }

            }, this);

        }
        if ($scope.peakModel.weaknesses != undefined && $scope.peakModel.weaknesses.length > 0) {
            $scope.peakModel.weaknesses.forEach(function (element) {
                if (element != null) {
                    if (element.trim() != null && element.trim() != "") {
                        // if (!element.match($scope.validStringRegex)) {
                        //     $scope.isStep2Valid = false;
                        //     $scope.weaknessErrorMessage = "Weakness cannont contain any special characters except(. , )().";
                        // }
                    }
                }

            }, this);

        }
        return $scope.isStep2Valid;
    }


    $scope.clearErrorMessage = function (step) {
        switch (step) {
            case "STEP1":
                $scope.titleErrorMessage = "";
                $scope.missionErrorMessage = "";
                $scope.visionErrorMessage = "";
                $scope.strategyErrorMessage = "";
                break;
            case "STEP2":
                $scope.strengthErrorMessage = "";
                $scope.opportunityErrorMessage = "";
                $scope.threatErrorMessage = "";
                $scope.weaknessErrorMessage = "";
                break;
            case "STEP3":
                $scope.successCriteriaErrorMessage = "";
                $scope.biasErrorMessage = "";
                $scope.successCriteriaErrorMessage = "";
                break;
            default:
                break;
        }

    }

    function validateStep3() {
        $scope.isStep3Valid = false;
        if ($scope.peakModel.successCriteria.length > 5) {
            $scope.isStep3Valid = false;
            $scope.successCriteriaErrorMessage = "The success criteria cannot contain more than 5 items.";
        }
        else if ($scope.peakModel.biases.length > 5) {
            $scope.isStep3Valid = false;
            $scope.biasErrorMessage = "The bias cannot contain more than 5 items.";
        }
        else if (($scope.peakModel.currentState != null || $scope.peakModel.currentState != "") && $scope.peakModel.currentState.length > 300) {
            $scope.isStep3Valid = false;
            $scope.currentStateErrorMessage = "The current state cannot contain more than 300 letters.";
        }
        else {
            $scope.isStep3Valid = true;
            $scope.successCriteriaErrorMessage = "";
            $scope.biasErrorMessage = "";
            $scope.currentStateErrorMessage = "";
        }
    }

    $scope.showError = function (errorHead, errorMessage) {
        $scope.errorHead = errorHead;
        $scope.errorMessage = errorMessage;
        $("#errorModal").modal("show");
    }
    $scope.closeError = function (errorHead, errorMessage) {
        $("#errorModal").modal("hide");
    }
});

app.controller('biasAssessmentModalController', function ($uibModalInstance, $scope, $rootScope, $http, param, $filter, peakService) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.getBiases = function () {
        peakService.getBiases().then(function (response) {
            $scope.biasesList = [];
            for (var i = 0; i < response.data.length; i++) {
                $scope.biasesList.push({ item: response.data[i].item, description: response.data[i].description, biasWeight: 0 });
            }
            $scope.totalSteps = Math.round($scope.biasesList.length / 3);
            $scope.steps = 1;
            $scope.totalBias = $scope.biasesList.length;
            $scope.isCanPrev = true;
            $scope.isCanNext = false;
            $scope.percentage = (100 / $scope.totalBias);
            $scope.incrementor = Math.round($scope.percentage * 100 * 3) / 100;
            $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
        });
    };
    $scope.range = function (min, max) {
        var input = [];
        var step = 1;
        for (var i = min; i <= max; i += step) {
            if (i < $scope.biasesList.length) {
                input.push($scope.biasesList[i]);
            }
        }

        return input;
    };
    $scope.resetPage = function () {
        init();
    };
    $scope.nextPagefunc = function () {
        $scope.isCanPrev = false;
        $scope.percentage += $scope.incrementor;
        $scope.startNum = $scope.endNum + 1;
        $scope.endNum = $scope.startNum + ($scope.biasPerPage - 1);
        if ($scope.endNum >= $scope.biasesList.length) {
            $scope.isCanNext = true;
        } else {
            $scope.isCanNext = false;
        }
        if ($scope.percentage > 100) {
            $scope.percentage = 100;
        }
        $scope.steps = $scope.steps + 1;
        $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
    };
    $scope.prevPage = function () {
        $scope.startNum = $scope.startNum - $scope.biasPerPage;
        $scope.endNum = $scope.endNum - $scope.biasPerPage;
        if ($scope.endNum < 3) {
            $scope.isCanPrev = true;
        } else {
            $scope.isCanPrev = false;
        }
        $scope.isCanNext = false;
        if ($scope.percentage >= 100) {
            $scope.percentage -= $scope.incrementor / $scope.biasPerPage;
        } else {
            $scope.percentage -= $scope.incrementor;
        }
        $scope.steps = $scope.steps - 1;
        $scope.inputs = $scope.range($scope.startNum, $scope.endNum);
    };
    $scope.submit = function () {
        $scope.strongestBiases = [];

        function checkForAllSameBiasWeight() {
            $scope.allBiasesAreSame = true;
            for (var index1 = 0; index1 < $scope.biasesList.length; index1++) {
                for (var index2 = 1; index2 < $scope.biasesList.length; index2++) {
                    if ($scope.biasesList[index1].biasWeight != $scope.biasesList[index2].biasWeight) {
                        $scope.allBiasesAreSame = false;
                    }
                }
            }
            return $scope.allBiasesAreSame;
        }

        // $scope.biasesList = $filter('orderBy')($scope.biasesList, 'biasWeight');
        if (!checkForAllSameBiasWeight()) {
            $scope.biasesList.sort(function (a, b) {
                return b.biasWeight - a.biasWeight;
            });
        }

        for (var index = 0; index < 5; index++) {
            $scope.strongestBiases.push({ item: $scope.biasesList[index].item, description: $scope.biasesList[index].description, biasWeight: $scope.biasesList[index].biasWeight });
        }
        $rootScope.$broadcast("change", { data: $scope.strongestBiases });
        $uibModalInstance.dismiss('cancel');
    };
    function init() {
        $scope.totalquest = 0;
        $scope.biasPerPage = 3;
        $scope.startNum = 0;
        $scope.endNum = $scope.biasPerPage - 1;
        $scope.getBiases();
    }
    init();
});
