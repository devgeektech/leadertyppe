
var app = angular.module('leaderTYPE');
app.controller('leaderPeakCtrl', function (webService, checkOut, peakService, $scope, $location, $state, auth, store, $rootScope, $timeout, $cookieStore, envService, constants) {

    $scope.showCreateSWOT = false;
    $scope.showListSWOT = false;
    $scope.showViewSWOT = false;
    $scope.showDefaultViewSWOT = false;
    $scope.subscribedPackages = [false, false];
    $rootScope.createSwotFlag = false;
    $scope.$watch('showCreateSWOT', function (newValue, oldValue, scope) {
        if (newValue) {
            $scope.showListSWOT = false;
            $scope.showViewSWOT = false;
            $scope.showDefaultViewSWOT = false;
        }
    });
    $scope.$watch('showListSWOT', function (newValue, oldValue, scope) {
        if (newValue) {
            $scope.showCreateSWOT = false;
            $scope.showViewSWOT = false;
            $scope.showDefaultViewSWOT = false;
        }
    });
    $scope.$watch('showViewSWOT', function (newValue, oldValue, scope) {
        if (newValue) {
            $scope.showCreateSWOT = false;
            $scope.showListSWOT = false;
            $scope.showDefaultViewSWOT = false;
        }
    });

    $scope.$watch('showDefaultViewSWOT', function (newValue, oldValue, scope) {
        if (newValue) {
            $scope.showCreateSWOT = false;
            $scope.showListSWOT = false;
            $scope.showViewSWOT = false;
        }
    });

    $scope.$on('myltpeak', function (event, args) {
        switch (args.type) {
            case "createSWOT":
                $rootScope.createSwotFlag = true;
                $scope.showCreateSWOT = true;
                $timeout(function () {
                    $rootScope.$broadcast('createSwot', { data: args.data });
                });
                break;
            case "viewSWOT":
                $rootScope.createSwotFlag = false;
                $scope.showViewSWOT = true;
                $timeout(function () {
                    $rootScope.$broadcast('viewThisSWOT', { data: args.data });
                });
                break;
            case "listSWOT":
                $rootScope.createSwotFlag = false;
                $scope.showListSWOT = true;
                $timeout(function () {
                    $rootScope.$broadcast('listSwot', { data: args.data });
                });
                break;
            case "defaultViewSWOT":
                $rootScope.createSwotFlag = false;
                init();
                break;
        }

    });

    function init() {
        $rootScope.loaderService.show("Loading Swot List...");
        webService.getPackageService($rootScope.authService.userId()).then(function (response) {
            var highestPackage = null;
            if (response != null && response.data.length > 0) {
                var isValid = (new Date(response.data[0].nextRenewalDate) - new Date()) > 0;
                if ((response.data[0].packageName == "PLATINUM") && isValid) {
                    highestPackage = response.data[0];
                }
            }


            if (highestPackage != null && highestPackage.packageName == "PLATINUM") {
                $scope.subscribedPackages[0] = true;
                if ((new Date(highestPackage.nextRenewalDate) - new Date()) < 0) {
                    $scope.subscribedPackages[1] = true;
                }
            }
            if ($scope.subscribedPackages[0] == true && !$scope.subscribedPackages[1]) {
                if ($rootScope.authService.isAuthenticated()) {
                    peakService.getAllSwot($rootScope.authService.userId()).then(
                        function success(response) {
                            if (response.data != undefined && response.data.length >= 1) {
                                $rootScope.$broadcast('myltpeak', { type: 'listSWOT', data: response.data });
                                $rootScope.loaderService.hide();
                            }
                            else {
                                $rootScope.loaderService.hide();
                                $scope.showDefaultViewSWOT = true;
                            }
                        }
                    );

                } else {
                    $rootScope.loaderService.hide();
                    $rootScope.authService.gotoHome();
                }
            }
            else {
                $rootScope.loaderService.hide();
            }
        });
    }

    $scope.gotoPackage = function () {
        $state.go('landingpage', { 'package': true });
    }





    // //
    // if ($rootScope.authService.isAuthenticated()) {
    //     $rootScope.loaderService.show("Loading Swot List")
    //     peakService.getAllSwot($rootScope.authService.userId()).then(
    //         function success(response) {
    //             // var noOfSwots = response.data.length;
    //             $rootScope.loaderService.hide();
    //             if (response.data != undefined && response.data.length >= 1) {
    //                 // this data is to  be sent to listSWOT page
    //                 $rootScope.$broadcast('myltpeak', { type: 'listSWOT', data: response.data });
    //             }
    //             else {
    //                 $scope.showDefaultViewSWOT = true;
    //                 // $rootScope.$broadcast('myltpeak', { type: 'defaultViewSWOT' });
    //             }
    //         }
    //     );
    // }
    // else {
    //     $rootScope.authService.gotoHome();
    // }
    // }

    if (constants.apiUrl == "#{apiUrl}") {
        envService.get($location.$$protocol + "://" + $location.$$host + "/").then(function (response) {
            if (response && response != "") {
                constants.apiUrl = response;
            }
            else {
                constants.apiUrl = "http://dev-leadertype-rest.mybluemix.net/"
            }
            init();
        });
    }
    else {
        init();
    }

});
