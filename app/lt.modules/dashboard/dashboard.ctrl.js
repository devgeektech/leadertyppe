var app = angular.module('leaderTYPE');
app.controller('dashboardCtrl', function ($scope, $state, $rootScope) {
  var previoudLocation;
  $rootScope.heading = "";
  $scope.movingNewView = "";
  $scope.previoudLocation = 'mylti';
  if ($rootScope.authService.isAuthenticated()) {
    $rootScope.loaderService.show("Loading your dashboard");
  }else {
    $rootScope.loaderService.show("Loading assessment");
  }
  $scope.goToHome = function(){
    var state = angular.copy($state.current);
    $scope.movingNewView = "home";
    $scope.cancelClick(true,$scope.previoudLocation);
  }
  $scope.gotoDashboard = function(){
    var state = angular.copy($state.current);
    $scope.movingNewView = "dashboard";
    $scope.cancelClick(true,$scope.previoudLocation);
  }
  $scope.gotoAccount = function(){
    var state = angular.copy($state.current);
    $scope.movingNewView = "account";
    $scope.cancelClick(true,$scope.previoudLocation);
  }
  $scope.gotoProfile = function(){
    var state = angular.copy($state.current);
    $scope.movingNewView = "profile";
    $scope.cancelClick(true,$scope.previoudLocation);
  }
  $scope.logout = function(){
    var state = angular.copy($state.current);
    $scope.movingNewView = "logout";
    $scope.cancelClick(true,$scope.previoudLocation);
  }
  $scope.gotoMyLTI = function () {
    var state = angular.copy($state.current);
    if (state.name != 'dashboard.mylti' && $scope.previoudLocation != 'myltteam'){
      $scope.movingNewView = "mylti";
      $scope.cancelClick(true,$scope.previoudLocation);
    }else{
      if($scope.previoudLocation != 'mylti'){
        $rootScope.heading = "";
        $scope.previoudLocation = 'mylti';
        $state.go('dashboard.mylti');
      }
    }
    /*if ($state.current.name == 'dashboard.myltbrand') {
      $scope.movingNewView = "mylti";
      $scope.cancelClick(true);
    }else {
      if ($state.current.name != 'dashboard.mylti' || $scope.previoudLocation != 'mylti') {
        $rootScope.heading = "";
        $scope.previoudLocation = 'mylti';
        $state.go('dashboard.mylti');
      }
    }*/
  }

  $scope.gotoMyLTBrand = function () {
    var state = angular.copy($state.current);
    if (state.name != 'dashboard.myltbrand' && ($scope.previoudLocation != 'mylti' && $scope.previoudLocation != 'myltteam')){
      $scope.movingNewView = "myltbrand";
      $scope.cancelClick(true,$scope.previoudLocation);
    }else{
      if($scope.previoudLocation != 'myltbrand'){
        $rootScope.heading = "MY LEADERBRAND";
        $scope.previoudLocation = 'myltbrand';
        $state.go('dashboard.myltbrand');
      }
    }
    /*if ($state.current.name != 'dashboard.myltbrand') {
      $rootScope.heading = "MY LEADERBRAND";
      $state.go('dashboard.myltbrand');
    }*/
  }
  $scope.gotoMyLTTeam = function () {
    var state = angular.copy($state.current);
    if (state.name != 'dashboard.myltteam' && ($scope.previoudLocation != 'mylti' && $scope.previoudLocation != 'myltteam')){
      $scope.movingNewView = "myltteam";
      $scope.cancelClick(true,$scope.previoudLocation);
    }else{
      if($scope.previoudLocation != 'myltteam'){
        $rootScope.heading = "MY LEADERSHIP TEAM";
        $scope.previoudLocation = 'myltteam';
        $state.go('dashboard.myltteam');
      }
    }
    /*if ($state.current.name == 'dashboard.myltbrand') {
      $scope.movingNewView = "myltteam";
      $scope.cancelClick(true);
    }
    else {
      if ($state.current.name != 'dashboard.myltteam') {
        $rootScope.heading = "MY LEADER TEAM";
        $scope.previoudLocation = 'myltteam';
        $state.go('dashboard.myltteam');
      }
    }*/
  }
  $scope.gotoMyLTIPeak = function(){
    var state = angular.copy($state.current);
    if (state.name != 'dashboard.myltpeak' && ($scope.previoudLocation != 'mylti' && $scope.previoudLocation != 'myltteam')){
      $scope.movingNewView = "myltpeak";
      $scope.cancelClick(true,$scope.previoudLocation);
    }else{
      if($scope.previoudLocation != 'myltpeak'){
        $rootScope.heading = "MY LEADERPEAK ";
        $scope.previoudLocation = 'myltpeak';
        $state.go('dashboard.myltpeak');
      }
    }
    /*if ($state.current.name == 'dashboard.myltbrand') {
      $scope.movingNewView = "myltpeak";
      $scope.cancelClick(true);
    }else if($state.current.name != 'dashboard.myltpeak') {
      $rootScope.heading = "MY LEADERPEAK ";
      $state.go('dashboard.myltpeak');
    }*/
  }

  $rootScope.$on('dashboard', function (event, result) {
    switch (result) {
      case "mylti":
        $rootScope.heading = "MY LEADERTYPE";
        $scope.saveEditShowFlag = false;
        $rootScope.brandclassName = "customHeaderForOtherDasboardPage";
        break;
      case "myltbrand":
        $rootScope.heading = "MY LEADERBRAND";
        $scope.saveEditShowFlag = true;
        $scope.saveEditClass = "glyphicon-pencil";
        $scope.saveEditFlag = false;
        $scope.editSaveTemplate = "Edit";
        $scope.isBrandingPage = false;
        $rootScope.brandclassName = "customHeaderForBrandPage";
        break;
      case "myltteam":
        $rootScope.heading = "MY LeaderTEAM";
        $scope.saveEditShowFlag = false;
        $rootScope.brandclassName = "customHeaderForOtherDasboardPage";
        break;
      case "myltpeak":
        $rootScope.heading = "MY LeaderPEAK";
        $scope.saveEditShowFlag = false;
        $rootScope.brandclassName = "customHeaderForOtherDasboardPage";
        break;
      default:
        $rootScope.heading = "MY LEADERTYPE";
        $scope.saveEditShowFlag = false;
        $rootScope.brandclassName = "customHeaderForOtherDasboardPage";
        break;
    }

  });
  $scope.saveEditClick = function () {
    if ($scope.saveEditFlag) {
      //on edit is clicked
      $scope.saveEditClass = "glyphicon glyphicon-pencil";
      $rootScope.$broadcast('saveEditClick', { saveEditFlag: $scope.saveEditFlag });
      $scope.saveEditFlag = false;
      $scope.editSaveTemplate = "Edit";
    } else {
      //on save is cliked
      $scope.saveEditClass = "glyphicon glyphicon-ok";
      $scope.editSaveTemplate = "Save";
      $rootScope.$broadcast('saveEditClick', { saveEditFlag: $scope.saveEditFlag });
      $scope.saveEditFlag = true;
    }
  }
  $scope.cancelClick = function (val,previoudLocation) {
    if (!val) {
      $scope.movingNewView = "";
    }
    if(previoudLocation =='mylti'){
      if($rootScope.takingAssessment){
        $rootScope.$broadcast('sliderCheckFormModified', $scope.movingNewView);
      }else{
        $rootScope.$broadcast("confirmcancel", $scope.movingNewView);
      }
    }else if(previoudLocation =='myltbrand'){
      $rootScope.$broadcast('brandCheckFormModified', $scope.movingNewView);
    }else if(previoudLocation =='myltpeak'){
      if($rootScope.createSwotFlag){
        $rootScope.$broadcast('swotCheckFormModified', $scope.movingNewView);
      }else{
        $rootScope.$broadcast("confirmcancel", $scope.movingNewView);
      }
    }else{
      $rootScope.$broadcast('cancelClick', $scope.movingNewView);
    }
  }

  $rootScope.$on("confirmcancel", function (event, val) {
    if (val != "") {
      switch (val) {
        case "mylti":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('dashboard.mylti');
          break;
        case "myltbrand":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('dashboard.myltbrand');
          break;
        case "myltteam":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('dashboard.myltteam');
          break;
        case "myltpeak":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('dashboard.myltpeak');
          break;
        case "myltteamfeedback":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('feedback');
          break;
        case "home":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go("landingpage");
          break;
        case "account":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go("accountpage");
          break;
        case "profile":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $rootScope.loaderService.show("Loading your profile");
          $state.go('profile');
          break;
        case "dashboard":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $state.go('dashboard.mylti', {}, {reload: true});
          break;
        case "logout":
          $rootScope.createSwotFlag = false;
          $rootScope.takingAssessment = false;
          $rootScope.authService.logout();
          break;
      }
    }
  });
});
