var app = angular.module('leaderTYPE');


app.controller('navBarCtrl', function ($scope, $state, $rootScope) {
    $scope.state = $state;
    $scope.$on('onLoginSuccess', function (args) {
        $rootScope.$apply();
    });

    $scope.$on('onProfileUpdate', function (args) {
        $rootScope.$apply();
    });
});