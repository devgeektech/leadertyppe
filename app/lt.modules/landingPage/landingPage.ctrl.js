'use strict';

angular.module('leaderTYPE')
    .controller('landingPageCtrl', ['reportService', 'webService', 'ltiService', '$scope', '$location', '$state', 'auth', 'store', '$rootScope', '$timeout', '$cookieStore', '$anchorScroll', '$stateParams', function (reportService, webService, ltiService, $scope, $location, $state, auth, store, $rootScope, $timeout, $cookieStore, $anchorScroll, $stateParams) {
        window.onbeforeunload = null;
        $scope.contactUs = {};
        $scope.successMessage = "";
        $scope.packages = {};
        $scope.subPackages = [];
        $scope.campaign = null;
        $scope.scrollItems = [
            "Because every leader is an exception to the rule...",
            "Learn from leaders who share your LeaderType™.",
            "Craft and refine your unique leadership brand.",
            "Build your influence by sharing best practices."
        ];
        $scope.packageDuration = "Month";
        $scope.selectedPackageDuration = $cookieStore.get('selectedPackageDuration') ? parseInt($cookieStore.get('selectedPackageDuration')) : 0;
        $scope.leaderCoachPackageInfoIndex = 0;
        $scope.leaderCoachPrice = 0;
        $scope.selectedSubPackages = {};
        $scope.subPackageMoreInfo = {};
        $scope.styleClass = "navbar-promo-top";
        
        var cartId = 0;
        var isRefreshed = false;
        var isClearCart = false;
        var previousValue = -1;
        $scope.cartList = [];
        $scope.LeaderCoachSubpackageSelection = [];
        function loadCartItems() {
            var item = $cookieStore.get("cartList");
            if (item) {
                $scope.cartList = JSON.parse(item);
                if ($scope.cartList.length > 0) {
                    cartId = $scope.cartList[$scope.cartList.length - 1].cartId;
                }
            }
        }

        function clearCart() {
            $cookieStore.put("cartList", "");
            $scope.cartList = [];
        }

        //package
        function loadPackages() {
            isRefreshed = true;
            webService.getPackages().then(function (response) {
                if (response.isSuccess) {
                    $scope.packages.data = response.data;
                    angular.forEach($scope.packages.data, function (value) {
                        if (value.parentPackageId == null) {
                            $scope.selectedSubPackages[value.packageId] = [];
                            var subsLength = $scope.loadSubPackages(value.packageId).length;
                            value.hasSubPackages = subsLength > 0;
                            subsLength += value.features.length;
                            if (subsLength > $scope.packFeatureCount) {
                                $scope.packFeatureCount = subsLength;
                            }
                        }
                    });
                    setPackages();
                    var gotoPackage = $stateParams.package;
                    if (gotoPackage != null) {
                        $location.hash('package');
                        $anchorScroll();
                    } else {
                        var urlFrom = $location.search().from;
                        if (angular.equals(urlFrom, 'leadercoach')) {
                            var scrollTo = $location.search().to;
                            if (angular.equals(scrollTo, 'package')) {
                                $location.hash('package');
                                $anchorScroll();
                            }
                            else if (angular.equals(scrollTo, 'contact')) {
                                $location.hash('contact');
                                $anchorScroll();
                            }
                            else if (angular.equals(scrollTo, 'leaderCoachMobileApp')) {
                                $state.go('leaderCoachMobileApp');
                            }
                            else if (angular.equals(scrollTo, 'elementOfLeadership')) {
                                $state.go('elementOfLeadership');
                            }
                        }
                    }
                }
                loadCampaign();
            });

        }


        function loadCampaign() {
            reportService.getCampaign().then(function (response) {
                if (response) {
                    $scope.campaign = response;
                }
                $rootScope.loaderService.hide();
            })
        }
        loadPackages();

        loadCartItems();

        $scope.loadSubPackages = function (packageId) {
            var subPackages = [];
            for (var i = 0; i < $scope.packages.data.length; i++) {
                if ($scope.packages.data[i].parentPackageId == packageId) {
                    $scope.packages.data[i].isSelected = false;
                    subPackages.push($scope.packages.data[i]);
                };

            };
            return subPackages;
        }

        $scope.$watch('selectedPackageDuration', function (packageDuration) {
            $cookieStore.put('selectedPackageDuration', packageDuration);
            if (!isRefreshed) {
                if (!$scope.isClearCart && previousValue != $scope.selectedPackageDuration && $cookieStore.get("cartList") != "")
                    $("#packageRefreshConfirmationModal").modal("show");
                // clearCart();
            }
            else {
                isRefreshed = !isRefreshed;
            }
            $scope.confirmClearCart = function () {
                $("#packageRefreshConfirmationModal").modal("hide");
                clearCart();
            }
            $scope.hideCartModal = function () {
                $("#packageRefreshConfirmationModal").modal("hide");
                $scope.isClearCart = true;
                if ($scope.selectedPackageDuration == 1) {
                    $scope.selectedPackageDuration = 0;
                }
                else
                    $scope.selectedPackageDuration = 1;
            }
            $scope.isClearCart = false;
            setPackages();
            previousValue = $scope.selectedPackageDuration;
        });

        function setPackages() {
            if ($scope.selectedPackageDuration == 0) {
                loadMonthPackages();
            }
            else {
                loadYearPackages();
            }
        }

        function loadMonthPackages() {
            angular.forEach($scope.packages.data, function (value) {
                if (value.parentPackageId == null) {
                    value.unitInfo = {};
                    var keepGoing = true;
                    angular.forEach(value.priceInfo, function (pInfo) {
                        if (keepGoing) {
                            if (pInfo.duration == "Month" || pInfo.duration == "OneTime") {
                                value.unitInfo.unit = pInfo.unit;
                                value.unitInfo.price = pInfo.price;
                                value.unitInfo.duration = pInfo.duration;
                                keepGoing = false;
                                if ($scope.selectedSubPackages[value.packageId].length > 0) {
                                    leaderCoachCost();
                                }
                            }
                        }
                    });
                }
            });
        }

        function loadYearPackages() {
            angular.forEach($scope.packages.data, function (value) {
                if (value.parentPackageId == null) {
                    value.unitInfo = {};
                    var keepGoing = true;
                    angular.forEach(value.priceInfo, function (pInfo) {
                        if (keepGoing) {
                            if (pInfo.duration == "Year" || pInfo.duration == "OneTime") {
                                value.unitInfo.unit = pInfo.unit;
                                value.unitInfo.price = pInfo.price;
                                value.unitInfo.duration = pInfo.duration;
                                keepGoing = false;
                                if ($scope.selectedSubPackages[value.packageId].length > 0) {
                                    leaderCoachCost();
                                }
                            }
                        }
                    });
                }
            });
        }

        $scope.packages.getParent = function (subPackId) {
            var childPack = $scope.packages.getPack(subPackId);
            var parentPack = {};
            angular.forEach($scope.packages.data, function (pack) {
                if (pack.packageId == childPack.parentPackageId) {
                    parentPack = pack;
                }
            });
            return parentPack;
        };

        $scope.packages.getPack = function (packId) {
            var pack = {};
            angular.forEach($scope.packages.data, function (value) {
                if (value.packageId == packId) {
                    pack = value;
                }
            });
            return pack;
        };

        $scope.toggleSelection = function (parentPackageId, packageId) {
            var selectionIndex = $scope.selectedSubPackages[parentPackageId].indexOf(packageId);
            selectionIndex > -1 ? $scope.selectedSubPackages[parentPackageId].splice(selectionIndex, 1)
                : $scope.selectedSubPackages[parentPackageId].push(packageId);
            $scope.selectedSubPackages[parentPackageId].sort();
            leaderCoachCost();
        };

        $scope.exists = function (parentPackageId, packageId) {
            return $scope.selectedSubPackages[parentPackageId].indexOf(packageId) > -1;
        };

        $scope.showSubPackageInfo = function (subPackageId) {

            $scope.subPackageMoreInfo = $scope.packages.getPack(subPackageId);
            $scope.subPackageFirstInfo = [];
            $scope.subPackageSecondInfo = [];
            for (var i = 0; i < Math.round($scope.subPackageMoreInfo.features.length / 2); i++) {
                $scope.subPackageFirstInfo.push({ features: $scope.subPackageMoreInfo.features[i] });
            }
            for (var i = Math.round($scope.subPackageMoreInfo.features.length / 2); i < $scope.subPackageMoreInfo.features.length; i++) {
                $scope.subPackageSecondInfo.push({ features: $scope.subPackageMoreInfo.features[i] });
            }
            $("#subPackageInfoModal").modal('show');
        }

        $scope.$watchCollection('selectedSubPackages', function (value) {
            leaderCoachCost();
        });

        function leaderCoachCost() {
            angular.forEach($scope.packages.data, function (pack) {
                if (pack.parentPackageId == null && pack.hasSubPackages) {
                    if ($scope.selectedSubPackages[pack.packageId].length == 0) {
                        pack.unitInfo = {};
                        pack.unitInfo.price = 0;
                    }
                    else {
                        angular.forEach($scope.selectedSubPackages[pack.packageId], function (selectedSubPack) {

                            var keepGoing = true;
                            angular.forEach(pack.priceInfo, function (subPackPriceInfo) {
                                if (keepGoing) {
                                    if (subPackPriceInfo.packageIds == null || subPackPriceInfo.duration == ($scope.selectedPackageDuration > 0 ? 'Year' : 'Month')) {
                                        var isAvailable = angular.equals(subPackPriceInfo.packageIds, $scope.selectedSubPackages[pack.packageId]);
                                        if (isAvailable) {
                                            pack.unitInfo.price = subPackPriceInfo.price;
                                            pack.unitInfo.unit = subPackPriceInfo.unit;
                                            pack.unitInfo.duration = subPackPriceInfo.duration;
                                            keepGoing = false;
                                        }
                                    }
                                }
                                // }
                            });

                        });
                    }
                }
            });

        }

        $scope.onPackageClick = function (title, cost, packages) {
            if (title == "BASIC") {
                $rootScope.authService.gotoDashboard();
            } else if (title == "PREMIUM") { // changed from LeaderCOACH
                $scope.addItemToCart(title, cost, packages, $scope.selectedPackageDuration == 0 ? "Month" : "Year");
            }
            else if (title == "PLATINUM") {
                $scope.addItemToCart(title, cost, packages, $scope.selectedPackageDuration == 0 ? "Month" : "Year");
            } else if (title == "ENHANCED") {
                //$rootScope.authService.gotoMyLTBrand();
                $scope.addItemToCart(title, cost, packages, $scope.selectedPackageDuration == 0 ? "Month" : "Year");
            }
        }
        //Cart
        $scope.addItemToCart = function (title, cost, selectedpackage, renewalType) {
            $scope.cartList = [];
            var cartItem = {};
            cartItem.cartId = ++cartId;
            cartItem.productName = title;
            cartItem.price = cost;
            cartItem.renewalType = renewalType;
            for (var i = 0; i < $scope.cartList.length; i++) {
                if ($scope.cartList[i].productName === title && $scope.cartList[i].renewalType === renewalType) {
                    $scope.cartList.splice(i, 1);
                }
            }
            toastr.options.timeOut = 500;
            toastr.success(cartItem.productName + ' added to the cart.');
            cartItem.packageId = selectedpackage.packageId;
            $scope.cartList.push(cartItem);
            $cookieStore.put('cartList', JSON.stringify($scope.cartList));
        }

        $scope.removeItemFromCart = function (itemToRemove) {
            for (var i = 0; i < $scope.cartList.length; i++)
                if ($scope.cartList[i].productName === itemToRemove) {
                    $scope.cartList.splice(i, 1);
                    // calculateTotal();
                }
            if ($scope.cartList.length > 0) {
                $cookieStore.put('cartList', JSON.stringify($scope.cartList));
            }
            else {
                $cookieStore.put('cartList', "");
            }

        }
        $scope.isCartEmpty = function () {
            if ($scope.cartList.length == 0) {
                return true;
            }
        }

        $scope.cartLength = function () {
            return cartList.length;
        }
        $scope.packFeatureCount = 0;
        $scope.subPackFeatureCount = 0;

        $scope.showModal = function () {
            $("#SubPackagePopup").modal('show');
        }

        $scope.range = function (count) {

            var ratings = [];

            for (var i = 0; i < count; i++) {
                ratings.push(i)
            }

            return ratings;
        }



        $scope.gotoMethodology = function () {
            $state.go('methodology');
        }

        $scope.gotocheckOut = function () {
        
            if ($rootScope.authService.isAuthenticated()) {
                $state.go('checkout');
            }
            else {
                $rootScope.authService.loginWithSignUp();
                $scope.$on('onLoginSuccess', function (args) {
                    $state.go('checkout');
                });
            }
        }

        // link directions for resources section. 
        $scope.gotoBecomeALeaderTypeCoach = function () {
            $state.go('becomeALeaderTypeCoach');
        }

        $scope.gotoElementOfLeadership = function () {
            $state.go('elementOfLeadership');
        }

        $scope.gotoLeaderCoachMobileApp = function () {
            $state.go('leaderCoachMobileApp');
        }

        $scope.gotoSearchLeadershipCoachDatabase = function () {
            $state.go('searchLeadershipCoachDatabase');
        }

        // Redirect to Privacy policy
        $scope.gotoPrivacy = function () {
            $state.go('privacy');
        }

        $scope.showError = false;
        $scope.sendLead = function () {
            if ($scope.SentMessage.$invalid) {
                $scope.showError = true;
            } else {
                $scope.showError = false;
                webService.postContact($scope.contactUs).then(function (response) {
                    if (response.isSuccess) {
                        reset();
                        $scope.successMessage = "Hurrah! Your message has been sent successfully";
                        $timeout(function () {
                            $scope.successMessage = "";
                        }, 5000);
                    }
                    else {
                        reset();
                        $scope.successMessage = "Sorry,Something went wrong please try again later";
                        $timeout(function () {
                            $scope.successMessage = "";
                        }, 5000);
                    }
                });
            }

        }
        $rootScope.$on("onLoginSuccess", function () {
            $state.go($state.current, {}, { reload: true });
        });

        $rootScope.$on("onEmailFailed", function () {
            $state.go('noemail')
        });

        $rootScope.$on("onLoginFailed", function () {
            $rootScope.loaderService.hide();
        });

        $rootScope.$on("onEmailVerification", function () {
            $state.go('verifyemail');

        });
        $rootScope.authService.loginWithSignUp();
        $rootScope.$on("onSignUp", function () {
            $rootScope.authService.signUp();
        });

        function reset() {
            $scope.contactUs = {};
        }

        $scope.showLeaderDetails = function (id) {
            ltiService.getLTIndicator(id).then(function (response) {
                if (response.isSuccess) {
                    $scope.selectedLeader = response.data;
                    $("#president-popup").modal('show');
                }
                else {
                    dialogs.error();
                }
            });
        }
        $scope.goToLearnMore = function () {
            $state.go('howitworks');
        }
        $scope.gotoLeaderCoachMobileAppLead = function (id) {
            switch (id) {
                case 'Pack0001':
                    $state.go('leaderCoachMobileAppLeadYourself');
                    break;
                case 'Pack0002':
                    $state.go('leaderCoachMobileAppLeadConversations');
                    break;
                case 'Pack0003':
                    $state.go('leaderCoachMobileAppLeadMeetings');
                    break;
                default:
                    $state.go('leaderCoachMobileApp');
                    break;
            }

        }


        /***********************************************
                 * Universal Parallax
                 * Copyright - ForBetterWeb.com
         ***********************************************/

        // scrolling
        $(function () {
            $('a[href*="#"]:not([href="#"])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top - 50
                        }, 1000);
                        return false;
                    }
                }
            });
        });
        var introHeader = $('.intro'),
            intro = $('.intro');

        buildModuleHeader(introHeader);

        $(window).resize(function () {
            var width = Math.max($(window).width(), window.innerWidth);
            buildModuleHeader(introHeader);
        });

        $(window).scroll(function () {
            effectsModuleHeader(introHeader, this);
        });

        intro.each(function (i) {
            if ($(this).attr('data-background')) {
                $(this).css('background-image', 'url(' + $(this).attr('data-background') + ')');
            }
        });
        function buildModuleHeader(introHeader) {
        };
        function effectsModuleHeader(introHeader, scrollTopp) {
            if (introHeader.length > 0) {
                var homeSHeight = introHeader.height();
                var topScroll = $(document).scrollTop();
                if ((introHeader.hasClass('intro')) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css('top', (topScroll * .4));
                }
                if (introHeader.hasClass('intro') && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css('opacity', (1 - topScroll / introHeader.height() * 1));
                }
            }
        };

        /***********************************************
         * jQuery Parallax
         ***********************************************/

        $('.bg-img').parallax("50%", .12);
        $('.bg-img2').parallax("50%", .12);
        $('.bg-img3').parallax("50%", .12);
        $('.bg-img4').parallax("50%", .12);
        $('.bg-img5').parallax("50%", .12);

        /***********************************************
         * jQuery to collapse the navbar on scroll
         ***********************************************/

        $(window).scroll(function () {

            var nav = $('.navbar-universal');
            if (nav.length) {
                if ($('.promo-card').length > 0) {
                    $scope.styleClass = "navbar-promo-top";
                } else {
                    $scope.styleClass = "navbar-promo-top-0";
                }
                if ($(window).scrollTop() > 133) {
                    $(".navbar-fixed-top").addClass("top-nav-collapse");
                    $(".navbar-fixed-top").removeClass($scope.styleClass);
                } else {
                    $(".navbar-fixed-top").removeClass("top-nav-collapse");
                    $(".navbar-fixed-top").addClass($scope.styleClass);
                }
            }
        });

        /***********************************************
         * Tabs
         ***********************************************/

        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })

        /***********************************************
        * Highlight the top nav as scrolling occurs
        ***********************************************/

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 65
        })

        /***********************************************
         * Closes the Responsive Menu on Menu Item Click in One Page Nav
         ***********************************************/

        $('.navbar-onepage .navbar-collapse ul li a').on('click', function () { $('.navbar-onepage .navbar-toggle:visible').click(); });

        // /***********************************************
        //  * Active class to nav
        //  ***********************************************/

        // var url = window.location;
        // $('ul.nav a[href="' + url.href + '"]').parent().addClass('active');
        // $('ul.nav a').filter(function () {
        //     return this.href == url.href;
        // }).parent().addClass('active');


        /***********************************************
         * Carousel
         ***********************************************/

        $('.carousel-big').carousel({
            interval: 6500, //changes the speed
            pause: "false"
        })

        $('.carousel-small').carousel({
            interval: 5000, //changes the speed
            pause: "false"
        })

        /***********************************************
         * HTML5 Placeholder
         ***********************************************/

        $(function () {
            $('input, textarea').placeholder();
        });

        /***********************************************
         * Load WOW.js
         ***********************************************/

        new WOW().init();

        // $("#carousel-intro").style.setProperty( 'height',  window.outerHeight + 'px', 'important' );
        $(".carousel-inner").css("cssText", "height: " + window.innerHeight + "px !important;");
        // $(".carousel-inner").css("min-height", window.innerHeight + "px");
        // $("#carousel-intro .item .fill").css("min-height", window.innerHeight + "px");
        // $("#carousel-intro .item").css("min-height", window.innerHeight + "px");
        // $("#carousel-intro .intro-body").css("min-height", window.innerHeight + "px");
        //  $(".item").css("min-height", window.innerHeight + "px");



        $('.carousel-control.left').click(function () {
            $('#carousel-intro').carousel('prev');
        });

        $('.carousel-control.right').click(function () {
            $('#carousel-intro').carousel('next');
        });

        /***********************************************
        * Connectivity
        ***********************************************/

        $(function () {
            var canvas = $("canvas")[0];
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
            canvas.style.display = 'block';
            var dot = '';
            var ctx = canvas.getContext("2d");
            ctx.lineWidth = .3;

            // Generate a color
            function Color(min) {
                min = min || 0;
                this.r = this.value(min);
                this.g = this.value(min);
                this.b = this.value(min);
                this.style = "rgba(" + this.r + "," + this.g + "," + this.b + ",1)";
            }

            Color.prototype = {
                value: function (min) {
                    return Math.floor(Math.random() * 255 + min);
                }
            }

            function mixComponents(comp1, comp2, weight1, weight2) {
                return (comp1 * weight1 + comp2 * weight2) / (weight1 + weight2);
            }

            function gradient(dot1, dot2, midColor) {
                var grad = ctx.createLinearGradient(Math.floor(dot1.x), Math.floor(dot1.y), Math.floor(dot2.x), Math.floor(dot2.y));
                grad.addColorStop(0, dot1.color.style);
                grad.addColorStop(Math.floor(dot1.radius / (dot1.radius / dot2.radius)), midColor);
                grad.addColorStop(1, dot2.color.style);
                return grad;
            }

            function lineStyle(dot1, dot2) {
                var r = mixComponents(dot1.color.r, dot2.color.r, dot1.radius, dot2.radius);
                var g = mixComponents(dot1.color.g, dot2.color.g, dot1.radius, dot2.radius);;
                var b = mixComponents(dot1.color.b, dot2.color.b, dot1.radius, dot2.radius);;
                var midColor = 'rgba(' + Math.floor(r) + ',' + Math.floor(g) + ',' + Math.floor(b) + ', 1)';

                return gradient(dot1, dot2, midColor);
            }

            var connectArea = {
                x: 50 * canvas.width / 100,
                y: 50 * canvas.height / 100
            };

            var dots = {
                nb: 70,
                distMax: 200,
                connectAreaRadius: 550,
                array: []
            };

            function Dot() {
                this.x = Math.random() * canvas.width;
                this.y = Math.random() * canvas.height;
                this.vx = Math.random() - 0.5;
                this.vy = Math.random() - 0.5;
                this.radius = Math.random() * 2;
                this.color = new Color();
            }

            Dot.prototype = {
                draw: function () {
                    ctx.beginPath();
                    ctx.fillStyle = this.color.style;
                    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
                    ctx.fill();
                }
            };

            function moveDots() {
                for (var i = 0; i < dots.nb; i++) {
                    var dot = dots.array[i];

                    if (dot.y < 0 || dot.y > canvas.height) {
                        dot.vy = - dot.vy;
                    } else if (dot.x < 0 || dot.x > canvas.width) {
                        dot.vx = - dot.vx;
                    }
                    dot.x += dot.vx;
                    dot.y += dot.vy;
                }
            }

            function connectDots() {
                for (var i = 0; i < dots.nb; i++) {
                    for (var j = 0; j < dots.nb; j++) {
                        if (i === j) continue;

                        var dot1 = dots.array[i];
                        var dot2 = dots.array[j];

                        var xDiff = dot1.x - dot2.x;
                        var yDiff = dot1.y - dot2.y;
                        var xCoreDiff = dot1.x - connectArea.x;
                        var yCoreDiff = dot1.y - connectArea.y;

                        if ((xDiff < dots.distMax && xDiff > -dots.distMax)
                            && (yDiff < dots.distMax && yDiff > -dots.distMax)
                            && (xCoreDiff < dots.connectAreaRadius && xCoreDiff > -dots.connectAreaRadius)
                            && (yCoreDiff < dots.connectAreaRadius && yCoreDiff > -dots.connectAreaRadius)) {
                            ctx.beginPath();
                            ctx.strokeStyle = lineStyle(dot1, dot2);
                            ctx.moveTo(dot1.x, dot1.y);
                            ctx.lineTo(dot2.x, dot2.y);
                            ctx.stroke();
                            ctx.closePath();
                        }
                    }
                }
            }

            function createDots() {
                for (var i = 0; i < dots.nb; i++) {
                    dots.array.push(new Dot());
                }
            }

            function drawDots() {
                for (var i = 0; i < dots.nb; i++) {
                    dot = dots.array[i];
                    dot.draw();
                }
            }

            function animateDots() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                if (window.matchMedia("screen and (min-width:1000px)").matches) {
                    moveDots();
                    connectDots();
                    drawDots();
                }
            }

            $("canvas").on("mousemove mouseleave", function (e) {
                if (e.type == "mousemove") {
                    connectArea.x = e.pageX;
                    connectArea.y = e.pageY;
                }
                if (e.type == "mouseleave") {
                    connectArea.x = 50 * canvas.width / 100;
                    connectArea.y = 50 * canvas.height / 100;
                }
            });

            createDots();
            setInterval(animateDots, 40);

        });

        function changeHandler(event) {
            // youtube Mode has changed.
            $('html, body').animate({
                scrollTop: $("#video").offset().top
            }, 0);

        }

        document.addEventListener("fullscreenchange", changeHandler, false);
        document.addEventListener("webkitfullscreenchange", changeHandler, false);
        document.addEventListener("mozfullscreenchange", changeHandler, false);



        $scope.loadSubPackagesDisplay = function (packageObj) {
            //$scope.packages.data
            var packageIds = [];
            var dataToDisplay = []

            for (var id of packageObj.priceInfo[0].packageIds) {
                if (id != "Pack0004")
                    packageIds.push(id);
            }

            // for (var obj of $scope.packages.data) {
            //     for (var ids of packageIds) {
            //         if (obj.packageId == ids) {
            //             dataToDisplay.push(obj);
            //         }
            //     }
            // }

            for (var obj of $scope.packages.data) {
                for (var ids of packageIds) {
                    if (obj.packageId == ids) {
                        dataToDisplay = [];
                        dataToDisplay.push(obj);
                    }
                }
            }


            return dataToDisplay;
        }
    }]);
