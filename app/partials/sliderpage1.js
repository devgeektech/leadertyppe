var App = angular.module('myApp');


App.controller('Sliderpage1Ctrl',function($scope,$state) {
  $scope.range = function(){
    console.log("Inside range func");
    var min= $scope.startNum;
    var max=$scope.endNum;
    var step=1;
    var input = [];
    for (var i = min; i <= max; i += step){ 
      input.push(i);
    }
    console.log(input+" range end");
    return input;
  };

    // window.onload=(function()
// {
//   console.log("onload");
//     $scope.questionPerPage=3;
//     $scope.startNum=0;
//     $scope.endNum=$scope.questionPerPage-1;
//     $scope.inputs=$scope.range();
//     $scope.getQuestion();
//     console.log("onload end");
// });

$scope.getQuestion=function (){
  console.log("getQuestion");
          $scope.questions = {"set1":[
        {
          "id":1,
          "question":"I want a leader who is...",
          "answer1":"Direct and honest.",
          "answer2":"Warm and genuine.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":2,
          "question":"I'm known for..",
          "answer1":"Taking things very literally.",
          "answer2":"Reading between the lines.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":3,
          "question":"I tend to be..",
          "answer1":"Skeptical and questioning.",
          "answer2":"Trusting and affirming.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":4,
          "question":"When learning I want..",
          "answer1":"Novel concepts or theories.",
          "answer2":"Practical applications.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":5,
          "question":"I like having..",
          "answer1":"A few, close friends.",
          "answer2":"A large, diverse network.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":6,
          "question":"I'm naturally..",
          "answer1":"An open book.",
          "answer2":"A private person.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":7,
          "question":"I tend to trust my..",
          "answer1":"Experiences.",
          "answer2":"Hunches.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":8,
          "question":"I'd rather be..",
          "answer1":"Quick.",
          "answer2":"Sure.",
          "code1":"I",
          "code2":"E",
          "sliderValue":0.0
        },
        {
          "id":9,
          "question":"When resolving a conflict I try to..",
          "answer1":"Weigh the pros and cons objectively.",
          "answer2":"Find common ground on which to build.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":10,
          "question":"My worst nightmare is being stuck..",
          "answer1":"In chaos.",
          "answer2":"In a rut.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":11,
          "question":"When making a decision it's important to..",
          "answer1":"Apply the rules consistently.",
          "answer2":"Allow for extenuating circumstances.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":12,
          "question":"Regarding the news of the day I'm more likely to..",
          "answer1":"Relate to the people involved.",
          "answer2":"Analyze what happened and why.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":13,
          "question":"On teams I'm more bothered by a lack of..",
          "answer1":"Cooperation.",
          "answer2":"Structure.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":14,
          "question":"I like to just..",
          "answer1":"Go",
          "answer2":"Be",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":15,
          "question":"Which sounds more like you?",
          "answer1":"I get a general idea, and add details if needed.",
          "answer2":"I gather specifics in order to have a general idea.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":16,
          "question":"I want to be where..",
          "answer1":"There's a lot going on.",
          "answer2":"I can focus.",
          "code1":"S",
          "code2":"N",
          "sliderValue":0.0
        },
        {
          "id":17,
          "question":"Which sounds more like you?",
          "answer1":"I am comfortable with silence in a conversation.",
          "answer2":"I often fill-in any gaps in conversation.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":18,
          "question":"I like to..",
          "answer1":"Understand the context.",
          "answer2":"Notice the details.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":19,
          "question":"Facing a tough situation I'm more inclined to...",
          "answer1":"Reflect on it.",
          "answer2":"Talk it out.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":20,
          "question":"Given a new question I initially search for..",
          "answer1":"Images to spur my imagination.",
          "answer2":"Reliable sources of information.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":21,
          "question":"In a new environment I want to..",
          "answer1":"Observe and understand.",
          "answer2":"Engage and interact.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":22,
          "question":"I'm more persuaded by..",
          "answer1":"Logical argument.",
          "answer2":"Passionate belief.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":23,
          "question":"I'm more inclined to..",
          "answer1":"Praise.",
          "answer2":"Critique.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
        },
        {
          "id":24,
          "question":"I tend to be a..",
          "answer1":"Realist.",
          "answer2":"Dreamer.",
          "code1":"T",
          "code2":"F",
          "sliderValue":0.0
}],
"set2":[
        {
          "id":25,
          "question":"I describe myself as..",
          "answer1":"Clarity",
          "answer2":"Harmony",
          "code1":"IT",
          "code2":"EF",
          "sliderValue":0.0
        },
        {
          "id":26,
          "question":"I describe myself as..",
          "answer1":"Defining",
          "answer2":"Nurturing",
          "code1":"IT",
          "code2":"EF",
          "sliderValue":0.0
        },
        {
          "id":27,
          "question":"I describe myself as..",
          "answer1":"Winning",
          "answer2":"Well-being",
          "code1":"IT",
          "code2":"EF",
          "sliderValue":0.0
        },
        {
          "id":28,
          "question":"I describe myself as..",
          "answer1":"Variety",
          "answer2":"Complexity",
          "code1":"IT",
          "code2":"EF",
          "sliderValue":0.0
        },
        {
          "id":29,
          "question":"I describe myself as..",
          "answer1":"Potential",
          "answer2":"Proven",
          "code1":"IF",
          "code2":"ET",
          "sliderValue":0.0
        },
        {
          "id":30,
          "question":"I describe myself as..",
          "answer1":"Directive",
          "answer2":"Supportive",
          "code1":"IF",
          "code2":"ET",
          "sliderValue":0.0
        },
        {
          "id":31,
          "question":"I describe myself as..",
          "answer1":"Catalyst",
          "answer2":"Stabilizer",
          "code1":"IF",
          "code2":"ET",
          "sliderValue":0.0
        },
        {
          "id":32,
          "question":"I describe myself as..",
          "answer1":"Rationale",
          "answer2":"Needs",
          "code1":"IF",
          "code2":"ET",
          "sliderValue":0.0
        },
        {
          "id":33,
          "question":"I describe myself as..",
          "answer1":"Meaningful",
          "answer2":"Programatic",
          "code1":"IS",
          "code2":"EN",
          "sliderValue":0.0
        },
        {
          "id":34,
          "question":"I describe myself as..",
          "answer1":"Autonomy",
          "answer2":"Consensus",
          "code1":"IS",
          "code2":"EN",
          "sliderValue":0.0
        },
        {
          "id":35,
          "question":"I describe myself as..",
          "answer1":"Competent",
          "answer2":"Authentic",
          "code1":"IS",
          "code2":"EN",
          "sliderValue":0.0
        },
        {
          "id":36,
          "question":"I describe myself as..",
          "answer1":"Experience",
          "answer2":"Envision",
          "code1":"IS",
          "code2":"EN",
          "sliderValue":0.0
        },
        {
          "id":37,
          "question":"I describe myself as..",
          "answer1":"Prepare",
          "answer2":"Improvise",
          "code1":"IS",
          "code2":"EN",
          "sliderValue":0.0
        },
        {
          "id":38,
          "question":"I describe myself as..",
          "answer1":"Verify",
          "answer2":"Brainstorm",
          "code1":"IN",
          "code2":"ES",
          "sliderValue":0.0
        },
        {
          "id":39,
          "question":"I describe myself as..",
          "answer1":"Values",
          "answer2":"Goals",
          "code1":"IN",
          "code2":"ES",
          "sliderValue":0.0
        },
        {
          "id":40,
          "question":"I describe myself as..",
          "answer1":"Elegant",
          "answer2":"Efficient",
          "code1":"IN",
          "code2":"ES",
          "sliderValue":0.0
        }
      ]
}; 

          $scope.totalquest=40;
    // $scope.questions.length-1
        
          $scope.percentage=(100/$scope.totalquest) ;
           $scope.incrementor = Math.round($scope.percentage * 100 * $scope.questionPerPage) / 100;
    
          $scope.getContent(0);
          console.log("getQuestion end");
};




$scope.getContent=function (i){

    console.log("inside getContent()",i);
   
      if(i<$scope.questionPerPage){
        $scope.key=0;
        $scope.prevPa=null;
        $scope.isCanPrev=false;
        $scope.isCanNext=true;
        $scope.nextPage=$scope.questionPerPage;
        $scope.dis=false;
      }

      else if(i>=($scope.totalquest-($scope.totalquest%$scope.questionPerPage))-1){
        $scope.key=i;
        $scope.prevPa=i-$scope.questionPerPage;
        $scope.isCanPrev=true;
        $scope.isCanNext=false;
        $scope.nextPage=null;
        $scope.dis=true;
      }

      else{
       //$scope.key=i;
        $scope.prevPa=i-$scope.questionPerPage;
        $scope.isCanPrev=true;
        $scope.isCanNext=true;
       $scope.nextPage=i+$scope.questionPerPage;
       // $scope.dis=true;
        }
        $scope.inputs=$scope.range();
        console.log("get content end");
      
};


 $scope.nextPagefunc=function(e){
     console.log("Inside nextPage func()");
    $scope.percentage += $scope.incrementor;
    if($scope.percentage > 100){
      $scope.percentage = 100;
    }
    
    $scope.startNum=$scope.endNum+1;
    $scope.endNum=$scope.startNum+($scope.questionPerPage-1);

    $scope.getContent($scope.startNum);  
console.log("nextpage out");
  }

 $scope.resetPage=function(){
     console.log("Inside resetPage func");
    $scope.percentage=Math.round(((100/$scope.totalquest))* 100) / 100;
    $scope.startNum=0;
    $scope.endNum=$scope.questionPerPage-1;
    $scope.getContent($scope.startNum);
    console.log("reset out");
  }

 $scope.prevPage=function(){
     console.log("Inside prev page func");
     if($scope.percentage >= 100){
        $scope.percentage -= $scope.incrementor/$scope.questionPerPage;
      }else{
         $scope.percentage -= $scope.incrementor;
      }
     
      
    
    $scope.startNum=$scope.startNum-$scope.questionPerPage;
    $scope.endNum=$scope.endNum-$scope.questionPerPage;
    
    $scope.getContent($scope.startNum);
    console.log("prev page out");
  }

 $scope.submit=function(){
     console.log("Inside submit func");
    $state.go('ltiresults');
       console.log("submit out");
  }

// $scope.range = function(){
//     console.log("Inside range func");
//     var min= $scope.startNum;
//     var max=$scope.endNum;
//     let step=1;
//     var input = [];
//     for (var i = min; i <= max; i += step){ 
//       input.push(i);
//     }
//     console.log(input+" range end");
//     return input;
//   }

 $scope.onChange=function(valNum){
     console.log("Inside onChange func");
    $scope.questionPerPage=valNum;
    $scope.startNum=$scope.startNum;
    $scope.endNum=$scope.startNum+(valNum-1);
    console.log("on change out");
  }

  $scope.ltiAssessment=function(){
     console.log("Inside ltiAssessment func");
    $state.go('lti');
       console.log("ltiAssessment out");
    }

    $scope.gotoLeaderBrand=function(){
     console.log("Inside gotoLeaderBrand func");
    $state.go('brandingexistinguser');
       console.log("gotoLeaderBrand out");
    }

    $scope.gotoLtiReport=function(){
     console.log("Inside gotoLtiReport func");
    $state.go('report');
       console.log("gotoLtiReport out");
    }
    $scope.gotoHome=function(){
     console.log("Inside gotoLeaderBrand func");
    $state.go('landingpage');
       console.log("gotoLeaderBrand out");
    }

     $scope.soon = function(){
         $state.go('comingsoon');
    }

// $scope.getContent=getContent;
// $scope.nextPagefunc=nextPagefunc;
// $scope.resetPage=resetPage;
// $scope.prevPage=prevPage;
// $scope.submit=submit;
// $scope.range=range;
// $scope.onChange=onChange;

  console.log("onload");
    $scope.questionPerPage=3;
    $scope.startNum=0;
    $scope.endNum=$scope.questionPerPage-1;
    $scope.inputs=$scope.range();
    $scope.getQuestion();
    console.log("onload end");


});


//http:localhost:8080/StripeServer/rest/stripe/result