'use strict';

angular.module('myApp')

// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('/landingpage', {
//     templateUrl: 'partials/landingpage.html',
//     controller: 'landingpageCtrl'
//   });
// }])

.controller('landingpageCtrl', ['$scope','$location','$state', function($scope, $location,$state) {
        
        $scope.ltiAssessment=function(){
        console.log("Inside ltiAssessment func");
        $state.go('lti');
        console.log("ltiAssessment out");
    }
        
        $scope.goToDashboard = function(){
             jQuery("#myModal").modal('hide');
             $location.path('/profile');
        };

        $scope.comingSoon=function(){
        console.log("Inside comingSoon func");
        $state.go('comingsoon');
        console.log("comingSoon out");
    }
        
        /***********************************************
         * Preloader
         ***********************************************/

        jQuery(window).load(function () {
            jQuery("#status").fadeOut();
            jQuery("#preloader").delay(1000).fadeOut("slow");
        })

        /***********************************************
         * Universal Parallax
         * Copyright - ForBetterWeb.com
         ***********************************************/

        // scrolling
        $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
         target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top-50
        },1000);
        return false;
      }
    }
  });
});
        var introHeader = $('.intro'),
            intro = $('.intro');

        buildModuleHeader(introHeader);

        $(window).resize(function() {
            var width = Math.max($(window).width(), window.innerWidth);
            buildModuleHeader(introHeader);
        });

        $(window).scroll(function() {
            effectsModuleHeader(introHeader, this);
        });

        intro.each(function(i) {
            if ($(this).attr('data-background')) {
                $(this).css('background-image', 'url(' + $(this).attr('data-background') + ')');
            }
        });
        function buildModuleHeader(introHeader) {
        };
        function effectsModuleHeader(introHeader, scrollTopp) {
            if (introHeader.length > 0) {
                var homeSHeight = introHeader.height();
                var topScroll = $(document).scrollTop();
                if ((introHeader.hasClass('intro')) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css('top', (topScroll * .4));
                }
                if (introHeader.hasClass('intro') && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css('opacity', (1 - topScroll/introHeader.height() * 1));
                }
            }
        };

        /***********************************************
         * jQuery Parallax
         ***********************************************/

        $('.bg-img').parallax("50%", .12);
        $('.bg-img2').parallax("50%", .12);
        $('.bg-img3').parallax("50%", .12);
        $('.bg-img4').parallax("50%", .12);
        $('.bg-img5').parallax("50%", .12);

        /***********************************************
         * jQuery to collapse the navbar on scroll
         ***********************************************/

        $(window).scroll(function () {

            var nav = $('.navbar-universal');
            if (nav.length) {

                if ($(".navbar-universal").offset().top > 50) {
                    $(".navbar-fixed-top").addClass("top-nav-collapse");
                } else {
                    $(".navbar-fixed-top").removeClass("top-nav-collapse");
                }}
        });

        /***********************************************
         * Tabs
         ***********************************************/

        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })


        /***********************************************
         * jQuery for page scrolling feature
         ***********************************************/

        // $(function () {
        //     $('a.page-scroll').on('click', function (event) {
        //         var $anchor = $(this);
        //         $('html, body').stop().animate({
        //             scrollTop: ($($anchor.attr('href')).offset().top - 55)
        //         }, 1500, 'easeInOutExpo');
        //         event.preventDefault();
        //     });
        // });

         /***********************************************
         * Highlight the top nav as scrolling occurs
         ***********************************************/

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 65
        })

        /***********************************************
         * Closes the Responsive Menu on Menu Item Click in One Page Nav
         ***********************************************/

        $('.navbar-onepage .navbar-collapse ul li a') .on('click', function() { $('.navbar-onepage .navbar-toggle:visible') .click(); });

        /***********************************************
         * Active class to nav
         ***********************************************/

        var url = window.location;
        $('ul.nav a[href="' + url + '"]').parent().addClass('active');
        $('ul.nav a').filter(function () {
            return this.href == url;
        }).parent().addClass('active');


        /***********************************************
         * Carousel
         ***********************************************/

        $('.carousel-big').carousel({
            interval: 6500, //changes the speed
            pause: "false"
        })

        $('.carousel-small').carousel({
            interval: 5000, //changes the speed
            pause: "false"
        })

        /***********************************************
         * HTML5 Placeholder
         ***********************************************/

        $(function () {
            $('input, textarea').placeholder();
        });

        /***********************************************
         * Load WOW.js
         ***********************************************/

        new WOW().init();

       $("#Carousel-intro").css("min-height",window.innerHeight+"px");
        
      
        
        $('.carousel-control.left').click(function() {
            $('#Carousel-intro').carousel('prev');
        });

        $('.carousel-control.right').click(function() {
         $('#Carousel-intro').carousel('next');
        });


}]);