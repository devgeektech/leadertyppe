'use strict';
var myApp = angular.module('myApp', [
    'ui.router',
    'auth0.lock',
    'angular-jwt']);
myApp.config(function ($stateProvider, $urlRouterProvider,lockProvider) {

    lockProvider.init({
        clientID: 'CrhPEaVBsed7H1dadvkU2vBe9QPZeedh',
        domain: 'snathan.auth0.com'
    });
    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================
        //1
        .state('landingpage', {
            url: '/landingpage',
            templateUrl: 'partials/landingpage.html',
            controller: 'landingpageCtrl'
        })
        //existing user 2
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'partials/dashboard.html',
            controller: 'DashboardCtrl'
        })
        //new user 2
        .state('dashboard_new', {
            url: '/dashboard_new',
            templateUrl: 'partials/dashboardNew.html',
            controller: 'DashboardNewCtrl'
        })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        //lti assesment 3
        .state('lti', {
            url: '/lti',
            templateUrl: 'partials/sliderpage1.html',
            controller: 'Sliderpage1Ctrl'
        })
        //Report 4
        .state('report', {
            url: '/report',
            templateUrl: 'partials/report.html',
            controller: 'ReportCtrl'
        })

        //Leadership brand new user 4
        .state('profile', {
            url: '/profile',
            templateUrl: 'partials/profile.html',
            controller: 'profileCtrl'

        })

        //Leadership brand 4
        .state('brandingexistinguser', {
            url: '/brandingexistinguser',
            templateUrl: 'partials/brandingexistinguser.html',
            controller: 'brandingexistinguserCtrl'
        })

        .state('comingsoon', {
            url: '/soon',
            templateUrl: 'partials/soon.html',
            controller: 'SoonCtrl'
        })

        .state('ltiresults', {
            url: '/results',
            templateUrl: 'partials/ltiresults.html',
            controller: 'LtiResultsCtrl'
        })

        //Login
        .state('login', {
            url: '/login',
            templateUrl: 'partials/login.html',
            controller: 'loginController'
        })

    // .state('sample1', {
    //     url: '/sample1',
    //     views: {
    //         nav: {
    //             templateUrl: 'navbar.html'
    //         },
    //         content: {
    //             templateUrl: 'sample1.html'
    //         }
    //     }
    // })

    $urlRouterProvider.otherwise('/landingpage');


});